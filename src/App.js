import React from 'react';
import './App.css';

import { Route , Switch  } from 'react-router-dom';
import ProtectedRoute from './hoc/ProtectedRoute/ProtectedRoute';

import Layout from './container/Layout/Layout';
import Login from './container/Auth/Login/Login';

function App() {
  return (
    <Switch>
        <Route path="/login" exact component={Login}></Route>
        <ProtectedRoute path='/' component={Layout}></ProtectedRoute>
    </Switch>
  );
}

export default App;
