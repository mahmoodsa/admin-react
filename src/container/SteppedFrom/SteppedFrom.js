import { RouterSharp } from '@material-ui/icons';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Redirect, Route, Switch, withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';

import FormPage from '../FormPage/FormPage';

import classes from './SteppedFrom.module.scss';
// import BlockItem from '../BlockList/BlockItem/BlockItem';

const SteppedForm = props => {
    const [currentStep, setCurrentStep] = useState(1);

    const stepCallBack = (stepResponse, stepContent) => {
        nextStep(stepResponse, stepContent);
    }
    const nextStep = (stepResponse, stepContent) => {
        const tempState = { ...props.history.state }
        if (stepContent.locationStateSetter && stepContent.locationStateSetter.length > 0) {
            stepContent.locationStateSetter.forEach(setterObject => {
                tempState[setterObject.setterStateTitle] = stepResponse[setterObject.apiResponseTitle];
            })
        }
        props.history.push({
            pathname: props.match.url + '/steps/' + (currentStep + 1),
            state: tempState
        });
        setCurrentStep(currStep => {
            return currStep + 1;
        })
    }
    // redirect to First Step
    useEffect(() => {
        props.history.push(props.match.url + '/steps/1');
    }, [])
    const routes = []
    const links = []

    props.steppedControls.forEach((item, index) => {
        links.push(
            <React.Fragment
                key={'step__link_' + index}>
                <NavLink
                    activeClassName={classes.ActiveStep}
                    to={props.match.url + '/steps/' + (index + 1)}
                    className={classes.Step}>
                    <span className={classes.StepSpacer}></span>
                    <span className={classes.StepContent}>{index + 1}</span>
                </NavLink>
            </React.Fragment>
        )
        routes.push(
            <Route
                path={props.match.url + '/steps/' + (index + 1)}
                key={'step__route_' + index}
                exact
                render={() => <FormPage key={'step__form__' + index} {...item} callBackFunction={(res) => stepCallBack(res, item)} />}
            />)
    })
    return (
        <React.Fragment>
            <div className={classes.StepContainer}>
                {links}
            </div>
            <Switch>
                {routes}
            </Switch>
        </React.Fragment>
    )
}
export default withRouter(SteppedForm);