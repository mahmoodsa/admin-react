import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch } from 'react-router';

import Drawer from '../../component/Navigation/Drawer/Drawer';
import Navbar from '../../component/Navigation/Navbar/Navbar';
import FormPage from '../FormPage/FormPage';
import SteppedFrom from '../SteppedFrom/SteppedFrom'
import Table from '../Table/Table'
import FileManager from '../Inputs/FileManager/FileManager';

import classes from './Layout.module.scss';
import Slideshow from '../Inputs/slideshow/Slideshow';
import ItemSelector from '../Inputs/ItemSelector/ItemSelector';

const Layout = props => {
  const routes = []
  let pages = useSelector(state => state.dynamicPages.pages)
  pages.forEach(page => {
    page.pages.forEach(item => {
      routes.push(<Route
        path={item.link}
        key={item.id}
        render={() => {
          if (item.pageType === 'form') {
            return <FormPage key={'form__' + item.id} {...item} />
          } else if (item.pageType === 'steppedForm') {
            return <SteppedFrom key={'steppped_form__' + item.id} {...item} />
          } else if (item.pageType === 'table') {
            return <Table key={'table' + item.id} {...item} />
          }
        }} />
      )
    })
  })
  return (
    <div className={classes.Layout}>
      <Drawer />
      <div className={classes.ContentContainer}>
        <Navbar></Navbar>
        <div className={classes.PageContainer}>
          <div className={classes.PageHeading}>
            <h4 className={classes.Title}>تنظیمات سایت</h4>
            <h5 className={classes.SubTitle}>مدیریت صفحه خدمات</h5>
          </div>
          <div className={classes.Content}>
            <div className={classes.ContentBody}>
              <Switch>
                {routes}
                <Route to="/" render={() => <h1 style={{padding: '1rem', fontFamily:"sans-serif"}}>page you are looking for is not found</h1>}/>
              </Switch>
            </div>
          </div>
        </div>
      </div>
      <FileManager />
      <ItemSelector />
      <Slideshow />
    </div>
  );
}

export default Layout;