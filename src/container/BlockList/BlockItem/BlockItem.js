import React, { useEffect, useState } from 'react';
import Button, { buttonTypes } from '../../../component/UI/Button/Button';

import Input from '../../../component/UI/Input/Input';
import { CLOSE_FILE_MANAGER } from '../../../store/actions/actionTypes';

import classes from './BlockItem.module.scss';

const BlockItem = props => {
    const [blockItem, setBlockItem] = useState({...props.item})
    const inputChangedHandler = (e, key, type_content) => {
        setBlockItem(blockItem => {
            let tempControls = {...controls};
            controls[blockItem.type_content][key].value = e.target.value;
            let newBlockItem = {};
            newBlockItem.blog_id = blockItem.blog_id;
            newBlockItem.id = blockItem.id;
            newBlockItem.order = blockItem.order;
            for(let key in controls[blockItem.type_content]) {
                newBlockItem[key] = controls[blockItem.type_content][key].value
            }
            return newBlockItem;
        })
    }
    const [controls, setControls] = useState({
        text: {
            type_content: {
                label: 'نوع بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'text', displayValue: 'متن' },
                        { value: 'file', displayValue: 'فایل' },
                        { value: 'slider', displayValue: 'اسلایدر' },
                        { value: 'QandA', displayValue: 'پرسش و پاسخ' },
                        { value: 'formBuilder', displayValue: 'فرم ساز' }
                    ]
                },
                value: 'text',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            position: {
                label: 'جهت بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'right', displayValue: 'راست' },
                        { value: 'center', displayValue: 'وسط' },
                        { value: 'left', displayValue: 'چپ' },
                    ]
                },
                value: 'right',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            text_fa: {
                label: 'متن فارسی',
                elementType: 'input',
                elementConfig: {
                    type: 'textarea',
                    placeholder: 'متن فارسی بلاک',
                    styles: { height: '100px' }
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            text_en: {
                label: 'متن انگلیسی',
                elementType: 'input',
                elementConfig: {
                    type: 'textarea',
                    placeholder: 'متن انگلیسی بلاک',
                    styles: { height: '100px' }
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
        },
        file: {
            type_content: {
                label: 'نوع بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'text', displayValue: 'متن' },
                        { value: 'file', displayValue: 'فایل' },
                        { value: 'slider', displayValue: 'اسلایدر' },
                        { value: 'QandA', displayValue: 'پرسش و پاسخ' },
                        { value: 'formBuilder', displayValue: 'فرم ساز' }
                    ]
                },
                value: 'file',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            position: {
                label: 'جهت بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'right', displayValue: 'راست' },
                        { value: 'center', displayValue: 'وسط' },
                        { value: 'left', displayValue: 'چپ' },
                    ]
                },
                value: 'right',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            file_id: {
                label: 'انتخاب فایل',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'file id',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            }
        },
        slider: {
            type_content: {
                label: 'نوع بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'text', displayValue: 'متن' },
                        { value: 'file', displayValue: 'فایل' },
                        { value: 'slider', displayValue: 'اسلایدر' },
                        { value: 'QandA', displayValue: 'پرسش و پاسخ' },
                        { value: 'formBuilder', displayValue: 'فرم ساز' }
                    ]
                },
                value: 'slider',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            position: {
                label: 'جهت بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'right', displayValue: 'راست' },
                        { value: 'center', displayValue: 'وسط' },
                        { value: 'left', displayValue: 'چپ' },
                    ]
                },
                value: 'right',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            sliderList: {
                label: 'انتخاب اسلایدر',
                elementType: 'SLIDE_SHOW',
                elementConfig: {
                    
                },
                value: [],
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            }
        },
        QandA: {
            type_content: {
                label: 'نوع بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'text', displayValue: 'متن' },
                        { value: 'file', displayValue: 'فایل' },
                        { value: 'slider', displayValue: 'اسلایدر' },
                        { value: 'QandA', displayValue: 'پرسش و پاسخ' },
                        { value: 'formBuilder', displayValue: 'فرم ساز' }
                    ]
                },
                value: 'QandA',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            position: {
                label: 'جهت بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'right', displayValue: 'راست' },
                        { value: 'center', displayValue: 'وسط' },
                        { value: 'left', displayValue: 'چپ' },
                    ]
                },
                value: 'right',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            question_fa: {
                label: 'سوال فارسی',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'سوال',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            question_en: {
                label: 'سوال انگلیسی',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'سوال',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            answer_fa: {
                label: 'سوال فارسی',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'جواب',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            answer_en: {
                label: 'سوال انگلیسی',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'جواب',
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            }
        },
        formBuilder: {
            type_content: {
                label: 'نوع بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'text', displayValue: 'متن' },
                        { value: 'file', displayValue: 'فایل' },
                        { value: 'slider', displayValue: 'اسلایدر' },
                        { value: 'QandA', displayValue: 'پرسش و پاسخ' },
                        { value: 'formBuilder', displayValue: 'فرم ساز' }
                    ]
                },
                value: 'formBuilder',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            position: {
                label: 'جهت بلاک',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        { value: 'right', displayValue: 'راست' },
                        { value: 'center', displayValue: 'وسط' },
                        { value: 'left', displayValue: 'چپ' },
                    ]
                },
                value: 'right',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            type: {
                label: 'نوع بلاک فرم ساز',
                elementType: 'select',
                elementConfig: {
                    placeholder: 'سوال',
                    options: [
                        {value: 'test' , displayValue: 'رادیو باتن'},
                        {value: 'radix' , displayValue: 'چک باکس'},
                        {value: 'drop_box' , displayValue: 'لسیت باز شو'},
                        {value: 'check_box' , displayValue: 'تایید'},
                        {value: 'text' , displayValue: 'متن'},
                        {value: 'file' , displayValue: 'فایل'},
                    ]
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            }
        }
    });


    useEffect(() => {
        if(blockItem.type_content === 'slider') {
            setControls(controls => {
                const tempControls = {...controls}
                const tempControl = {...tempControls.slider}
                const tempSliderControl = {...tempControl.sliderList}
                const tempSliderValue = []
                blockItem.content.forEach(sliderItem => {
                    tempSliderValue.push({
                        file_manager_id: sliderItem.id,
                        text_fa: sliderItem.text_fa,
                        text_en: sliderItem.text_en,
                        thumbnail: sliderItem.thumbnail
                    })
                })
                tempSliderControl.value = [{
                    text_fa: 'google',
                    text_en: 'yahoo',
                    thumbnail: 'facebook',
                    file_manager_id: 'google'
                }]
                tempControl.sliderList = tempSliderControl;
                tempControls.slider = tempControl;
                return tempControls;
            })
        }

    }, [])

    useEffect(() => {

        let tempContent = [];
        if (blockItem.type_content === 'text') {
            for (let key in controls.text) {
                const formElement = controls.text[key];
                tempContent.push(<Input
                key={key}
                label={formElement.label}
                elementType={formElement.elementType}
                elementConfig={formElement.elementConfig}
                value={formElement.value}
                invalid={!formElement.valid}
                shouldValidate={formElement.validation}
                touched={formElement.touched}
                changed={(e) => inputChangedHandler(e, key, 'text')}/>)
            }
        }
        else if (blockItem.type_content === 'file') {
            for (let key in controls.file) {
                const formElement = controls.file[key];
                tempContent.push(<Input
                key={key}
                label={formElement.label}
                elementType={formElement.elementType}
                elementConfig={formElement.elementConfig}
                value={formElement.value}
                invalid={!formElement.valid}
                shouldValidate={formElement.validation}
                touched={formElement.touched}
                changed={(e) => inputChangedHandler(e, key, 'file')}/>)
            }
        } else if (blockItem.type_content === 'QandA') {
            for (let key in controls.QandA) {
                const formElement = controls.QandA[key];
                tempContent.push(<Input
                key={key}
                label={formElement.label}
                elementType={formElement.elementType}
                elementConfig={formElement.elementConfig}
                value={formElement.value}
                invalid={!formElement.valid}
                shouldValidate={formElement.validation}
                touched={formElement.touched}
                changed={(e) => inputChangedHandler(e, key, 'QandA')}/>)
            }
        } else if (blockItem.type_content === 'formBuilder') {
            for (let key in controls.formBuilder) {
                const formElement = controls.formBuilder[key];
                tempContent.push(<Input
                key={key}
                label={formElement.label}
                elementType={formElement.elementType}
                elementConfig={formElement.elementConfig}
                value={formElement.value}
                invalid={!formElement.valid}
                shouldValidate={formElement.validation}
                touched={formElement.touched}
                changed={(e) => inputChangedHandler(e, key, 'formBuilder')}/>)
            }
        } else if (blockItem.type_content === 'slider') {
            for (let key in controls.slider) {
                const formElement = controls.slider[key];
                tempContent.push(<Input
                key={key}
                label={formElement.label}
                elementType={formElement.elementType}
                elementConfig={formElement.elementConfig}
                value={formElement.value}
                invalid={!formElement.valid}
                shouldValidate={formElement.validation}
                touched={formElement.touched}
                changed={(e) => inputChangedHandler(e, key, 'slider')}/>)
            }
        }
        setContent(tempContent)
    }, [controls])

    let [content, setContent] = useState([]);


    return <div className={classes.BlockItem}>
        {content}
        <div className={classes.BlockSubmit}>
            <Button
                buttonType="button"
                clicked={() => props.BlockSubmitted(blockItem)}
                type={buttonTypes.primary}>ثبت بلاک</Button>
        </div>
    </div>;
}

export default BlockItem;