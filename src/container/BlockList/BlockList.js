import React, {useState} from 'react';

import Button, { buttonTypes } from '../../component/UI/Button/Button';
import BlockItem from './BlockItem/BlockItem';

import classes from './BlockList.module.scss';

const BlockList = props => {
    const [blocks, setBlocks] = useState(props.blocks);
    const blockTemplate = {
        type_content: ['text', 'slider', 'file', 'QandA', 'formBuilder'],
        text: {
            text: ''
        }
    }
    const createBlockHandler = () => {
        setBlocks(blocks => {
            return [
                ...blocks,
                {
                    type_content: 'text',
                    text: '',
                    order: 1,
                    position: 'right',
                    blog_id: '1',
                    block_id: null
                }
            ]
        })
    }
    const blockTypeChnaged = (e, item, index) => {
        const value = e.target.value
        setBlocks(blocks => {
            const tempBlocks = [...blocks];
            const tempBlockItem = {...tempBlocks[index]}
            tempBlockItem.type_content = value;
            tempBlocks[index] = tempBlockItem;
            return tempBlocks;
        })
    }
    const blockPositionChnaged = (e, item, index) => {
        const value = e.target.value
        setBlocks(blocks => {
            const tempBlocks = [...blocks];
            const tempBlockItem = {...tempBlocks[index]}
            tempBlockItem.position = value;
            tempBlocks[index] = tempBlockItem;
            console.log(tempBlocks);
            return tempBlocks;
        })
    }
    const BlockSubmitted = (e, item, index) => {
        console.log(e);
    }

    return (
        <div className={classes.BlockList}>
            {blocks.map((item, index) => {
                return <BlockItem 
                            {...item}
                            BlockSubmitted={(e) => BlockSubmitted(e, item, index)}
                            blockTypeChanged={(e) => blockTypeChnaged(e, item, index)}
                            blockPositionChnaged={e => blockPositionChnaged(e,item, index)}
                            key={'block__item___' + index} />
            })}
            <Button
                buttonType="button"
                clicked={createBlockHandler}
                type={buttonTypes.primary}>ایجاد بلاک جدید</Button>
        </div>
    );
}

export default BlockList;