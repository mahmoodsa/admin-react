import React, { useEffect, useState } from 'react';

import axios from '../../../axios';

import classes from './AsyncDropdown.module.scss';

const AsyncDropdown = props => {

    const [options, setOptions] = useState([]);

    useEffect(() => {
        if (props.getDataApiConfig) {

            axios({
                url: props.getDataApiConfig.url,
                method: props.getDataApiConfig.method
            })
                .then(res => {
                    const tempOptions = res.data.data.map(item => {
                        let tempCondition = {}
                        if (props.condition) {
                            for (let key in props.condition) {
                                tempCondition[key] = item[key]
                            }
                        }
                        return {
                            displayValue: item[props.optionsConfig.optionDisplayValue],
                            value: item[props.optionsConfig.optionValue],
                            ...tempCondition
                        };
                    })
                    setOptions(tempOptions);
                })
                .catch(err => {

                })
        }
    }, [])
    useEffect(() => {
        if (props.options && props.options.length > 0) {
            const tempOptions = props.options.map(item => {
                let tempCondition = {}
                if (props.condition) {
                    for (let key in props.condition) {
                        tempCondition[key] = item[key]
                    }
                }
                return {
                    displayValue: item[props.optionsConfig.optionDisplayValue],
                    value: item[props.optionsConfig.optionValue],
                    ...tempCondition
                };
            })
            setOptions(tempOptions);
        }
    }, [props.options])


    return <div className={props.className}>
        <select value={props.value}
            onChange={props.changed}
            className={classes.InputElement}>
            <option value="">انتخاب کنید</option>
            {options.map(option => {
                if (props.condition) {
                    for (let key in props.condition) {
                        if (props.condition[key] != option[key]) {
                            return;
                        }
                    }
                }
                return <option
                    key={option.value}
                    value={option.value}>{option.displayValue}</option>
            })}
        </select>
    </div>
}
export default AsyncDropdown;