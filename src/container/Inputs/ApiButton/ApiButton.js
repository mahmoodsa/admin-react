import React, { useState } from 'react';
import axios from '../../../axios';

import { toPersianNumbers, commaSeparate } from '../../../utility'

import Button, { buttonTypes } from '../../../component/UI/Button/Button';

import classes from './ApiButton.module.scss';

const ApiButton = props => {

  const [price, setPrice] = useState(0);
  
  let priceContainer = null;

  const buttonClickHandler = () => {
    axios({
      method: props.elementConfig.apiType,
      url: props.elementConfig.apiUrl,
    })
      .then(res => {
        if(res.data.success) {
          setPrice(res.data.price);
        }
      })
      .catch(err => {
        console.log(err);
      })
  }

  if(price > 0) {
    priceContainer = <div className={classes.PriceContainer}>
      <span className={classes.Label}>قیمت فعلی دلار</span>
      <span>{toPersianNumbers(commaSeparate(price))}</span>
      <span className={classes.Label}>ریال</span>
    </div>
  }

  return (
    <React.Fragment>
      <Button
        type={buttonTypes.primary}
        buttonType="button"
        clicked={buttonClickHandler}>
        {props.elementConfig.placeholder}
      </Button>
      {priceContainer}
    </React.Fragment>
  )
}

export default ApiButton;