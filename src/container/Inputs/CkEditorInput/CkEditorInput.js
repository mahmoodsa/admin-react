import React from 'react';

import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';


import classes from './CkEditorInput.module.scss';

const CkEditorInput = props => {
    return <div className={classes.InputContainer}>
        <CKEditor
            editor={ClassicEditor}
            data={props.value}
            onChange={(event, editor) => {
                const data = editor.getData();
                props.changed(data);
            }}
            config={{
                // toolbar: ['heading', '|','bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', '|', 'outdent', 'indent',  '|', 'undo', 'redo' ,'alignment'],
                alignment: {
                    options: [ 'left', 'right' ]
                },
                heading: {
                    options: [
                        { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                        { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                        { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                    ]
                }
            }}
        />
    </div>;
}

export default CkEditorInput;