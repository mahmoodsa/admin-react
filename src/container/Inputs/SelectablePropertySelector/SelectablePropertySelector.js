import React, { useEffect, useState } from 'react';

import Button, { buttonTypes } from '../../../component/UI/Button/Button';
import SelectablePropertyItem from './SelectablePropertyItem/SelectablePropertyItem'

import classes from './SelectablePropertySelector.module.scss';



const SelectablePropertySelector = props => {
  const [propertyItems, setPropertyItems] = useState([])
  const [isInitiated, setIsInitiated] = useState(false) ;
  const [propsRecieved, setPropsRecieved] = useState(false) ;
  
  const addPropertyItem = () => {
    setPropertyItems(currentItems => {
      const tempItems = [...currentItems , {key: `propertyItem__${Math.floor(Math.random() * 100000)}`,type: '', title: {fa: '', en: ''} , value: []}]
      return tempItems;
    })
  }

  const propertyItemChangeHandler = (value, index) => {
    setPropertyItems(currentItems => {
      const tempItems = [...currentItems];
      tempItems[index] = value;
      return tempItems;
    })
  }
  const propertyItemDeleteHandler = (index) => {
    setPropertyItems(currentItems => {
      const tempItems = [...currentItems];
      tempItems.splice(index, 1);
      return tempItems;
    })
  }

  useEffect(() => {
    if(!propsRecieved) {
      if(props.value.length > 0) {
        setPropertyItems(props.value);
        setPropsRecieved(true);
      }
      setIsInitiated(true);
    }
  }, [props.value])

  useEffect(() => {
    if(isInitiated) {
      props.changed(propertyItems)
    }
  }, [propertyItems])
  
  return (
    <div className={classes.SelectablePropertySelector}>
      {propertyItems.map((item, index) => 
        <SelectablePropertyItem 
          key={item.key} 
          index={index}
          config={item} 
          changed={value => propertyItemChangeHandler(value, index)}
          deleted={() => propertyItemDeleteHandler(index)}/>
      )}
      <Button buttonType="button" type={buttonTypes.primary} clicked={addPropertyItem}>
        افزودن ویژگی
      </Button>
    </div>
  );
}

export default SelectablePropertySelector;