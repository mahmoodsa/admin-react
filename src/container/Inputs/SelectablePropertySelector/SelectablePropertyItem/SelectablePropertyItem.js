import React, { useState } from 'react';

import Input from '../../../../component/UI/Input/Input';
import Button, { buttonTypes } from '../../../../component/UI/Button/Button';

import classes from './SelectablePropertyItem.module.scss';

import { Close } from '@material-ui/icons';

const SelectablePropertyItem = props => {

  const inputChangeHandler = (e, config, inputName, index) => {
    const tempConfig = { ...config }
    const tempTitle = { ...tempConfig.title }
    if (inputName === 'title_fa') {
      tempTitle.fa = e.target.value;
    } else if (inputName === 'title_en') {
      tempTitle.en = e.target.value;
    } else if (inputName === 'type') {
      tempConfig.type = e;
      tempConfig.value = []
    } else if (inputName === 'sizeValue') {
      const tempValue = [...tempConfig.value]
      tempValue[index] = e.target.value;
      tempConfig.value = tempValue;
    } else if (inputName === 'value_fa') {
      const tempValue = [...tempConfig.value]
      const tempValueItem = { ...tempValue[index] }
      tempValueItem.fa = e.target.value;
      tempValue[index] = tempValueItem;
      tempConfig.value = tempValue;
    } else if (inputName === 'value_en') {
      const tempValue = [...tempConfig.value]
      const tempValueItem = { ...tempValue[index] }
      tempValueItem.en = e.target.value;
      tempValue[index] = tempValueItem;
      tempConfig.value = tempValue;
    } else if (inputName === 'value_hex') {
      const tempValue = [...tempConfig.value]
      const tempValueItem = { ...tempValue[index] }
      tempValueItem.hex = e.target.value;
      tempValue[index] = tempValueItem;
      tempConfig.value = tempValue;
    } else if (inputName === 'icon_title_fa') {
      const tempValue = [...tempConfig.value];
      const tempValueItem = { ...tempValue[index] };
      tempValueItem.fa = e.target.value;
      tempValue[index] = tempValueItem;
      tempConfig.value = tempValue;
    } else if (inputName === 'icon_title_en') {
      const tempValue = [...tempConfig.value];
      const tempValueItem = { ...tempValue[index] };
      tempValueItem.en = e.target.value;
      tempValue[index] = tempValueItem;
      tempConfig.value = tempValue;
    } else if (inputName === 'icon') {
      const tempValue = [...tempConfig.value]
      const tempValueItem = { ...tempValue[index] }
      tempValueItem.icon = e;
      tempValue[index] = tempValueItem;
      tempConfig.value = tempValue;
    } else if (inputName === 'image') {
      const tempValue = [...tempConfig.value]
      const tempValueItem = { ...tempValue[index] }
      tempValueItem.image = e;
      tempValue[index] = tempValueItem;
      tempConfig.value = tempValue;
    }
    tempConfig.title = tempTitle;
    props.changed(tempConfig)
  }

  const [control, setControl] = useState({
    elementType: 'select',
    elementConfig: {
      placeholder: 'نقش',
      options: [
        { value: '', displayValue: 'انتخاب نوع ویژگی' },
        { value: 'color', displayValue: 'رنگ' },
        { value: 'number', displayValue: 'عددی' },
        { value: 'string', displayValue: 'متن' },
        { value: 'with_icon', displayValue: 'ایکون' },
        { value: 'drop_down', displayValue: 'دراپ داون' },
      ]
    },
    value: '',
    touched: false,
    validation: {
      required: true,
    },
    disableLabel: true
  })

  const typeChangeHandler = (e, config) => {
    setControl(control => {
      return ({ ...control, value: e.target.value })
    })
    inputChangeHandler(e.target.value, config, 'type');
  }

  const addValueToItem = (config) => {
    const tempConfig = { ...config };
    let tempValue = { ...tempConfig.value };
    if (tempConfig.type === 'color') {
      tempValue = [...tempConfig.value, { fa: '', en: '', hex: '', icon: '' }]
    } else if (tempConfig.type === 'string') {
      tempValue = [...tempConfig.value, { fa: '', en: '' }]
    } else if (tempConfig.type === 'number') {
      tempValue = [...tempConfig.value, '']
    } else if (tempConfig.type === 'drop_down') {
      tempValue = [...tempConfig.value, '']
    } else if (tempConfig.type === 'with_icon') {
      tempValue = [...tempConfig.value, { fa: '', en: '', icon: '', image: '' }]
    }
    tempConfig.value = tempValue;
    props.changed(tempConfig)
  }

  let valuesUi = null;
  if (props.config.type === 'string') {
    valuesUi = <div>
      {
        props.config.value.length > 0
          ? props.config.value.map((item, index) => (
            <div className={classes.dFlex} key={'string_item__' + index}>
              <span className={classes.InputLabel}>مقدار فارسی</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].fa ? props.config.value[index].fa : ''}
                changed={e => inputChangeHandler(e, props.config, 'value_fa', index)} />
              <span className={classes.InputLabel}>مقدار انگلیسی</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].en ? props.config.value[index].en : ''}
                changed={e => inputChangeHandler(e, props.config, 'value_en', index)} />
            </div>
          ))
          : null
      }
      <div className={classes.dFlex}>
        <Button buttonType="button" type={buttonTypes.primary} clicked={() => addValueToItem(props.config)}>
          افزودن مقدار جدید
        </Button>
      </div>
    </div>
  } else if (props.config.type === 'color') {
    valuesUi = <div>
      {
        props.config.value.length > 0
          ? props.config.value.map((item, index) => (
            <div className={classes.dFlex} key={`color_item_${index}`}>
              <span className={classes.InputLabel}>نام رنگ فارسی</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].fa}
                changed={e => inputChangeHandler(e, props.config, 'value_fa', index)} />
              <span className={classes.InputLabel}>نام رنگ انگلیسی</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].en}
                changed={e => inputChangeHandler(e, props.config, 'value_en', index)} />
              <span className={classes.InputLabel}>کد رنگ</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].hex}
                changed={e => inputChangeHandler(e, props.config, 'value_hex', index)} />
              <span className={classes.InputLabel}>آیکون</span>
              <Input
                disableLabel={true}
                elementType="fileManager"
                elementConfig={{
                  limit: 1,
                  returnUrl: true
                }}
                value={[]}
                valid={false}
                touched={false}
                validation={{
                  required: true,
                }}
                name='icon'
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].icon}
                changed={e => inputChangeHandler(e, props.config, 'icon', index)} />
            </div>
          ))
          : null
      }
      <div className={classes.dFlex}>
        <Button buttonType="button" type={buttonTypes.primary} clicked={() => addValueToItem(props.config)}>
          افزودن مقدار جدید
        </Button>
      </div>
    </div>
  } else if (props.config.type === 'number') {
    valuesUi = <div>
      {
        props.config.value.length > 0
          ? props.config.value.map((item, index) => (
            <div className={classes.dFlex} key={"size__item__" + index}>
              <span className={classes.InputLabel}>مقدار {index + 1}</span>
              <Input
                elementConfig={{
                  type: 'number'
                }}
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index]}
                changed={e => inputChangeHandler(e, props.config, 'sizeValue', index)} />
            </div>
          ))
          : null
      }
      <div className={classes.dFlex}>
        <Button buttonType="button" type={buttonTypes.primary} clicked={() => addValueToItem(props.config)}>
          افزودن مقدار جدید
        </Button>
      </div>
    </div>
  } else if (props.config.type === 'with_icon') {
    valuesUi = <div>
      {
        props.config.value.length > 0
          ? props.config.value.map((item, index) => (
            <div className={classes.dFlex} key={`color_item_${index}`}>
              <span className={classes.InputLabel}>عنوان آیکون فارسی</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].fa}
                changed={e => inputChangeHandler(e, props.config, 'icon_title_fa', index)} />
              <span className={classes.InputLabel}>عنوان آیکون انگلیسی</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].en}
                changed={e => inputChangeHandler(e, props.config, 'icon_title_en', index)} />
              <span className={classes.InputLabel}>آیکون</span>
              <Input
                disableLabel={true}
                elementType="fileManager"
                elementConfig={{
                  limit: 1,
                  returnUrl: true
                }}
                value={[]}
                valid={false}
                touched={false}
                validation={{
                  required: true,
                }}
                name='icon'
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].icon}
                changed={e => inputChangeHandler(e, props.config, 'icon', index)} />
              <span className={classes.InputLabel}>عکس</span>
              <Input
                disableLabel={true}
                elementType="fileManager"
                elementConfig={{
                  limit: 1,
                  returnUrl: true
                }}
                value={[]}
                valid={false}
                touched={false}
                validation={{
                  required: true,
                }}
                name='image'
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].image}
                changed={e => inputChangeHandler(e, props.config, 'image', index)} />
            </div>
          ))
          : null
      }
      <div className={classes.dFlex}>
        <Button buttonType="button" type={buttonTypes.primary} clicked={() => addValueToItem(props.config)}>
          افزودن مقدار جدید
        </Button>
      </div>
    </div>
  } else if (props.config.type === 'drop_down') {
    valuesUi = <div>
      {
        props.config.value.length > 0
          ? props.config.value.map((item, index) => (
            <div className={classes.dFlex} key={'string_item__' + index}>
              <span className={classes.InputLabel}>مقدار فارسی</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].fa ? props.config.value[index].fa : ''}
                changed={e => inputChangeHandler(e, props.config, 'value_fa', index)} />
              <span className={classes.InputLabel}>مقدار انگلیسی</span>
              <Input
                disableLabel={true}
                classes={{ root: classes.InputElement }}
                value={props.config.value[index].en ? props.config.value[index].en : ''}
                changed={e => inputChangeHandler(e, props.config, 'value_en', index)} />
            </div>
          ))
          : null
      }
      <div className={classes.dFlex}>
        <Button buttonType="button" type={buttonTypes.primary} clicked={() => addValueToItem(props.config)}>
          افزودن مقدار جدید
        </Button>
      </div>
    </div>
  }

  return (
    <div className={classes.SelectablePropertyItem}>
      <button onClick={() => { props.deleted() }} className={classes.DeleteButton}>
        <Close classes={{ root: classes.DeleteIcon }} />
      </button>
      <div className={classes.dFlex}>
        ویژگی {props.index + 1}
      </div>
      <div className={classes.dFlex}>
        <span className={classes.InputLabel} style={{ marginLeft: "4.4rem" }}>نوع ویژگی</span>
        <Input disableLabel={true} classes={{ root: classes.InputElement }} {...control} changed={e => typeChangeHandler(e, props.config)} value={props.config.type} />
      </div>
      <div className={classes.dFlex}>
        <span className={classes.InputLabel}>عنوان فارسی</span>
        <Input disableLabel={true} classes={{ root: classes.InputElement }} value={props.config.title.fa} changed={e => inputChangeHandler(e, props.config, 'title_fa')} />
        <span className={classes.InputLabel}>عنوان انگلیسی</span>
        <Input disableLabel={true} classes={{ root: classes.InputElement }} value={props.config.title.en} changed={e => inputChangeHandler(e, props.config, 'title_en')} />
      </div>
      <div>
        <div className={classes.dFlex}>مقادیر</div>
        {valuesUi}
      </div>
    </div>
  );
}

export default SelectablePropertyItem;