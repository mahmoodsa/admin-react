import React, { useEffect, useState } from 'react';

import axios from 'axios';

import AsyncDropdown from '../AsyncDropdown/AsyncDropdown';
import Button, { buttonTypes } from '../../../component/UI/Button/Button';
import { Add, Close } from '@material-ui/icons';

import classes from './MultipleAsyncDropdown.module.scss';

const MultipleAsyncDropdown = props => {

    const [options, setOptions] = useState({})
    const dropdownChangeHandler = (e, index, values) => {
        const tempValue = [...values];
        tempValue[index] = Number(e.target.value);
        props.changed(tempValue);
    }

    const addDropdownHandler = (values) => {
        props.changed([...values, 'null'])
    }

    const deleteDropdownItemHandler = (index, values) => {
        const tempValue = [...values];
        tempValue.splice(index, 1);
        props.changed(tempValue);
    }

    useEffect(() => {

        axios({
            url: props.getDataApiConfig.url,
            method: props.getDataApiConfig.method
        })
            .then(res => {
                setOptions(res.data.data);
            })
            .catch(err => {

            })
        if (!props.value || props.value.length <= 0) {
            props.changed([''])
        }
    }, [])

    const dropdonws = [];

    if (props.value && props.value.length > 0) {
        props.value.forEach((item, index) => {
            dropdonws.push(
                <div
                    className={classes.AsyncDropdownContainer}
                    key={'AsyncDropdown__item__' + index}>
                    <button type="button" className={classes.DeleteButton} onClick={() => deleteDropdownItemHandler(index, props.value)}>
                        <Close classes={{ root: classes.DeleteIcon }} />
                    </button>
                    <AsyncDropdown
                        className={classes.AsyncDropdown}
                        options={options}
                        value={item}
                        optionsConfig={props.optionsConfig}
                        {...props.dropdownConfig}
                        changed={e => dropdownChangeHandler(e, index, props.value)} />
                </div>
            )
        });
    }

    return <div className={classes.MultipleAsyncDropdown}>
        {dropdonws}
        <Button type={buttonTypes.primary} buttonType="button" clicked={() => addDropdownHandler(props.value)}>
            <Add />
        </Button>
    </div>
}
export default MultipleAsyncDropdown;