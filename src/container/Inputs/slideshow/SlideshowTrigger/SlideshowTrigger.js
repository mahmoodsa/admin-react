import React from 'react';

import { useDispatch } from 'react-redux';
import classes from './SlideshowTrigger.module.scss';
import { Add, Edit, Close } from '@material-ui/icons';


const SlideshowTrigger = props => {

  const dispatch = useDispatch();

  let button = <button
    type="button"
    className={classes.AddButton}
    onClick={props.openSlideshow}>
    <Add classes={{ root: classes.AddIcon }} />
  </button>
  if (props.value) {
    button = <React.Fragment>
      <button
        type="button"
        className={classes.EditButton}
        onClick={props.openSlideshow}>
        <Edit classes={{ root: classes.EditIcon }} />
        <img src={props.value.thumbnail} alt="slildehsow item image" />
      </button>
      <button className={classes.DeleteButton} onClick={props.itemDeleted}>
        <Close classes={{root: classes.DeleteIcon}}></Close>
      </button>
    </React.Fragment>
  }
  return (
    <div className={classes.SlideshowTrigger}>
      {button}
    </div>
  );
}

export default SlideshowTrigger;