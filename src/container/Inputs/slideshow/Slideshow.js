import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from '../../../store/actions/index';

import Modal from '../../../hoc/Modal/Modal';
import FormPage from '../../FormPage/FormPage';

import classes from './Slideshow.module.scss';

const Slideshow = props => {
  const dispatch = useDispatch();
  const isOpen = useSelector(state => state.slideshow.isOpen)
  const SlideshowControls = useSelector(state => state.slideshow.controls)

  const slideshowClosed = () => dispatch(actions.closeSlideshow());
  const submitSlideshow = (controls) => dispatch(actions.submitSlideshow(controls))
  const submitHandler = (controls) => {
    submitSlideshow(controls)
  }
  return (
    <Modal 
      open={isOpen} 
      modalClosed={slideshowClosed}
      height="auto"
      width="500px">
      <div className={classes.Title}>انتخاب اسلاید</div>
      <div className={classes.Content}>
        <FormPage 
          controls={SlideshowControls}
          asyncControls={SlideshowControls}
          asyncSetterTrigger={isOpen}
          submitted={(controls) => submitHandler(controls)}/>
      </div>
    </Modal>
  );
}

export default Slideshow;