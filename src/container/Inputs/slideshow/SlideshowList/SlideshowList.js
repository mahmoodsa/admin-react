import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { error, success } from '../../../../utility'
import * as actions from '../../../../store/actions/index';

import SlideshowTrigger from '../SlideshowTrigger/SlideshowTrigger';

import classes from './SlideshowList.module.scss';

const SlideshowList = props => {
    const dispatch = useDispatch();

    const [initiated, setInitiated] = useState(false);

    const initializeData = value => {
        const transformedValue = value.map(item => {
            const tempItem = {};
            for (let key in item) {
                if (key === 'file_manager_id') {
                    tempItem['file_id'] = item[key];
                } else {
                    tempItem[key] = item[key];
                }
            }
            return tempItem;
        })
        submitHandler(transformedValue)
    }

    const submitHandler = (itemsList) => {
        props.submitted(itemsList);
    }

    const deleteHandler = (index, itemsList) => {
        const tempItems = [...itemsList]
        tempItems.splice(index, 1);
        submitHandler(tempItems)
    }
    
    const openSlideshow = (value, index, slideshowItems, isNewItem) => {
        const indexIsNull = typeof index === typeof null;
        let itemIndex = indexIsNull ? slideshowItems.length : index;
        dispatch(actions.openSlideshow(value, submitHandler, itemIndex, isNewItem, slideshowItems))
    }


    useEffect(() => {
        if (props.value.length > 0 && !initiated) {
            setInitiated(true);
            initializeData(props.value);
        }
    }, [props.value])

    let itemsList = [];
    if (props.value.length > 0) {
        itemsList = props.value.map((value, index) => {
            return <li
                className={classes.SlideshowItem}
                key={"slideshow_item" + index}>
                <SlideshowTrigger
                    openSlideshow={() => openSlideshow(value, index, props.value, false)}
                    index={index}
                    submitted={submitHandler}
                    itemDeleted={() => deleteHandler(index, props.value)}
                    value={value} />
            </li>
        })
    }
    return (
        <ul className={classes.SlideshowList}>
            {itemsList}
            <li className={classes.SlideshowItem}>
                <SlideshowTrigger submitted={submitHandler} openSlideshow={() => openSlideshow(null, null, props.value, true)} newItem={true} value={null} />
            </li>
        </ul>
    );
}

export default SlideshowList;