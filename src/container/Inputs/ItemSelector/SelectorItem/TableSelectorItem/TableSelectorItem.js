import React, { useEffect, useState } from 'react';

import classes from './TableSelectorItem.module.scss';

import Input from '../../../../../component/UI/Input/Input';

const TableSelectorItem = props => {
    useEffect(() => {
        setSelectItemcontrols(controls => {
            const tempControls = { ...controls };
            tempControls.value = props.isActive;
            return tempControls;
        })
    }, [props.isActive])
    
    const [selectItemcontrols, setSelectItemcontrols] = useState({
        disableLabel: true,
        elementType: 'checkbox',
        elementConfig: {
            type: 'checkbox',
            placeholder: 'مقدار تخفیف درصدی است؟',
            checked: true
        },
        value: false,
        validation: {
            required: true,
        },
        valid: false,
        touched: false
    })

    const selectItemChangeHandler = (e) => {
        props.changed(e)
    }

    const rowContent = [];
    for (let key in props.data) {
        rowContent.push(
            <div
                key={"rwo_content" + key}
                style={{ width: 100 / props.headerItemsLength - 10/props.headerItemsLength + '%' }}
                className={classes.tableCell}>
                {props.data[key]}
            </div>
        )
    }
    return (
        <div className={classes.tableRow}>
            <div style={{ width: '4%' }} className={classes.tableCell}>
                <Input {...selectItemcontrols} changed={selectItemChangeHandler} />
            </div>
            <div style={{ width: '6%' }} className={classes.tableCell}>{props.index}</div>
            {rowContent}
        </div>
    );
}

export default TableSelectorItem;