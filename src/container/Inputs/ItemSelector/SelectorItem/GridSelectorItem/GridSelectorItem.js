import React from 'react';

import classes from './GridSelectorItem.module.scss';

import { Check } from '@material-ui/icons';

const GridSelectorItem = props => {
    const selectorItemClasses = [classes.SelectorItem, props.isActive ? classes.Active : ''].join(' ');

    return (
        <div className={selectorItemClasses}>
            <button
                onClick={props.ItemClicked}
                className={classes.ImageContainer}>
                <img src="" alt="item Image"/>
                <span className={classes.Checkbox}>
                    <Check classes={{ root: classes.CheckIcon }} />
                </span>
            </button>
            <div className={classes.SelectorItemCaption}>
                <div className={classes.SelectorItemTitle}>
                    عنوان آیتم
                </div>
                <div className={classes.SelectorItemName}>
                    گالری هومن
                </div>
            </div>
        </div>
    );
}

export default GridSelectorItem;