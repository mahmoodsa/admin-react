import React, { useState } from 'react';

import Input from '../../../../component/UI/Input/Input';

import classes from './SelectorTableHeader.module.scss';

const SelectorTableHeader = (props) => {

    const [selectAllcontrols, setSelectAllcontrols] = useState({
        disableLabel: true,
        elementType: 'checkbox',
        elementConfig: {
            type: 'checkbox',
            placeholder: 'مقدار تخفیف درصدی است؟',
            checked: true
        },
        value: false,
        validation: {
            required: true,
        },
        valid: false,
        touched: false
    })

    const selectAllChangeHandler = (e) => {
        setSelectAllcontrols(controls => {
            const tempControls = { ...controls };
            props.allItemsSelectorHandler(e);
            tempControls.value = e;
            return tempControls;
        })
    }

    const headCellWidth = (100 / props.tableHeaderConfig.length) - 10/props.tableHeaderConfig.length + '%';
    return <div className={classes.SelectorTableHeader}>
        <div className={classes.HeaderItem} style={{ width: "4%" }}>
            <Input {...selectAllcontrols} changed={selectAllChangeHandler} />
        </div>
        <div className={classes.HeaderItem} style={{ width: "6%", paddingRight: '0', paddingLeft: '0' }}>
            #
        </div>

        {
            props.tableHeaderConfig ? props.tableHeaderConfig.map((headerItem, index) => {
                return <div
                    key={"tabel_header_item_+" + index}
                    className={classes.HeaderItem}
                    style={{ width: headCellWidth }}>
                    {headerItem.title}
                </div>
            }) : null
        }
    </div>
}

export default SelectorTableHeader;