import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

// import classes from './ItemSelectorTrigger.module.scss';

import Button, { buttonTypes } from '../../../../component/UI/Button/Button';

import * as actions from '../../../../store/actions/index';

const ItemSelectorTrigger = props => {
    const dispatch = useDispatch();

    const submitCallbackFunction = (selectedItems) => {
        props.submitted(selectedItems);
    }

    const ItemSelectorTriggerClicked = () => dispatch(actions.ItemSelectorTriggerClicked(props.getItemsUrl, props.value, submitCallbackFunction, props.selectorLabel, props.selectorConfig));

    useEffect(() => {
        // i don't know what to put here
    }, [])

    return <Button
        buttonType="button"
        type={buttonTypes.primary}
        clicked={ItemSelectorTriggerClicked}>انتخاب آیتم</Button>
}

export default ItemSelectorTrigger;