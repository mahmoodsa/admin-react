import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as actions from '../../../store/actions/index';

import classes from './ItemSelector.module.scss';

import SelectorTableHeader from './SelectorTableHeader/SelectorTableHeader';
import GridSelectorItem from './SelectorItem/GridSelectorItem/GridSelectorItem';
import TableSelectorItem from './SelectorItem/TableSelectorItem/TableSelectorItem';

import Modal from '../../../hoc/Modal/Modal';
import Button, { buttonTypes } from '../../../component/UI/Button/Button';


const ItemSelector = props => {
    const dispatch = useDispatch();
    const itemSelectorSelectItem = id => dispatch(actions.itemSelectorSelectItem(id))
    const itemSelectorDeselectItem = id => dispatch(actions.itemSelectorDeselectItem(id))
    const submitItemSelector = () => dispatch(actions.submitItemSelector())
    const closeItemSelector = () => dispatch(actions.closeItemSelector());
    const itemSelectorSelectAllItems = (isChecked) => dispatch(actions.itemSelectorSelectAllItems(isChecked));

    const ItemSelectorIsOpen = useSelector(state => state.itemSelector.isOpen);
    const itemsList = useSelector(state => state.itemSelector.itemsList);
    const selectedItemsId = useSelector(state => state.itemSelector.selectedItemsId);
    const label = useSelector(state => state.itemSelector.label);
    const selectorType = useSelector(state => state.itemSelector.type);
    const tableHeaderConfig = useSelector(state => state.itemSelector.tableHeaderConfig);

    const SelectorItemClickHandler = (id) => {
        if (selectedItemsId.indexOf(id.toString()) <= -1) {
            return itemSelectorSelectItem(id.toString())
        } else {
            return itemSelectorDeselectItem(id.toString())
        }
    }

    const selectorSubmited = useCallback(() => {
        submitItemSelector();
    }, [])

    const tableItemChangeHandler = (value, id) => {
        if (value) {
            itemSelectorSelectItem(id);
        } else {
            itemSelectorDeselectItem(id);
        }
    }

    const allItemsSelectorHandler = (isChecked) => {
        itemSelectorSelectAllItems(isChecked);
    }

    let [itemsListUi, setItemsListUi] = useState([]);
    let [tableHeader, setTableHeader] = useState(null);
    const headerItemsLength = tableHeaderConfig ? tableHeaderConfig.length : 0;

    useEffect(() => {
        if (itemsList && (itemsList.length > 0)) {
            if (selectorType === 'table') {
                // TABLE HEADER SECTION
                setTableHeader(<SelectorTableHeader headerItemsLength={headerItemsLength} tableHeaderConfig={tableHeaderConfig} allItemsSelectorHandler={allItemsSelectorHandler} />);

                // TABLE ITMES 
                setItemsListUi(
                    itemsList.map(item => {
                        const data = {};
                        tableHeaderConfig.forEach(headerItem => {
                            data[headerItem.name] = item[headerItem.name]
                        })
                        return <TableSelectorItem
                            data={data}
                            headerItemsLength={headerItemsLength}
                            key={"selector__item___" + item.id}
                            isActive={selectedItemsId.indexOf(item.id) > -1}
                            changed={(value) => tableItemChangeHandler(value, item.id)} />
                    })
                )
            } else if (selectorType === 'grid') {
                setItemsListUi(
                    itemsList.map(item => {
                        return <GridSelectorItem
                            key={"selector__item___" + item.id}
                            isActive={selectedItemsId.indexOf(item.id.toString()) > -1}
                            ItemClicked={() => SelectorItemClickHandler(item.id)} />
                    })
                )
            }
        } else {
            setItemsListUi(<div>آیتمی جهت نمایش وجود ندارد!</div>)
        }
    }, [selectedItemsId, itemsList, ItemSelectorIsOpen])

    return (
        <Modal
            open={ItemSelectorIsOpen}
            modalClosed={closeItemSelector}
            height="450px">
            <div className={classes.ItemSelector}>
                <div className={classes.SelectorHeader}>
                    <span>{label}</span>
                </div>
                <div className={[classes.SelectroItems, selectorType === 'table' ? classes.noPadding : ''].join(' ')}>
                    {tableHeader ?? null}
                    {itemsListUi}
                </div>
                <div className={classes.SelectorFooter}>
                    <Button
                        disabled={itemsList && (itemsList.length <= 0)}
                        type={buttonTypes.primary}
                        clicked={selectorSubmited}>
                        ثبت
                        </Button>
                </div>
            </div>
        </Modal>
    );
}

export default ItemSelector;