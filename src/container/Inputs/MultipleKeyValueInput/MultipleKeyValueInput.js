import React, { useEffect, useState } from 'react';

import classes from './MultipleKeyValueInput.module.scss'

import Input from '../../../component/UI/Input/Input';
import Button, { buttonTypes } from '../../../component/UI/Button/Button';

import { Add, Close, Remove } from '@material-ui/icons';

const MultipleKeyValueInput = props => {

  const [values, setValues] = useState({ ...props.value })
  const [addKeyInputControls, setAddKeyInputControls] = useState({
    elementType: 'text',
    elementConfig: {
      type: 'text',
      placeholder: 'عنوان ویژگی'
    },
    value: '',
    validation: {
      required: true,
    },
    valid: false,
    touched: false
  })

  const inputChangeHandler = (e, controlName, values, index, editingKey) => {
    const tempValues = { ...values };
    const tempValueList = [...tempValues[controlName]]
    tempValueList[index] = e.target.value;
    tempValues[controlName] = tempValueList;
    props.changed(tempValues)
  }

  const addItem = (key, values) => {
    if (key.length > 0) {
      props.changed({ ...values, [key]: [''] })
      setAddKeyInputControls(controls => {
        return { ...controls, value: '' }
      });
    }
  }

  const addKeyInputHandler = (e) => {
    setAddKeyInputControls(controls => {
      return { ...controls, value: e.target.value };
    })
  }

  const deleteValueItemHandler = (index, values, key) => {
    const tempValues = { ...values };
    const tempInputList = [...tempValues[key]];
    tempInputList.splice(index, 1);
    tempValues[key] = tempInputList;
    props.changed(tempValues)
  }

  const addValueHandler = (key, values) => {
    props.changed({ ...values, [key]: [...values[key], ''] })
  }
  const removeItemHandler = (key, values) => {
    const tempValues = { ...values };
    delete tempValues[key]
    props.changed(tempValues)
  }

  useEffect(() => {
    setValues(props.value)
  }, [])

  const propertyList = [];
  for (let key in props.value) {
    propertyList.push(
      <div
        key={"multiple_KEY_VALUE_INPUT_item____" + key}
        className={[classes.dFlex, classes.MB3].join(' ')}>
        <div className={classes.RemoveItemContainer}>
          <Button
            type={buttonTypes.primary}
            buttonType="button"
            clicked={() => removeItemHandler(key, props.value)}
            className={classes.removeItemButton}>
            <Remove classes={{ root: classes.removeItemIcon }} />
          </Button>
        </div>
        <span className={classes.InputLabel}>{key}</span>
        <div>
          {
            props.value[key] ? props.value[key].map((item, index) => {
              return <div key={'inputItem' + key + index} className={classes.InputItemContainer}>
                {props.valueCountlimit > 1 ?
                  <button type="button" className={classes.DeleteButton} onClick={() => deleteValueItemHandler(index, props.value, key)}>
                    <Close classes={{ root: classes.DeleteIcon }} />
                  </button> 
                  : ''
                }
                <Input
                  elementType="input"
                  label={key}
                  disableLabel={true}
                  changed={(e) => inputChangeHandler(e, key, props.value, index, false)}
                  value={props.value[key][index]}
                  classes={{ root: classes.ItemValue, label: classes.InputLabel }} />
              </div>
            }) : ''
          }
          {props.valueCountlimit > 1
            ? <Button
              buttonType="button"
              type={buttonTypes.primary}
              className={classes.AddInputButton}
              clicked={() => addValueHandler(key, props.value)}>
              <Add />
            </Button>
            : null}
        </div>
      </div>
    )
  }

  return (
    <div className={classes.MultipleKeyValueInput}>
      {propertyList}
      <div className={classes.dFlex}>
        <span className={classes.InputLabel}>افزودن ویژگی جدید</span>
        <Input
          disableLabel={true}
          classes={{ root: classes.InputElement }}
          elementType={addKeyInputControls.elementType}
          elementConfig={addKeyInputControls.elementConfig}
          value={addKeyInputControls.value}
          changed={addKeyInputHandler} />
        <Button
          buttonType="button"
          type={buttonTypes.primary}
          clicked={() => addItem(addKeyInputControls.value, props.value)}>افرودن ویژگی</Button>
      </div>
    </div>
  );
}

export default MultipleKeyValueInput;