import React from 'react';

const CheckBox  = props => {

  const inputChangeHandler = (e) => {
      if(e.target.checked) {
      props.changed(true)
    } else {
      props.changed(false)
    }
  }
  return <input
    onChange={inputChangeHandler}
    type="checkbox"
    checked={props.value} />;
}

export default CheckBox;