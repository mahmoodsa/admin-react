import React, { useEffect, useState } from 'react';

import { Check, Delete, Edit, InsertDriveFile } from '@material-ui/icons'

import classes from './FileItem.module.scss';

const FileItem  = props => {

  const [fileItemClass, setFileItemClass] = useState(classes.ImageContainer)

  useEffect(() => {    
    setFileItemClass([classes.FileItem , props.active ? classes.active : '', props.fileType === 'file' ? classes.IconItem: ''].join(' '))
  
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.active])

  const itemClickHandler = () => {
    if(props.active) {
      props.clicked(true)
    } else {
      props.clicked(false)
    }
  }

  return (
    <div className={fileItemClass}>
      <button 
        onClick={itemClickHandler}
        className={classes.ImageContainer}>
        { props.fileType === 'file' ? <InsertDriveFile classes={{root: classes.FileIcon}} /> : <img src={props.image} /> }
        <span className={classes.CehckBox}>
          <Check classes={{root: classes.CheckIcon}} />
        </span>
      </button>
      <div className={classes.Caption}>
        <div className={classes.Details}>
          <div 
            data-title={props.title}
            className={classes.FileName}>{props.title}</div>
          <div className={classes.FileSize}>{props.fileSize} kb</div>
        </div>
        <div className={classes.Options}>
          <button className={classes.deleteButton}>
            <Delete 
              onClick={props.deleted}
              classes={{root: classes.DeleteIcon}}/>
          </button>
          <button className={classes.EditButton}>
            <Edit 
              onClick={props.edited}
              className={classes.EditIcon} />
          </button>
        </div>
      </div>
    </div>
  );
}

export default React.memo(FileItem);