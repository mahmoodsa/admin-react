import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import * as actions from '../../../../../../store/actions/index'
import { success } from '../../../../../../utility'

import FileItem from './FileItem/FileItem';
import {ErrorOutline} from '@material-ui/icons'
import Spinner from '../../../../../../component/UI/Spinner/Spinner'; 

import classes from './FileItems.module.scss'

const FileItems  = props => {
  const dispatch = useDispatch();
  const deleteFile = useCallback((id) => dispatch(actions.deleteFile(id)), [dispatch])
  const selectItem = (id) => dispatch(actions.selectItem(id))
  const deSelectItem = (id) => dispatch(actions.deSelectItem(id))

  const fileItems = useSelector(state => state.fileManager.fileItems);
  const error = useSelector(state => state.fileManager.downloadError);
  const selected = useSelector(state => state.fileManager.selected);
  
  const editHandler = useCallback((id) => {
    success('editHandler...: ' + id);
  }, [])

  const activeHandler = (id, active) => {
    if(active) {
      deSelectItem(id)
    } else {
      selectItem(id)
    }
  }

  let content = <div className={classes.CenterBox}>
    <Spinner width="30px" height="30px"/>
  </div>

  if(fileItems && fileItems.length > 0) {
    content = fileItems.map((item, index ) => {
      const active = selected ? selected.indexOf(item.id) > -1 : false;
      return <FileItem
        key={item.id}
        fileType={item.file_type}
        image={item.thumbnail}
        title={item.name}
        fileSize={Math.floor(item.size/ 1000)}
        active={active}
        clicked={(active) => activeHandler(item.id, active)}
        deleted={deleteFile.bind(this, item.id)}
        edited={editHandler.bind(this, item.id)} />
    })
  } else {
    content = <div className={classes.CenterBox}>
        فایلی یافت نشد
      </div>
  }

  if(error) {
    content = <div className={classes.CenterBox}>
      <div className={classes.ErrorBox}>
        <ErrorOutline classes={{root: classes.ErrorIcon}} /> 
        <span>
          {error.statusText}
        </span>
        </div>
    </div>
  }

  return (
    <div className={classes.FileItems}>
        {content}
    </div>
  );
}

export default React.memo(FileItems);