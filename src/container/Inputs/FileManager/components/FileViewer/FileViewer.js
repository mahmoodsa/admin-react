import React from 'react';

import FileItems from './FileItems/FileItems'
import Footer from './Footer/Footer'

import classes from './FileViewer.module.scss';

const FileViewer  = props => {
  return <div className={classes.FileViewer}>
    <FileItems />
    <Footer />
  </div>;
}

export default FileViewer;