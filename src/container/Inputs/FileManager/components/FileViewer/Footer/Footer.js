import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as actions from '../../../../../../store/actions/index';

import Button, { buttonTypes } from '../../../../../../component/UI/Button/Button';
import { AllInclusive } from '@material-ui/icons';

import classes from './Footer.module.scss';

const Footer = props => {
    const dispatch = useDispatch();
    const limit = useSelector(state => state.fileManager.limit);
    const selectedCount = useSelector(state => state.fileManager.selected.length);
    const submitFileManager = () => dispatch(actions.submitFileManager())
    return (
        <div className={classes.Footer}>
            <Button 
                clicked={submitFileManager}
                type={buttonTypes.primary}>تایید</Button>
            <span className={classes.CountContainer} >
                <span className={classes.Count}>{ selectedCount ?? 0 }</span>
              /
              <span className={classes.Count}>{ limit > 1000 ? <AllInclusive /> : limit }</span>
            </span>
        </div>
    );
}

export default Footer;