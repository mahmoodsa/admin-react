import React, { Fragment, useCallback, useEffect, useMemo, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux'

import * as actions from '../../../../../store/actions/index';
import Button, { buttonTypes } from '../../../../../component/UI/Button/Button';

import classes from './UploadButton.module.scss';

const UploadButton  = props => {
  const dispatch = useDispatch();
  const uploadApiDetails = useSelector(state => state.fileManager.fileTypes)
  const activeApiDetails = useMemo(() => {
    const active = uploadApiDetails.filter(item => {
      return item.isActive
    })[0];
    return active;
  }, [uploadApiDetails])
  
  const fileInput = useRef();
  
  const uploadClickHandler = useCallback(() => {
    fileInput.current.click();
  }, [])

  const inputChangeHandler = (e) => {
    if(typeof e.target.files[0] == typeof undefined) {
      return false;
    }
    dispatch(actions.uploadTrigger(
      activeApiDetails,
      e.target.files[0]));
  }
  return (
    <Fragment>
    <Button 
      clicked={uploadClickHandler}
      className={classes.UploadButton}
      type={buttonTypes.primary}>{activeApiDetails.apiDetails.title}</Button>
      {/* {canvas} */}
      <input 
        type="file"
        ref={fileInput}
        onChange={inputChangeHandler}
        className={classes.HiddenInput} />
      </Fragment>
  );
}

export default UploadButton;