import React, { Fragment, useCallback, useMemo, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as actions from '../../../../../store/actions/index';

import Modal from '../../../../../hoc/Modal/Modal';
import FormPage from '../../../../FormPage/FormPage';

import classes from './AltModal.module.scss'

const AltModal  = props => {
  const dispatch = useDispatch();
  const uploadStart = alts => dispatch(actions.uploadStart(alts))
  const uplaodCancel = () => dispatch(actions.uplaodCancel())
  
  const open =  useSelector(state => state.fileManager.altOpen);
  const config = useSelector(state => state.fileManager.altConfig);
  const uploadPayload = useSelector(state => state.fileManager.uploadPayload)
  const uploadApiDetails = useSelector(state => state.fileManager.fileTypes)

  const video = useRef(null);

  const activeApiDetails = useMemo(() => {
    const active = uploadApiDetails.filter(item => {
      return item.isActive
    })[0];
    return active;
  }, [uploadApiDetails])

  const controls ={
    alt_fa: {
      label: 'alt فارسی',
      elementType: 'input',
      elementConfig: {
          type: 'text',
          placeholder: 'alt'
      },
      value: '',
      validation: {
          required: true
      },
      valid: false,
      touched: false
    },
    alt_en: {
        label: 'alt انگلیسی',
        elementType: 'input',
        elementConfig: {
            type: 'text',
            placeholder: 'alt'
        },
        value: '',
        validation: {
            required: true
        },
        valid: false,
        touched: false
    }
  }

  const altSubmitHandler = (controls) => {
    const payload = {
      ...uploadPayload,
      alt_fa: controls['alt_fa'].value,
      alt_en: controls['alt_en'].value,
      api: activeApiDetails
    }
    if(activeApiDetails) {
      payload.vidoeComponent= video.current
    }
    uploadStart(payload)
  }
  
  const deleteSlideHandler = (id) => {
  }

  const altCancelHandler = useCallback(() => {
    uplaodCancel();
  }, [])

  let videoThumbnailSelector = null;
  if (config.uploadVideo) {
    videoThumbnailSelector = <Fragment>
      <div className={classes.AltTitle}>لطفا thumbnail را انتخاب کنید</div>
      <div className={classes.VideoThumbnailBox}>
        <video 
          src={uploadPayload.videoUrl} 
          controls
          ref={video}
          ></video>
      </div>
    </Fragment>
  }
  let content = <Fragment>
      {videoThumbnailSelector}
      <div className={classes.AltTitle}>لطفا alt فایل را وارد کنید</div>
      <FormPage
        // formActions={[{title: 'حذف', submitFunction: deleteSlideHandler,  submitFunctionPayload: '1'}]}
        submitted={formElements => altSubmitHandler(formElements)}
        styles={{
          label: {flex: ' 0 0 30%',whiteSpace: 'no-wrap'}
        }}
        controls={controls} />
    </Fragment>
  

  return (
    <Modal 
      inbox={true} 
      width="400px"
      height="auto"
      open={open}
      modalClosed={altCancelHandler}>
        {content}
      </Modal>
  );
}

export default AltModal;