import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import * as actions from '../../../../../../store/actions/index';

import _FolderItem from './FolderItem';

import { ArrowLeft, Folder } from '@material-ui/icons';
import classes from './FolderItem.module.scss';

const FolderItem = props => {
    const dispatch = useDispatch();
    const folderItemClickHandler = (id) => {
        dispatch(actions.folderItemClicked(id));
    }

    const expandFolderHandler = () => {
        setIsOpen(isOpen => {
            return !isOpen;
        })
    }

    const [isOpen, setIsOpen] = useState(false);

    const folderItemClass = [isOpen ? classes.isOpen : null, classes.FolderItem].join(' ');
    return (
        <li className={folderItemClass}>
            <div className={classes.FolderContent}>
                <button onClick={() => folderItemClickHandler(props.id)}>
                    <Folder
                        classes={{ root: classes.FolderIcon }} />
                    <span className={classes.FolderName}>
                        {props.name}
                    </span>
                </button>
                {props.childs && props.childs.length 
                ? <ArrowLeft classes={{ root: classes.ArrowIcon }} onClick={expandFolderHandler}/> 
                : <div style={{ width: '24px', height: '24px' }}></div>}
            </div>
            <ul className={classes.ChilFoldersList}>
                {props.childs ? props.childs.map(item => {
                    return <_FolderItem
                        key={'folder_item___' + item.id}
                        {...item} />
                }) : null}
            </ul>
        </li>
    )
}
export default FolderItem;