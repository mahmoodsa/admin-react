import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'

import * as actions from '../../../../../store/actions/index';
import FolderItem from './FolderItem/FolderItem';
import Spinner from '../../../../../component/UI/Spinner/Spinner';

import classes from './Sidebar.module.scss';

const Sidebar = props => {
  const dispatch = useDispatch();
  const onSidebarLinkClicked = id => dispatch(actions.sidebarLinkClickHandler(id));

  const fileTypes = useSelector(state => state.fileManager.fileTypes);
  const folders = useSelector(state => state.fileManager.folders);
  const folsersDownloading = useSelector(state => state.fileManager.foldersDownloading);

  return (
    <div className={classes.Sidebar}>
      <div className={classes.ListHeader}>
        دسته بندی
        <span></span>
      </div>
      <ul className={classes.SidebarList}>
        {fileTypes.map((item, index) => {
          return (
            <li
              className={item.isActive ? classes.active : ''}
              key={item.title + index}>
              <button onClick={() => onSidebarLinkClicked(item.id)}>
                {/* {item.icon} */}
                {item.title}
              </button>
            </li>
          )
        })}
      </ul>
      <div className={classes.ListHeader}>
        پوشه ها
        <span></span>
      </div>
      <ul className={classes.FolderList}>
        {
          folsersDownloading 
          ? <Spinner width="20px" height="20px" r="28" strokeWidth="8" />
          : folders.map(item => {
            return <FolderItem
              {...item}
              key={"folder_item___" + item.id}/>
          })
        }
      </ul>
    </div>
  );
}

export default Sidebar;