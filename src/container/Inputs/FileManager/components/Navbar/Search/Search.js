import React from 'react';

import { Search} from '@material-ui/icons'

import classes from './Search.module.scss';

const SearchBox  = props => {
  const boxClass = [classes.SearchBox, props.className].join(' ')
  return (
    <div className={boxClass}>
      <input 
        onChange={props.changed}
        className={classes.SearchInput}
        placeholder="جستجو..."
        type="text" />
        <Search fontSize="small" classes={{root: classes.SearchIcon}}/>
    </div>
  );
}

export default SearchBox;