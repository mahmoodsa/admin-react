import React, { useCallback } from 'react';

import UploadButton from '../UploadButton/UploadButton';
import Search from './Search/Search';

import classes from './Navbar.module.scss'

const Navbar  = props => {
  const clickHandler = useCallback(() => {
    console.log('google');
  }, [])
  const changeHandler = useCallback(() => {
    console.log('changeHandler.....');
  }, [])
  return (
    <div className={classes.Navbar}>
      <div className={classes.NavbarLeft}>
          <UploadButton />
          <Search 
            changed={changeHandler}
            className={classes.SearchBox}/>
      </div>
    </div>
  );
}

export default Navbar;