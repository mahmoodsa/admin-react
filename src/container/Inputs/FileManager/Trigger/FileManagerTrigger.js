import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as actions from '../../../../store/actions/index'

import Button, { buttonTypes } from '../../../../component/UI/Button/Button'
import classes from './FileManagerTrigger.module.scss';

const FileManagerTrigger = props => {
  const dispatch = useDispatch();
  const setLimit = useCallback(() => dispatch(actions.setLimit(props.limit)), [])
  const setReturnUrl = useCallback(() => dispatch(actions.setReturnUrl(props.returnUrl)));
  const FileManagerItems = useSelector(state => state.fileManager.fileItems)

  const submitted = (selectedItems) => {
    props.submitted(selectedItems)
  };
  const openFileManager = () => {
    dispatch(actions.openFileManager(props.value, submitted));
    setLimit();
    setReturnUrl();
  }
  const fileThumbnails = [];
  if (props.value && typeof props.value == 'number') {
    FileManagerItems.forEach(item => {
      if (item.id === props.value) {
        fileThumbnails.push(item.thumbnail)
      }
    });
  } else if (props.value && typeof props.value == 'object') {
    props.value.forEach(value => {
      FileManagerItems.forEach(fileManagerItem => {
        if(typeof value == 'object') {
          if (value.id == fileManagerItem.id) {
            fileThumbnails.push(fileManagerItem.thumbnail);
          }
        } else if (typeof value == 'number') {
          if (value == fileManagerItem.id) {
            fileThumbnails.push(fileManagerItem.thumbnail);
          }
        }
      })
    });
  }
  let fileManagerComponent = null;
  if (fileThumbnails.length > 0) {
    fileManagerComponent = fileThumbnails.map((thumbnail, index) => {
      return <div
        key={"file_manger_component__" + index}
        className={classes.FileManagerTrigger}
        onClick={openFileManager}>
        <img src={thumbnail} />
        <span className={classes.text}>ویرایش</span>
      </div>
    }
    )
  } else {
    fileManagerComponent = <Button
      buttonType="button"
      clicked={openFileManager}
      type={buttonTypes.primary}>
      {props.children}
    </Button>
  }
  return (
    <div className={classes.fileItemsContainer}>
      {
        fileManagerComponent
      }
    </div>
  );
}

export default FileManagerTrigger;