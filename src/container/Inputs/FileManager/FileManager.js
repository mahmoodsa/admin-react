import React, { Fragment, useEffect, useCallback} from 'react';

import * as actions from '../../../store/actions/index';

import Modal from '../../../hoc/Modal/Modal';
import Sidebar from './components/Sidebar/Sidebar';
import FileViewer from './components/FileViewer/FileViewer';
import Navbar from './components/Navbar/Navbar';
import AltModal from './components/AltModal/AltModal';

import classes from './FileManager.module.scss';
import { useDispatch, useSelector } from 'react-redux';

const FileManager  = props => {
  const dispatch = useDispatch();
  const closeFileManager = () => {
    dispatch(actions.closeFileManager())
  }
  const downloadStart = api => dispatch(actions.downloadStart(api));
  
  const fileTypes = useSelector(state => state.fileManager.fileTypes);

  const fileManagerIsOpen = useSelector(state => state.fileManager.isOpen);
  
  const activeFileType = fileTypes.filter(item => item.isActive)[0] ?? false;

  useEffect(() => {
    downloadStart(activeFileType.apiDetails.get)
  }, [activeFileType])
  return (
    <Fragment>
      <Modal 
        style={{root: {zIndex: '11'}}}
        width="850px" 
        height="480px" 
        open={fileManagerIsOpen} 
        modalClosed={closeFileManager}>
        <div className={classes.FileManager}>
          <AltModal />
          <Sidebar />
          <Navbar />
          <FileViewer />
        </div>
      </Modal>
    </Fragment>
  );
}

export default FileManager;