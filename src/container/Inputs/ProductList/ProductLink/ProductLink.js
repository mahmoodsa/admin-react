import React from 'react';

import { NavLink, withRouter } from 'react-router-dom';

import classes from './ProductLink.module.scss';

const ProductLink = props => {

  console.log(props);

  const editProductClickHandler = () => {
    if (props.productType === 'product') {
      props.history.push({
        pathname: '/product/edit',
        state: {
          id: props.value.id,
          product_id: props.value.id
        }
      })
    }
  }

  let link = null;
  let image = null;
  let productPrice = null;
  let selectedProps = [];

  if (props.value && props.value.kay && props.value.kay.image_urls) {
    image = <div className={classes.ImageContainer}>
      <img src={"https://ring-maker.ir" + props.value.kay.image_urls[0]} />
    </div>
  } else if (props.value && props.value.files && (props.value.files.length > 0)) {
    image = <div className={classes.ImageContainer}>
      <img src={props.value.files[0].url} />
    </div>
  }

  if (props.productType == 'product') {
    productPrice = <div className={classes.ProductPriceContainer}>
      <div className={classes.ProductPriceItem}>
        <span className={classes.PriceLabel}>قیمت هنگام خرید :</span>
        {props.value.submitted_price * props.value.order_count}
      </div>
      <div className={classes.ProductPriceItem}>
        <span className={classes.PriceLabel}>قیمت لحضه ای :</span>
        {props.value.value_unit * props.value.order_count}
      </div>
    </div>

    if (props.value && props.value.selected) {
      props.value.selected.forEach((item, index) => {
        selectedProps.push(
          <div
            key={'selected_property' + index + props.value.id}
            className={classes.SelectedProps}>
            <span className={classes.Key}>{Object.keys(item)[0]} :</span>
            <span className={classes.Value}>{item[Object.keys(item)[0]]}</span>
          </div>
        )
      })
    }
  }


  if (props.productType == 'kay') {

    productPrice = <div className={classes.ProductPriceContainer}>
      <div className={classes.ProductPriceItem}>
        <span className={classes.PriceLabel}>قیمت هنگام خرید :</span>
        {props.value.submitted_price * props.value.count}
      </div>
      <div className={classes.ProductPriceItem}>
        <span className={classes.PriceLabel}>قیمت لحضه ای :</span>
        {props.value.kay.price * props.value.count}
      </div>
    </div>

    link = <div className={classes.ProductDetailsContainer}>
      {image}
      <div className={classes.ProductDetails}>
        <div className={classes.Lable}>
          {props.value.kay.title}
        </div>
        {productPrice}
      </div>
      <div className={classes.prodouctLinksContainer}>
        <a
          href={"http://localhost:8000/شخصی_سازی?sku=" + props.value.hash}
          target="blank"
          className={classes.showProductLink}>نمایش محصول در سایت</a>
      </div>
    </div>
  } else {
    link = <div className={classes.ProductDetailsContainer}>
      {image}
      <div className={classes.ProductDetails}>
        <div className={classes.Lable}>
          {props.value.product_name.fa}
        </div>
        <div>
          {productPrice}
          {selectedProps}
        </div>
      </div>
      <div className={classes.prodouctLinksContainer}>
        <button className={classes.editProudct} onClick={editProductClickHandler}>ویرایش محصول</button>
        <a
          href={"http://localhost:8000/محصول/" + props.value.slug.fa}
          target="blank"
          className={classes.showProductLink}>نمایش محصول در سایت</a>
      </div>
    </div>
  }

  return <div className={classes.ProductLinkContainer}>
    {link}
  </div>;
}

export default withRouter(ProductLink);