import React from 'react';

import classes from './ProductList.module.scss';

import ProductLink from './ProductLink/ProductLink'

const ProductList = props => {

  let productsList = null;

  if (props.value && props.value.length > 0) {
    productsList = props.value.map((producdtItem, index) => {
      return <ProductLink 
        key={`product-link${producdtItem.hash ?? producdtItem.product_code + index}`} 
        productType={props.elementConfig.productType}
        value={producdtItem} />
    })
  }

  return <div className={classes.ProductList}>
    {productsList}
  </div>;
}

export default ProductList;