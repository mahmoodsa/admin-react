import React from 'react';

import Button, { buttonTypes } from '../../../component/UI/Button/Button';

import { Add, Close } from '@material-ui/icons';

import classes from './KeyValueInput.module.scss';

const KeyValueInput = props => {
    const inputChangeHandler = (e, index, key, values) => {
        const tempValues = { ...values };
        const tempValue = [...tempValues[key]]
        tempValue[index] = e.target.value;
        tempValues[key] = tempValue;
        props.changed(tempValues)
    }

    const addInputField = (key, values) => {
        const tempValues = { ...values };
        const tempValue = [...tempValues[key]];
        tempValue.push('');
        tempValues[key] = tempValue;
        props.changed(tempValues);
    }

    const InputDeleteHandler = (index, key, values) => {
        const tempValues = { ...values };
        const tempValue = [...tempValues[key]];
        tempValue.splice(index, 1);
        tempValues[key] = tempValue;
        props.changed(tempValues);
    }

    const fields = [];
    for (let key in props.value) {
        const KeyValueInput = [];
        if (props.value[key] && props.value[key].length > 0) {
            props.value[key].forEach((item, index) => {
                KeyValueInput.push(
                    <div
                        key={'input____key_value__input_field_contianer' + key + "___" + index}
                        className={classes.inputContainer}>
                        <input
                            key={'input____key_value__input_field_' + key + "___" + index}
                            type="text"
                            value={item}
                            onChange={e => inputChangeHandler(e, index, key, props.value)}
                            className={classes.InputElement} />
                        <button
                            type="button"
                            key={'input____key_value__input_field_delete___button___' + key + "___" + index}
                            className={classes.DeleteButton}
                            onClick={() => InputDeleteHandler(index, key, props.value)}>
                            <Close classes={{ root: classes.CloseIcon }} />
                        </button>
                    </div>
                )
            })
            KeyValueInput.push(
                <Button
                    key={"add__input_button___" + key}
                    type={buttonTypes.primary}
                    className={classes.AddButton}
                    buttonType="button"
                    clicked={() => addInputField(key, props.value)}>
                    <Add />
                </Button>
            )
        }
        fields.push(
            <div className={classes.Input} key={'input___fields___' + key}>
                <label className={classes.Label}> {key} </label>
                {KeyValueInput}
            </div>)
    }

    return <div>{fields}</div>;
}
export default KeyValueInput;