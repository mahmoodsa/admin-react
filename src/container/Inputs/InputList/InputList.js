import React, { useEffect } from 'react';

import Button, { buttonTypes } from '../../../component/UI/Button/Button';
import { Close, Add } from '@material-ui/icons';

import classes from './InputList.module.scss';

const InputList = props => {


    const inputChangeHandler = (e, index, values) => {
        const tempValues = [...values];
        const tempValue = [...tempValues[index]]
        tempValues[index] = e.target.value;
        props.changed(tempValues)
    }

    const addInputField = (values) => {
        const tempValues = [...values];
        tempValues.push('');
    }

    const InputDeleteHandler = (index, values) => {
        const tempValues = [...values];
        tempValues.splice(index, 1);
        props.changed(tempValues);
    }

    useEffect(() => {
        if (!props.value || props.value.length <= 0) {
            addInputField('')
        }
    }, [])
    const inputList = [];
    if (props.value && props.value.length > 0) {
        props.value.forEach((item, index) => {
            inputList.push(
                <div className={classes.InputContainer}
                    key={"input____list___input_container____" + index}>
                    <input
                        key={"input____list___input_field____" + index}
                        type="text"
                        value={item}
                        className={classes.InputElement}
                        onChange={e => inputChangeHandler(e, index, props.value)} />
                    <button
                        type="button"
                        key={'input____list__input_field_delete___button___' + index}
                        className={classes.DeleteButton}
                        onClick={() => InputDeleteHandler(index, props.value)}>
                        <Close classes={{ root: classes.CloseIcon }} />
                    </button>
                </div>
            )
        })
    }
    inputList.push(
        <Button
            key={"add__input_button___"}
            type={buttonTypes.primary}
            className={classes.AddButton}
            buttonType="button"
            clicked={() => addInputField(props.value)}>
            <Add />
        </Button>
    )

    return (
        <div className={classes.Input}>
            { inputList}
        </div>
    );
}

export default InputList;