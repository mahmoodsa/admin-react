import React, { useCallback, useRef } from 'react';
import { withRouter } from 'react-router';
import axios from '../../../axios';

import Button, { buttonTypes } from '../../../component/UI/Button/Button';

import classes from './FileUploader.module.scss';

const FileUploader = (props) => {
    const inputref = useRef(null);

    const uploadButtonClickHandler = useCallback(() => {
        inputref.current.click();
    }, [])

    const fileInputChangeHandler = (e) => {
        if (typeof e.target.files[0] == typeof undefined) {
            return false;
        }
        const formData = new FormData();
        formData.append(props.elementConfig.apiConfig.uploadFileName, e.target.files[0])
        formData.append(props.elementConfig.apiConfig.uploadIdName, props.location.state[props.elementConfig.apiConfig.uploadIdName])
        axios.post(props.elementConfig.apiConfig.url, formData)
            .then(res => {
                if(res.data.success) {
                    console.log('success');
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    return (
        <React.Fragment>
            <input
                ref={inputref}
                type="file"
                className={classes.Hidden}
                onChange={e => fileInputChangeHandler(e)} />
            <Button
                clicked={uploadButtonClickHandler}
                type={buttonTypes.primary}
                buttonType="button">
                {
                    props.elementConfig.placeholder ? props.elementConfig.placeholder : 'آپلود فایل'
                }
            </Button>
        </React.Fragment>
    )
}

export default withRouter(FileUploader)