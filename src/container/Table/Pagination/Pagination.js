import React, { useEffect, useState } from 'react';

import classes from './Pagination.module.scss';

import { ChevronRight, ChevronLeft } from '@material-ui/icons'

const Pagination = props => {

    const paginationItems = [];
    for (let index = 0; index < props.pagesCount; index++) {
        paginationItems.push(
            <li key={index}>
                <button onClick={props.changed.bind(this, index)}>{index+1}</button>
            </li>);
    }
    let [prevPageClasses, setPrevPageClasses] = useState('');
    let [nextPageClasses, setNextPageClasses] = useState('')
    useEffect(() => {
        if(props.selectedPageIndex <=1) {
            setPrevPageClasses(classes.disabled);
        }
        if(props.selectedPageIndex >= props.pagesCount) {
            setNextPageClasses(classes.disabled);
        }
    }, [props.selectedPageIndex])
    return (
        <div className={classes.Pagination}>
            <ul>
                <li>
                    <button className={prevPageClasses}>
                        <ChevronRight fontSize="small" classes={{root: classes.SmallIcon}}/>
                        قبل
                    </button>
                </li>
                {paginationItems}
                <li>
                    <button className={nextPageClasses}>
                        بعد
                        <ChevronLeft fontSize="small" classes={{root: classes.SmallIcon}} />
                    </button>
                </li>
            </ul>
        </div>
    );
}

export default React.memo(Pagination);