import React from 'react';
import Input from '../../../component/UI/Input/Input';

import classes from './Filters.module.scss';

const Filters = props => {
    const filtersList = [];
    for(let key in props.controls) {
        const controlItem = {...props.controls[key]};
        filtersList.push(
            <Input 
                key={'filter__item_'+ key}
                classes={{root: classes.InputRoot, inputElement: classes.InputElement}}
                {...controlItem} 
                changed={e => props.changed(e, key)} disableLabel inlineInput />
        )
    }
    return (
        <div className={classes.FiltersContainer}>
            {filtersList}
        </div>
    );
}

export default Filters;