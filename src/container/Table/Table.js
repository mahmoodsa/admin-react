import React, { Fragment, useEffect, useState, useRef, useCallback } from 'react';
import axios from '../../axios';
import { NavLink } from 'react-router-dom';

import LayoutContent from '../../hoc/LayoutContent/LayoutContent';
import Pagination from './Pagination/Pagination'

import { Delete, Edit, Check, Visibility, VisibilityOff, Block, Reply } from '@material-ui/icons'

import classes from './Table.module.scss';
import Filters from './Filters/Filters';

const TablePage = props => {
    // const [paginationStart, setPaginationStart] = useState(0);
    const [itemsCount, setItemsCount] = useState(0);
    const [paginationOffset, setPaginationOffset] = useState(10);
    const [pagesCount, setPagesCount] = useState(0);
    const [selectedPage, setSelectedPage] = useState(0);
    const [data, setData] = useState([]);
    const [head, setHead] = useState([]);
    const [pages, setPages] = useState([]);
    const [body, setBody] = useState([]);
    const [filters, setFilters] = useState({});

    
    useEffect(() => {
        setFilters(props.controls.filters);
        axios.get(props.api.route)
            .then(res => {
                const tempPages = [[]];
                let pageNumber = 0;
                let itemCount = 0;
                if (res.data.data) {
                    setData(res.data.data);
                }
                res.data.data.forEach((rowItem, index) => {
                    if (itemCount >= paginationOffset) {
                        pageNumber++;
                        tempPages[pageNumber] = [];
                        itemCount = 0;
                    }
                    let rowActions = null
                    if (props.controls.actions) {
                        const actionItems = []
                        actionLoop: for (let key in props.controls.actions) {
                            const item = props.controls.actions[key];

                            if (key === 'delete') {
                                actionItems.push(
                                    <button
                                        key={'deleteButton____' + itemCount}
                                        className={classes.ActionButton}
                                        onClick={deleteHandler.bind(this, rowItem.id)}>
                                        <Delete classes={{ root: classes.DeleteIcon }} />
                                    </button>)
                            }
                            else if (key === 'edit') {
                                const editPayload = {};
                                item.payload.forEach(key => {
                                    editPayload[key.stateName] = rowItem[key.name];
                                })
                                actionItems.push(
                                    <NavLink
                                        key={'editButton____' + itemCount}
                                        className={classes.ActionButton}
                                        to={{
                                            pathname: item.redirect,
                                            state: {
                                                id: rowItem.id,
                                                ...editPayload
                                            }
                                        }}>
                                        <Edit classes={{ root: classes.EditIcon }} />
                                    </NavLink>)
                            }
                            else if (key === 'reply') {
                                const replyPayload = {};
                                item.payload.forEach(key => {
                                    replyPayload[key.stateName] = rowItem[key.name];
                                })
                                actionItems.push(
                                    <NavLink
                                        key={'replyButton____' + itemCount}
                                        className={classes.ActionButton}
                                        to={{
                                            pathname: item.redirect,
                                            state: {
                                                ...replyPayload
                                            }
                                        }}>
                                        <Reply classes={{ root: classes.ReplyIcon }} />
                                    </NavLink>)
                            }
                            else if (key === 'confirm') {
                                if (item.showConfirm) {
                                    for (let showConfirmKey in item.showConfirm) {
                                        if (rowItem[showConfirmKey] == item.showConfirm[showConfirmKey]) {
                                            continue actionLoop;
                                        }
                                    }
                                }
                                actionItems.push(
                                    <button
                                        key={'confirmButton____' + itemCount}
                                        className={classes.ActionButton}
                                        onClick={confirmHandler.bind(this, rowItem.id)}>
                                        <Block classes={{ root: classes.UnConfirmIcon }} />
                                    </button>)
                            }
                            else if (key === 'unConfirm') {
                                if (item.showUnConfirm) {
                                    for (let showUnConfirmKey in item.showUnConfirm) {
                                        if (rowItem[showUnConfirmKey] == item.showUnConfirm[showUnConfirmKey]) {
                                            continue actionLoop;
                                        }
                                    }
                                }
                                actionItems.push(
                                    <button
                                        key={'unConfirmButton____' + itemCount}
                                        className={classes.ActionButton}
                                        onClick={unConfirmHandler.bind(this, rowItem.id)}>
                                        <Check classes={{ root: classes.ConfirmIcon }} />
                                    </button>)
                            }
                            else if (key == 'active') {
                                actionItems.push(
                                    <button
                                        key={'confirmButton____' + itemCount}
                                        className={classes.ActionButton}
                                        onClick={activeHandler.bind(this, rowItem.id, rowItem.active)}>
                                        {
                                            rowItem.active
                                                ? <Visibility classes={{ root: classes.ConfirmIcon }} />
                                                : <VisibilityOff classes={{ root: classes.ConfirmIcon }} />
                                        }
                                    </button>)
                            }
                        }
                        rowActions = <td key={'td____actions__' + itemCount}>
                            {actionItems}
                        </td>
                    }
                    tempPages[pageNumber].push(
                        <tr key={rowItem.id + "_____" + itemCount} id={rowItem.id}>
                            <td key={'id____' + itemCount}>{index + 1}</td>
                            {
                                props.controls.head.map(key =>
                                    <td
                                        key={"td__" + key.name + itemCount}>
                                        {rowItem[key.name]}
                                    </td>
                                )
                            }
                            {rowActions}
                        </tr>)
                    itemCount++;
                })
                setPages(tempPages);
                setItemsCount(res.data.data.length);
                const addToPageNumber = (res.data.data.length % paginationOffset) !== 0 ? 1 : 0;
                setPagesCount(Math.floor(res.data.data.length / paginationOffset) + addToPageNumber)
                setHead([
                    <th
                        width="50"
                        style={{ textAlign: 'center' }}
                        key="th___index">
                        ردیف
                    </th>,

                    props.controls.head.map(item =>
                        <th
                            width={item.config.width}
                            style={{ textAlign: item.config.textAlign ?? 'center' }}
                            key={"th___" + item.name}>
                            {item.title}
                        </th>),

                    props.controls.actions
                        ? <th
                            width="200"
                            key="th__acitons"
                            style={{ textAlign: 'center' }}>
                            اقدامات
                         </th>
                        : null
                ])
                setBody(tempPages[selectedPage]);
            })
            .catch(err => {
                console.log(err);
            })
    }, [])

    useEffect(() => {
        setBody(pages[selectedPage]);
    }, [selectedPage, pages]);

    // when data is changed some how
    useEffect(() => {
        const tempPages = [[]];
        let pageNumber = 0;
        let itemCount = 0;
        data.forEach((rowItem, index) => {
            if (rowItem.disabled) {
                return false;
            }
            if (itemCount >= paginationOffset) {
                pageNumber++;
                tempPages[pageNumber] = [];
                itemCount = 0
            }
            let rowActions = null;
            if (props.controls.actions) {
                const actionItems = []
                actionLoop: for (let key in props.controls.actions) {
                    const item = props.controls.actions[key];

                    if (key === 'delete') {
                        actionItems.push(
                            <button
                                key={'deleteButton____' + itemCount}
                                className={classes.ActionButton}
                                onClick={deleteHandler.bind(this, rowItem.id)}>
                                <Delete classes={{ root: classes.DeleteIcon }} />
                            </button>)
                    }

                    else if (key === 'edit') {
                        const editPayload = {};
                        item.payload.forEach(key => {
                            editPayload[key.stateName] = rowItem[key.name];
                        })
                        actionItems.push(
                            <NavLink
                                key={'editButton____' + itemCount}
                                className={classes.ActionButton}
                                to={{
                                    pathname: item.redirect,
                                    state: {
                                        id: rowItem.id,
                                        ...editPayload
                                    }
                                }}>
                                <Edit classes={{ root: classes.EditIcon }} />
                            </NavLink>)
                    }

                    else if (key === 'reply') {
                        const replyPayload = {};
                        item.payload.forEach(key => {
                            replyPayload[key.stateName] = rowItem[key.name];
                        })
                        actionItems.push(
                            <NavLink
                                key={'replyButton____' + itemCount}
                                className={classes.ActionButton}
                                to={{
                                    pathname: item.redirect,
                                    state: {
                                        ...replyPayload
                                    }
                                }}>
                                <Reply classes={{ root: classes.ReplyIcon }} />
                            </NavLink>)
                    }
                    else if (key === 'confirm') {
                        if (item.showConfirm) {
                            for (let showConfirmKey in item.showConfirm) {
                                if (rowItem[showConfirmKey] == item.showConfirm[showConfirmKey]) {
                                    continue actionLoop;
                                }
                            }
                        }
                        actionItems.push(
                            <button
                                key={'confirmButton____' + itemCount}
                                className={classes.ActionButton}
                                onClick={confirmHandler.bind(this, rowItem.id)}>
                                <Block classes={{ root: classes.UnConfirmIcon }} />
                            </button>)
                    }

                    else if (key === 'unConfirm') {
                        if (item.showUnConfirm) {
                            for (let showUnConfirmKey in item.showUnConfirm) {
                                if (rowItem[showUnConfirmKey] == item.showUnConfirm[showUnConfirmKey]) {
                                    continue actionLoop;
                                }
                            }
                        }
                        actionItems.push(
                            <button
                                key={'unConfirmButton____' + itemCount}
                                className={classes.ActionButton}
                                onClick={unConfirmHandler.bind(this, rowItem.id)}>
                                <Check classes={{ root: classes.ConfirmIcon }} />
                            </button>)
                    } else if (key == 'active') {
                        actionItems.push(<button key={'confirmButton____' + itemCount} className={classes.ActionButton} onClick={activeHandler.bind(this, rowItem.id, rowItem.active)}>
                            {
                                rowItem.active ? <Visibility classes={{ root: classes.ConfirmIcon }} /> : <VisibilityOff classes={{ root: classes.ConfirmIcon }} />
                            }
                        </button>)
                    }
                }
                rowActions = <td key={'td____actions__' + itemCount} >
                    {actionItems}
                </td>
            }
            tempPages[pageNumber].push(
                <tr key={rowItem.id + "_____" + itemCount} id={rowItem.id}>
                    <td key={'id____' + itemCount}>{index + 1}</td>
                    {
                        props.controls.head.map(key =>
                            <td
                                key={"td__" + key.name + itemCount}>
                                {rowItem[key.name]}
                            </td>
                        )
                    }
                    {rowActions}
                </tr>)
            itemCount++;

        })
        setPages(tempPages);
        let length = 0;
        if (data.length > 0) {
            data.forEach(item => {
                if (!item.disabled) {
                    length++;
                }
            })
        }
        setItemsCount(length);
        const addToPageNumber = (length % paginationOffset) !== 0 ? 1 : 0;
        setPagesCount(Math.floor(length / paginationOffset) + addToPageNumber);
        setBody(tempPages[selectedPage]);
    }, [data])


    const paginationClickHandler = useCallback((pageNumber) => {
        setSelectedPage(pageNumber);
    })

    const deleteHandler = (id) => {
        const payload = {};
        props.controls.actions.delete.payload.forEach(item => {
            payload[item] = id;
        })
        axios.post(props.controls.actions.delete.apiRoute, payload)
            .then(res => {
                if (res.data.success) {
                    setData(data => {
                        const tempData = [...data]
                        let newData = [];
                        tempData.forEach((dataItem, index) => {
                            newData = tempData.filter(item => {
                                return item.id !== id
                            })
                        })
                        return newData;
                    })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    const activeHandler = (id, active) => {
        const payload = {};
        payload.confirm = 1;
        props.controls.actions.active.payload.forEach(item => {
            payload[item] = id;
        })
        let apiRoute = '';
        if (active) {
            apiRoute = props.controls.actions.active.diactiveApiRoute
        } else {
            apiRoute = props.controls.actions.active.activeApiRoute;
        }
        axios.post(apiRoute, payload)
            .then(res => {
                if (res.data.success) {
                    setData(data => {
                        let tempData = [...data];
                        tempData = tempData.map(item => {
                            if (item.id === id) {
                                return {
                                    ...item,
                                    active: !item.active
                                }
                            }
                            return item;
                        })
                        return tempData;
                    })
                }
            })
            .catch(err => {

            })

    }

    const confirmHandler = (id) => {
        const payload = {};
        payload.confirm = 1;
        props.controls.actions.confirm.payload.forEach(item => {
            payload[item] = id
        })
        axios.post(props.controls.actions.confirm.apiRoute, payload)
            .then(res => {
                if (res.data.success) {
                    setData(data => {
                        let tempDataIndex = 0;
                        const tempData = [...data];
                        const tempDataItem = tempData.filter((dataItem, dataIndex) => {
                            if (dataItem.id === id) {
                                tempDataIndex = dataIndex;
                                return { ...dataItem };
                            }
                        })[0];
                        tempDataItem.confirm = 1;
                        tempData[tempDataIndex] = tempDataItem;
                        return tempData;
                    })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    const unConfirmHandler = (id) => {
        const payload = {};
        payload.confirm = 0;
        props.controls.actions.confirm.payload.forEach(item => {
            payload[item] = id
        })
        axios.post(props.controls.actions.confirm.apiRoute, payload)
            .then(res => {
                if (res.data.success) {
                    setData(data => {
                        let tempDataIndex = 0;
                        const tempData = [...data];
                        const tempDataItem = tempData.filter((dataItem, dataIndex) => {
                            if (dataItem.id === id) {
                                tempDataIndex = dataIndex;
                                return { ...dataItem };
                            }
                        })[0];
                        tempDataItem.confirm = 0;
                        tempData[tempDataIndex] = tempDataItem;
                        return tempData;
                    })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }
    const filtersChangeHandler = (e, key) => {
        setFilters(filters => {
            const tempFilters = { ...filters };
            const tempFilterItem = { ...tempFilters[key] };
            tempFilterItem.value = e.target.value;
            tempFilters[key] = tempFilterItem;
            setData(data => {
                let tempData = [...data];
                tempData = tempData.map(item => {
                    const filters = [];
                    for (let filterItemKey in tempFilters) {
                        if (item[filterItemKey] === null) {
                            filters.push(true);
                        }
                        if (item[filterItemKey].toString().indexOf(tempFilters[filterItemKey].value) <= -1) {
                            filters.push(false);
                        } else {
                            filters.push(true);
                        }
                    }
                    let disabled = true;
                    filters.forEach(item => {
                        disabled = disabled && item;
                    })
                    item.disabled = !disabled
                    return item
                })
                return tempData;
            })
            return tempFilters;

        })
    }


    return (
        <Fragment>
            <LayoutContent>
                <Filters controls={filters} changed={filtersChangeHandler} />
                <table className={classes.Table}>
                    <thead>
                        <tr>{head}</tr>
                    </thead>
                    <tbody>{body}</tbody>
                </table>
            </LayoutContent>
            <Pagination
                pagesCount={pagesCount}
                changed={paginationClickHandler}
                selectedPageIndex={selectedPage} />
        </Fragment>
    );
}

export default TablePage;