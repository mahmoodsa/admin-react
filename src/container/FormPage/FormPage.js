import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Redirect, withRouter } from 'react-router';

import axios from '../../axios';

import LayoutContent from '../../hoc/LayoutContent/LayoutContent';
import Input, { elementTypes } from '../../component/UI/Input/Input';
import Button, { buttonTypes } from '../../component/UI/Button/Button'

import classes from './FormPage.module.scss';
import { error, success } from '../../utility';
// import BlockItem from '../BlockList/BlockItem/BlockItem';

const FormPage = props => {
  const [controls, setControls] = useState({ ...props.controls })
  const [controlSections] = useState(props.controlSections ? { ...props.controlSections } : false);
  const [redirect, setRedirect] = useState(null);
  
  const formatFilemanagerData = (data) => {
    if (!data) {
      return data;
    }
    if (typeof data === 'object') {
      return data.map(item => {
        if (typeof item === 'object') {
          return item.id;
        }
        return item;
      })
    }
    else {
      return data;
    }
  }

  const checkValidity = useCallback((value, rules, elementType) => {
    let isValid = true;
    let validationMessage = null;
    if ((elementType === elementTypes.fileManager)
      || (elementType === elementTypes.slideshow)
      || (elementType === elementTypes.MULTIPLE_KEY_VALUE_INPUT)
      || (elementType === elementTypes.KEY_VALUE_INPUT)
      || (elementType === elementTypes.INPUT_LIST)
      || (elementType === elementTypes.MULTIPLE_ASYNC_DROPDOWN)
      || (elementType === elementTypes.SELECTABLE_PROPERTY_SELECTOR)
      || (elementType === elementTypes.checkbox)
      || (elementType === elementTypes.DATE_PICKER)
      || (elementType === elementTypes.ITEM_SELECTOR_TABLE)
      || (elementType === elementTypes.itemSelector)
      || (elementType === elementTypes.FILE_UPLOADER)
      || (elementType === elementTypes.CK_EDITOR)
    ) {
      if (!rules) {
        return [true ,''];
      }
      if (rules.required) {
        if (typeof value == 'string') {
          isValid = value.trim() !== '' && isValid;
        } else if (typeof value == 'array' || typeof value == 'object') {
          isValid = value.length > 0 && isValid;
        }
        if (rules.minLength) {
          isValid = value.length >= rules.minLength && isValid;
        }

        if (rules.maxLength) {
          isValid = value.length <= rules.maxLength && isValid;
        }
      }
    } else {
      if (!rules) {
        return [true , ''];
      }
      if (rules.maxLength) {
        if(value.length >= rules.maxLength) {
          isValid = false;  
          validationMessage = `این فیلد حداکثر می تواند ${rules.maxLength} کارکتر باشد`
        }
      }
      if (rules.minLength) {
        if(value.length <= rules.minLength) {
          isValid =  false;
          validationMessage = `این فیلد حداقل باید ${rules.minLength} کارکتر باشد`
        }
      }
      if (rules.required) {
        if(value.trim().length == 0) {
          isValid =  false;
          validationMessage = 'این فیلد اجباری است'
        }
      }
    }
    return [isValid, validationMessage];

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const inputChangedHandler = (e, controlName) => {
    if (typeof e.target !== typeof undefined) {
      setControls(controls => {
        let [isValid , validationMessage] = checkValidity(e.target.value, controls[controlName].validation, controls[controlName].elementType)
        const updatedControls = {
          ...controls,
          [controlName]: {
            ...controls[controlName],
            value: e.target.value,
            valid: isValid,
            validationMessage: validationMessage,
            touched: true
          }
        }
        return updatedControls;
      });
    } else {
      setControls(controls => {
        const updatedControls = {
          ...controls,
          [controlName]: {
            ...controls[controlName],
            value: e,
            valid: checkValidity(e, controls[controlName].validation, controls[controlName].elementType),
            touched: true
          }
        }
        return updatedControls;
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }

  let form = [];
  for (let key in controls) {
    const formElement = controls[key];
    form.push(<Input
      styles={props.styles}
      key={key}
      label={formElement.label}
      elementType={formElement.elementType}
      elementConfig={formElement.elementConfig}
      value={formElement.value}
      invalid={!formElement.valid}
      shouldValidate={formElement.validation}
      validationMessage={formElement.validationMessage}
      touched={formElement.touched}
      changed={(e) => inputChangedHandler(e, key)} />)
  }

  let formActions = useMemo(() => {
    if (props.formActions) {
      return props.formActions.map((item, index) => {
        return <Button
          key={"form_action" + "__" + index + "__" + item.title}
          buttonType="button"
          clicked={item.submitFunction(item.submitFunctionPayload)}
          type={buttonTypes.primary}>
          {item.title}
        </Button>
      })
    }
    return [];
  }, []);

  const formSubmitHandler = (e) => {
    e.preventDefault();
    if (props.api && props.api.route) {
      const payload = {};
      controlsLoop: for (let key in controls) {
        if (controls[key].elementConfig.nullable) {
          if ((controls[key].value === null) || (controls[key].value === '')) {
            continue controlsLoop;
          }
        }
        if (controls[key].elementType === 'fileManager') {
          if (!controls[key].touched) {
            payload[controls[key].name] = formatFilemanagerData(controls[key].value);
            continue;
          }
        }
        if (controls[key].name) {
          payload[controls[key].name] = controls[key].value;
        } else {
          payload[key] = controls[key].value;
        }
      }
      if (props.hasId) {
        if (
          (typeof props.location.state !== typeof undefined)
          && (props.location.state !== null)) {
          props.api.get.payload.forEach(item => {
            payload[item] = props.location.state[item];
          })
        }
        if (props.api.post && props.api.post.payload) {
          props.api.post.payload.forEach(item => {
            payload[item] = props.location.state[item];
          })
        }
      }
      axios.post(props.api.route, payload)
        .then(res => {
          if (props.callBackFunction) {
            props.callBackFunction(res.data);
          }
          if (props.redirect) {
            setRedirect(<Redirect to={props.redirect} />)
          }
        })
        .catch(err => {
          console.log(err);
        })
    }
    if (props.submitted) {
      props.submitted(controls)
    }
  }

  useEffect(() => {
    if (props.loadAsyncData) {
      const payload = {};
      if (props.hasId) {
        if (
          (typeof props.location.state == typeof undefined)
          || (props.location.state == null)
        ) {
          return;
        }
        props.api.get.payload.forEach(item => {
          payload[item] = props.location.state[item]
        })
      }
      axios({
        method: props.loadAsyncDataConfig.method,
        url: props.loadAsyncDataConfig.url,
        data: payload
      })
        .then(res => {
          if (controlSections) {
            setControls(controls => {
              const tempControls = { ...controls };
              for (let index in controlSections) {
                const controlKey = controlSections[index];
                if (res.data.data[controlKey]) {
                  for (let key in tempControls) {
                    const tempControl = { ...tempControls[key] }
                    tempControl.value = res.data.data[controlKey][key] ?? '';
                    tempControls[key] = tempControl;
                  }
                }
                return tempControls;
              }
            })
          }
          else if (props.loadAsyncDataConfig.resDataStructure) {
            setControls(controls => {
              const tempControls = { ...controls };
              props.loadAsyncDataConfig.resDataStructure.forEach((item, index) => {
                const controlKey = item.name;
                const cotnrolType = item.type;
                if (cotnrolType === 'slider') {
                  if (res.data.data[controlKey]) {
                    const tempControl = { ...tempControls[controlKey] }
                    tempControl.value = res.data.data[controlKey] ?? '';
                    tempControls[controlKey] = tempControl;
                  }
                }
                else {
                  if (res.data.data[controlKey]) {
                    console.log(controlKey);
                    console.log(res.data.data[controlKey]);
                    for (let key in res.data.data[controlKey]) {
                      const tempControl = { ...tempControls[key] }
                      if(Object.keys(tempControl).length > 0) {
                        tempControl.value = res.data.data[controlKey][key] ?? '';
                        tempControls[key] = tempControl;
                      }
                    }
                  }
                }
              })
              return tempControls
            })
          }
          else {
            setControls(controls => {
              const tempControls = { ...controls }
              for (let key in tempControls) {
                if (res.data.data[key]) {
                  const tempControl = { ...tempControls[key] }
                  tempControl.value = res.data.data[key] ?? '';
                  tempControls[key] = tempControl;
                }
              }
              return tempControls;
            });
          }
        })
        .catch(err => {
          console.log(err);
        })
    }
  }, [])
  useEffect(() => {
    if (props.asyncControls) {
      setControls(controls => {
        const tempControls = { ...controls }
        for (let key in tempControls) {
          if (props.asyncControls[key]) {
            const tempControl = { ...tempControls[key] }
            tempControl.value = props.asyncControls[key].value ?? '';
            tempControls[key] = tempControl;
          }
        }
        return tempControls;
      });
    }
  }, [props.asyncControls])

  return <LayoutContent>
    <form onSubmit={formSubmitHandler}>
      {redirect}
      {form}
      <div className={classes.SubmitContainer}>
        <Button type={buttonTypes.primary}>{props.submitButtonText ? props.submitButtonText : 'ثبت'}</Button>
        {formActions}
      </div>
    </form>
  </LayoutContent>;
}
export default withRouter(FormPage);