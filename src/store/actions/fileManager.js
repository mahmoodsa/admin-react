import axios from '../../axios';
import { createImageThumbnail, createVideoUrl, createVideoThumbanil } from '../utility';
import * as actionTypes from './actionTypes';

export const sidebarLinkClickHandler = (id) => {
  return {
    type: actionTypes.SIDEBAR_LINK_CLICKED,
    id: id
  }
}
export const uploadTrigger = (activeFileType, file) => {
  return dispatch => {
    if (activeFileType.fileType === 'image') {
      createImageThumbnail(file)
        .then(res => {
          return dispatch({
            type: actionTypes.UPLOAD_TRIGGER,
            result: res,
            fileType: 'image'
          })
        })
    } else if (activeFileType.fileType === 'video') {
      createVideoUrl(file)
        .then(res => {
          dispatch({
            type: actionTypes.UPLOAD_TRIGGER,
            result: res,
            fileType: 'video'
          })
        })
    } else {
      dispatch({
        type: actionTypes.UPLOAD_TRIGGER,
        fileType: activeFileType.fileType,
        file
      })
    }
  }
}
export const uplaodCancel = () => {
  return {
    type: actionTypes.UPLOAD_CANCEL
  }
}
export const uploadStart = (payload) => {
  return dispatch => {
    if (payload.api.fileType === 'image') {
      dispatch({ type: actionTypes.UPLOAD_START });
      const formData = new FormData();
      formData.append('thumbnail_image', payload.thumbnail_image, payload.name)
      formData.append('image', payload.image, payload.name)
      formData.append('alt_fa', payload.alt_fa)
      formData.append('alt_en', payload.alt_en)
      axios.post('/api/admin/send_image', formData)
        .then(res => {
          dispatch(uploadSuccess());
        })
        .catch(err => {
          dispatch(uploadFail());
        })
    } else if (payload.api.fileType === 'video') {
      createVideoThumbanil(payload.vidoeComponent)
        .then(res => {
          const formData = new FormData();
          formData.append('thumbnail_video', res.thumbnail, payload.videoFile.name)
          formData.append('video', payload.videoFile, payload.videoFile.name)
          formData.append('alt_fa', payload.alt_fa)
          formData.append('alt_en', payload.alt_en)
          axios.post('/api/admin/send_video', formData)
            .then(res => {
              dispatch(uploadSuccess());
            })
            .catch(err => {
              dispatch(uploadFail());
            })
        })
    } else {
      const formData = new FormData();
      formData.append('file', payload.file, payload.file.name)
      formData.append('alt_fa', payload.alt_fa)
      formData.append('alt_en', payload.alt_en)
      axios.post(payload.api.apiDetails.post, formData)
        .then(res => {
          dispatch(uploadSuccess());
        })
        .catch(err => {
          dispatch(uploadFail());
        });
    }
  }
}
export const uploadFail = (err) => {
  return {
    type: actionTypes.UPLOAD_FAIL,
    err: err
  }
}
export const uploadSuccess = (fileType) => {
  return (dispatch, getState) => {
    const getFilesRoute = getState().fileManager.fileTypes.filter(item => item.isActive)[0].apiDetails.get ?? false;
    dispatch(downloadStart(getFilesRoute, 'get'));
    dispatch({
      type: actionTypes.UPLOAD_SUCCESS,
    })
  }
}
export const downloadStart = (apiRoute, method, payload) => {
  return dispatch => {
    dispatch({ type: actionTypes.DOWNLAOD_START })
    axios({
      url: apiRoute,
      data: payload,
      method: method
    })
      .then(res => {
        const result = res.data.data
        dispatch({ type: actionTypes.DOWNLAOD_SUCCESS, result })
      })
      .catch(err => {
        dispatch({ type: actionTypes.DOWNLAOD_FAIL, error: err.response })
      })
  }
}
export const downloadSuccess = () => {
  return {
    type: actionTypes.DOWNLAOD_SUCCESS
  }
}
export const downloadFail = () => {
  return {
    type: actionTypes.DOWNLAOD_FAIL
  }
}
export const deleteFile = (id) => {
  return dispatch => {
    axios.post('/api/admin/delete_file', { file_id: id })
      .then(res => {
        console.log(res);
        dispatch({ type: actionTypes.DELETE_FILE, id })
      })
      .catch(err => {
        console.log(err);
      })
  }
}
export const openFileManager = (initialValue, callbackFunction) => {
  // opens file manager 
  // and sets callback function to be triggered when filemanager is submitted
  return (dispatch, getState) => {
    const getFilesRoute = getState().fileManager.fileTypes.filter(item => item.isActive)[0].apiDetails.get ?? false;
    dispatch(downloadStart(getFilesRoute, 'get'));
    dispatch(setCallbackFunction(callbackFunction));
    dispatch({ type: actionTypes.OPEN_FILE_MANAGER });
    dispatch(setFileManagerInitialValue(formatInitialData(initialValue)));
    dispatch(downloadFolders())
  }
}
export const formatInitialData = (data) => {
  if(!data) {
    return data;
  }
  if (typeof data === 'object') {
    return data.map(item => {
      if (typeof item === 'object') {
        return item.id;
      }
      return item;
    })
  }
  else {
    return data;
  }
}
const setCallbackFunction = (callbackFunction) => {
  return {
    type: actionTypes.SET_CALLBACK_FUNCITON,
    callbackFunction
  }
}
export const setFileManagerValue = (value) => {
  return {
    type: actionTypes.SET_FILE_MANAGER_VALUE,
    value
  }
}
export const setFileManagerInitialValue = (value) => {
  return {
    type: actionTypes.SET_FILE_MANAGER_INITIAL_VALUE,
    value
  }
}
export const closeFileManager = () => {
  return {
    type: actionTypes.CLOSE_FILE_MANAGER
  }
}

export const selectItem = (id) => {
  return {
    type: actionTypes.SELECT_ITEM,
    id
  }
}
export const deSelectItem = (id) => {
  return {
    type: actionTypes.DE_SELECT_ITEM,
    id
  }
}
export const setLimit = (limit) => {
  return {
    type: actionTypes.SET_LIMIT,
    limit
  }
}
export const setReturnUrl = (returnUrl) => {
  return {
    type: actionTypes.SET_RETURN_URL,
    returnUrl
  }
}
export const submitFileManager = () => {
  return dispatch => {
    dispatch({ type: actionTypes.SUBMIT_FILE_MANAGER })
    dispatch(closeFileManager())
  }
}

// folders
export const downloadFolders = () => {
  return dispatch => {
    dispatch({
      type: actionTypes.DOWNLOAD_FOLDERS_START
    });
    axios.get('/api/admin/get_folders')
      .then(res => {
        // const foldersList = convertFolders(res.data.data, null);
        const foldersList = res.data.data;
        dispatch(downloadFoldersSuccess(foldersList))
      })
      .catch(err => {
        dispatch(downloadFoldersError(err))
      })
  }
}
const convertFolders = (folders, parent_id) => {
  const filteredFolders = folders.filter(item => {
    return item.parent_id === parent_id
  })
  filteredFolders.map(folder => {
    folder.children = convertFolders(folders, folder.id);
    return folder;
  })
  return filteredFolders;
}
export const downloadFoldersSuccess = (folders) => {
  return {
    type: actionTypes.DOWNLOAD_FOLDERS_SUCCESS,
    folders
  }
}
export const downloadFoldersError = (err) => {
  return {
    type: actionTypes.DOWNLOAD_FOLDERS_ERROR
  }
}
export const folderItemClicked = (folderId) => {
  return dispatch => {
    dispatch(downloadStart('/api/admin/get_folder_content' , 'post', { folder_id : folderId }));
  }
}