import axios from '../../axios';
import * as actionTypes from './actionTypes';

export const ItemSelectorTriggerClicked = (url, initialValue, callbackFunction, label, selectorConfig) => {
    return dispatch => {
        dispatch({
            type: actionTypes.SET_ITEM_SELECTOR_DOWNLOAD_URL,
            url: url
        })
        dispatch(setItemSelectorConfig(selectorConfig))
        dispatch(setItemSelectorLabel(label));
        dispatch(itemSelecotrDownloadItems(url));
        dispatch(setItemSelectorSelectedItems(initialValue));
        dispatch(setItemSelectorCallbackFunction(callbackFunction));
        dispatch(openItemSelector());
    } 
}
export const openItemSelector = () => {
    return {
        type: actionTypes.OPEN_ITEM_SELECTOR
    }
}
export const setItemSelectorConfig = (selectorConfig) => {
    return {
        type: actionTypes.SET_ITEM_SELECTOR_CONFIG,
        selectorConfig: selectorConfig  
    }
}
export const closeItemSelector = () => {
    return {
        type: actionTypes.CLOSE_ITEM_SELECTOR
    }
}
export const setItemSelectorSelectedItems = (selectedItems) => {
    return {
        type: actionTypes.SET_ITEM_SELECTOR_SELECTED_ITEMS,
        selectedItems
    }
}
export const setItemSelectorItems = (itemsList) => {
    return {
        type: actionTypes.SET_ITEM_SELECTOR_ITEMS,
        itemsList
    }
}
export const setItemSelectorLabel = (label) => {
    return {
        type: actionTypes.SET_ITEM_SELECTOR_LABEL,
        label
    }
}
export const setItemSelectorCallbackFunction = (callbackFunction) => {
    return {
        type: actionTypes.SET_ITEM_SELECTOR_CALLBACK_FUNCTION,
        callbackFunction
    }
}
export const itemSelectorSelectItem = (id) => {
    return {
        type: actionTypes.ITEM_SELECTOR_SELECT_ITEM,
        id: id
    }
}
export const itemSelectorDeselectItem = (id) => {
    return {
        type: actionTypes.ITEM_SELECTOR_DESELECT_ITEM,
        id: id
    }
}
export const itemSelecotrDownloadItems = (url) => {
    return dispatch => {
        dispatch(itemSelecotrDownloadStart())
        axios.get(url)
            .then(res => {
                console.log(res.data);
                dispatch(itemSelecotrDownloadSuccess(res.data.data));
            })
            .catch(err => {
                dispatch(itemSelecotrDownloadFial(err));
            })
    }
}
export const itemSelecotrDownloadStart = (url) => {
    return {
        type: actionTypes.ITEM_SELECTOR_DOWNLOAD_START
    }
}
export const itemSelecotrDownloadSuccess = (itemsList) => {
    return {
        type: actionTypes.ITEM_SELECTOR_DOWNLOAD_SUCCESS,
        itemsList
    }
}
export const itemSelecotrDownloadFial = (err) => {
    return {
        type: actionTypes.ITEM_SELECTOR_DOWNLOAD_FAIL,
        error: err
    }
}
export const submitItemSelector = () => {
    return dispatch => {
        dispatch({
            type: actionTypes.SUBMIT_ITEM_SELECTOR
        })
        dispatch(closeItemSelector());
    }
}
export const itemSelectorSelectAllItems = (isChecked) => {
    return {
        type: actionTypes.ITEM_SELECTOR_SELECT_ALL_ITEMS,
        isChecked: isChecked
    }
}