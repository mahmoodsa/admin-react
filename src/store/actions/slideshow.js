import axios from '../../axios';
import * as actionTypes from './actionTypes';

export const openSlideshow = (initialValue ,callbackFunction, itemIndex, newItem, slideshowItemsList) => {
  // opens file manager 
  // and sets callback function to be triggered when filemanager is submitted
  console.log('initialValue::' , initialValue);
  return (dispatch, getState) => {
      if(initialValue) {
        dispatch(setSlideshowInitialControls(initialValue))
      }
      if(newItem) {
        dispatch(refreshSlideshowControls());
      }
      dispatch(setSelectedItemIndex(itemIndex))
      dispatch(setSlideshowCallbackFunction(callbackFunction));
      dispatch(setSlideShowItemsList(slideshowItemsList))
      dispatch({type: actionTypes.OPEN_SLIDE_SHOW})
  }
}
export const refreshSlideshowControls = () => {
  return {
    type: actionTypes.REFRESH_SLIDE_SHOW_CONTROLS
  }
}
export const setSelectedItemIndex = index => {
  return {
    type: actionTypes.SET_SELECTED_ITEM_INDEX,
    index
  }
}
export const setSlideshowInitialControls = (value) => {
  return {
    type: actionTypes.SET_SLIDE_SHOW_INITI_CONTROLS,
    value: value
  }
}
const setSlideshowCallbackFunction = (callbackFunction) => {
  return {
  type: actionTypes.SET_SLIDE_SHOW_CALLBACK_FUNCITON,
  callbackFunction
  }
}
export const closeSlideshow = () => {
  return {
    type: actionTypes.CLOSE_SLIDE_SHOW
  }
}
export const submitSlideshow = (controls) => {
  return (dispatch, getState) => {
    const selectedItemId = getState().fileManager.selected[0];
    const selectedItem = getState().fileManager.fileItems.filter(item => {
      return item.id === selectedItemId
    })[0];
    const thumbnail = selectedItem ? selectedItem.thumbnail : '';
    dispatch(setSlideshowControls(controls));
    dispatch({
      type: actionTypes.SUBMIT_SLIDE_SHOW,
      controls,
      thumbnail
    });
    dispatch(closeSlideshow())
  }
}
export const setSlideshowControls = controls => {
  return {
    type: actionTypes.SET_SLIDE_SHOW_CONTROLS,
    controls
  }
}
export const setSlideShowItemsList = items => {
  return {
    type: actionTypes.SET_SLIDE_SHOW_ITEMS_LIST,
    items: items ? items : []
  }
}