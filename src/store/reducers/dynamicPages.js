// import * as actionTypes from '../actions/actionTypes';
// import { updateObject } from '../utility'

const initialState = {
  pages: [
    {
      title: 'مدیریت صفحات سایت',
      id: 'sliders',
      subittle: 'مدیریت صفحات سایت',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'صفحه اصلی',
          id: 'home_slider',
          link: '/pages/home',
          open: false,
          active: false,
          pageType: 'form',
          hasId: false,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'get',
            url: '/api/admin/get_home',
            resDataStructure: [{
              name: 'info',
              type: 'input_elements'
            },
            {
              name: 'home_top_slider',
              type: 'slider'
            },
            {
              name: 'home_second_slider',
              type: 'slider'
            },
            {
              name: 'home_category_baner',
              type: 'slider'
            },
            {
              name: 'home_diamond_slider',
              type: 'slider'
            },
            {
              name: 'home_down_baner',
              type: 'slider'
            }]
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_home',
          },
          redirect: '/pages/home',
          controls: {
            text_fa: {
              label: 'متن فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            text_en: {
              label: 'متن انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_fa: {
              label: 'meta title فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title فارسی'
              },
              value: '',
              validation: {
                required: true,
                minLength: 50,
                maxLength: 200
              },
              valid: false,
              touched: false
            },
            meta_title_en: {
              label: 'meta title انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_fa: {
              label: 'meta description فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_en: {
              label: 'meta description انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_fa: {
              label: 'meta keyword فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_en: {
              label: 'meta keyword انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            home_top_slider: {
              label: 'اسلایدشو اول صفحه اصلی',
              elementType: 'SLIDE_SHOW',
              elementConfig: {
                placeholder: 'اسلایدشو اول صفحه اصلی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            home_second_slider: {
              label: 'اسلایدشو دوم صفحه اصلی',
              elementType: 'SLIDE_SHOW',
              elementConfig: {
                placeholder: 'اسلایدشو دوم صفحه اصلی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            home_diamond_slider: {
              label: 'اسلاید شو الماس صفحه اصلی',
              elementType: 'SLIDE_SHOW',
              elementConfig: {
                placeholder: 'اسلاید شو الماس صفحه اصلی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            home_category_baner: {
              label: 'اسلایدشو دسته بندی ها',
              elementType: 'SLIDE_SHOW',
              elementConfig: {
                placeholder: 'اسلایدشو دسته بندی ها'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            home_down_baner: {
              label: 'بنر های پایین صحفه اصلی',
              elementType: 'SLIDE_SHOW',
              elementConfig: {
                placeholder: 'بننر های پایین صحفه اصلیو'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            // services: {
            //   label: 'انتخاب تخفیف ها',
            //   elementType: 'ITEM_SELECTOR',
            //   elementConfig: {
            //     getItemsUrl: '/api/admin/get_all_discount',
            //     selectorLabel: 'انتخاب تخفیف ها'
            //   },
            //   validation: {
            //     required: false
            //   },
            //   value: [],
            //   touched: false
            // },
          }
        },
        {
          title: 'صفحه فروشگاه',
          id: 'shop_page',
          link: '/pages/shop',
          open: false,
          active: false,
          pageType: 'form',
          hasId: false,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'get',
            url: '/api/admin/get_shop',
            resDataStructure: [{
              name: 'info',
              type: 'input_elements'
            },
            {
              name: 'slider',
              type: 'slider'
            }]
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_shop',
          },
          redirect: '/pages/shop',
          controls: {
            text_fa: {
              label: 'متن فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            text_en: {
              label: 'متن انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_fa: {
              label: 'meta title فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_en: {
              label: 'meta title انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_fa: {
              label: 'meta description فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_en: {
              label: 'meta description انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_fa: {
              label: 'meta keyword فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_en: {
              label: 'meta keyword انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            slider: {
              label: 'اسلایدشو',
              elementType: 'SLIDE_SHOW',
              elementConfig: {
                placeholder: 'اسلایدشو'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        },
        {
          title: 'صفحه درباره ما',
          id: 'about_us_slider',
          link: '/pages/about_us',
          open: false,
          active: false,
          pageType: 'form',
          hasId: false,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'get',
            url: '/api/admin/get_aboutus',
            resDataStructure: [{
              name: 'info',
              type: 'input_elements'
            }]
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_aboutus',
          },
          redirect: '/pages/about_us',
          controls: {
            text_fa: {
              label: 'متن فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            text_en: {
              label: 'متن انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_fa: {
              label: 'meta title فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_en: {
              label: 'meta title انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_fa: {
              label: 'meta description فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_en: {
              label: 'meta description انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_fa: {
              label: 'meta keyword فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_en: {
              label: 'meta keyword انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        },
        {
          title: 'صفحه سوالات متداول',
          id: 'questions_page',
          link: '/pages/questions',
          open: false,
          active: false,
          pageType: 'form',
          hasId: false,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'get',
            url: '/api/admin/get_questions',
            resDataStructure: [{
              name: 'info',
              type: 'input_elements'
            }]
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_questions',
          },
          redirect: '/pages/questions',
          controls: {
            text_fa: {
              label: 'متن فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            text_en: {
              label: 'متن انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_fa: {
              label: 'meta title فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_en: {
              label: 'meta title انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_fa: {
              label: 'meta description فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_en: {
              label: 'meta description انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_fa: {
              label: 'meta keyword فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_en: {
              label: 'meta keyword انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        },
        {
          title: 'صفحه کارت اعتباری',
          id: 'credit_card_page',
          link: '/pages/credit_card',
          open: false,
          active: false,
          pageType: 'form',
          hasId: false,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'get',
            url: '/api/admin/get_credit_card',
            resDataStructure: [{
              name: 'info',
              type: 'input_elements'
            }]
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_credit_card',
          },
          redirect: '/pages/credit_card',
          controls: {
            text_fa: {
              label: 'متن فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            text_en: {
              label: 'متن انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_fa: {
              label: 'meta title فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_en: {
              label: 'meta title انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_fa: {
              label: 'meta description فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_en: {
              label: 'meta description انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_fa: {
              label: 'meta keyword فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_en: {
              label: 'meta keyword انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        },
        {
          title: 'صفحه شخصی ساز حلقه',
          id: 'ring_maker_page',
          link: '/pages/ring_maker',
          open: false,
          active: false,
          pageType: 'form',
          hasId: false,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'get',
            url: '/api/admin/get_ring_maker',
            resDataStructure: [{
              name: 'info',
              type: 'input_elements'
            }]
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_ring_maker',
          },
          redirect: '/pages/ring_maker',
          controls: {
            text_fa: {
              label: 'متن فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            text_en: {
              label: 'متن انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'متن انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_fa: {
              label: 'meta title فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_en: {
              label: 'meta title انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_fa: {
              label: 'meta description فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_en: {
              label: 'meta description انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_fa: {
              label: 'meta keyword فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keyword_en: {
              label: 'meta keyword انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keyword انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        }
      ]
    },
    {
      title: 'ادمین ها',
      id: 'admins',
      subittle: 'مدیریت ادمین ها',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'لیست ادمین ها',
          id: 'admins_list',
          link: '/admins/index',
          open: false,
          active: false,
          pageType: 'table',
          api: {
            type: 'get',
            route: '/api/admin/get_all_admin'
          },
          controls: {
            head: [
              // {title: 'id', config: {textAlign: 'center'}},
              { name: 'name', title: 'نام', config: { textAlign: 'center' } },
              { name: 'family', title: 'نام خانوادگی', config: { textAlign: 'center' } },
              { name: 'email', title: 'ایمیل', config: { textAlign: 'center' } },
              { name: 'role', title: 'نقش', config: { textAlign: 'center' } }
            ],
            actions: {
              delete: { apiRoute: '/api/admin/delete_admin', payload: ['admin_id'] },
              edit: { redirect: '/admins/edit', payload: ['admin_id'] }
            }
          }
        },
        {
          title: 'ایجاد ادمین جدید',
          id: 'create_admins',
          link: '/admins/create',
          pageType: 'form',
          open: false,
          redirect: '/admins/index',
          api: {
            type: 'post',
            route: '/api/admin/create_admin'
          },
          active: false,
          controls: {
            name: {
              label: 'نام',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            family: {
              label: 'نام خانوادگی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام خانوادگی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            email: {
              label: 'ایمیل',
              elementType: 'input',
              elementConfig: {
                type: 'email',
                placeholder: 'ایمیل'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            password: {
              label: 'رمز عبور',
              elementType: 'input',
              elementConfig: {
                type: 'password',
                placeholder: 'رمز عبور',
                minLength: 5
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            password_confirmation: {
              label: 'تکرار رمز عبور',
              elementType: 'input',
              elementConfig: {
                type: 'password',
                placeholder: 'تکرار رمز عبور',
                minLength: 5
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            role: {
              label: 'نقش',
              elementType: 'select',
              elementConfig: {
                placeholder: 'نقش',
                options: [
                  { value: 'owner', displayValue: 'مدیر' },
                  { value: 'admin', displayValue: 'ادمین' },
                  { value: 'writer', displayValue: 'نویسنده' }
                ]
              },
              value: 'owner',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        },
        {
          title: 'ویرایش ادمین',
          id: 'edit_admins',
          hideOnDrawer: true,
          link: '/admins/edit',
          pageType: 'form',
          open: false,
          redirect: '/board/index/',
          hasId: true,
          api: {
            type: 'post',
            route: '/api/admin/edit_admin',
            get: {
              route: '/api/admin/get_admin',
              payload: ['admin_id']
            }
          },
          active: false,
          controls: {
            name: {
              label: 'نام',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            family: {
              label: 'نام خانوادگی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام خانوادگی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            email: {
              label: 'ایمیل',
              elementType: 'input',
              elementConfig: {
                type: 'email',
                placeholder: 'ایمیل'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            password: {
              label: 'رمز عبور',
              elementType: 'input',
              elementConfig: {
                type: 'password',
                placeholder: 'رمز عبور'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            password_confirmation: {
              label: 'تکرار رمز عبور',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'تکرار رمز عبور'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            role: {
              label: 'نقش',
              elementType: 'select',
              elementConfig: {
                placeholder: 'نقش',
                options: [
                  { value: 'owner', displayValue: 'مدیر' },
                  { value: 'admin', displayValue: 'ادمین' },
                  { value: 'writer', displayValue: 'نویسنده' }
                ]
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        }
      ]
    },
    {
      title: 'مدیریت کامنت ها',
      id: 'comments',
      subittle: 'مدیریت کامنت ها',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'لیست کامنت ها',
          id: 'comments_list',
          link: '/comments/index',
          open: false,
          active: false,
          pageType: 'table',
          api: {
            type: 'get',
            route: '/api/admin/get_all_comments'
          },
          controls: {
            head: [
              // {title: 'id', config: {textAlign: 'center'}},
              { name: 'user_type', title: 'نوع کاربر', config: { textAlign: 'center' } },
              { name: 'content', title: 'متن کامنت', config: { textAlign: 'center' } },
              { name: 'created_at', title: 'تاریخ ایجاد', config: { textAlign: 'center' } },
              { name: 'updated_at', title: 'تاریخ ویرایش', config: { textAlign: 'center' } },
            ],
            actions: {
              delete: { apiRoute: '/api/admin/delete_comment', payload: ['id'] },
              edit: { redirect: '/comments/edit', payload: [{ name: 'id', stateName: 'comment_id' }, { name: 'product_id', stateName: 'product_id' }] },
              confirm: { apiRoute: '/api/admin/confirm_comment', payload: ['id'], showConfirm: { 'confirm': 1 } },
              unConfirm: { apiRoute: '/api/admin/confirm_comment', payload: ['id'], showUnConfirm: { 'confirm': 0 } },
              reply: {
                redirect: '/comments/reply',
                payload: [{ name: 'product_id', stateName: 'product_id' }, { name: 'id', stateName: 'parent_id' }],
              },
            }
          }
        },
        {
          title: 'ویرایش کامنت',
          id: 'edit_comment',
          hideOnDrawer: true,
          link: '/comments/edit',
          pageType: 'form',
          open: false,
          redirect: '/comments/index',
          hasId: true,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'post',
            url: '/api/admin/get_singel_comment',
            resDataStructure: [
              {
                name: 'content',
                type: 'input_elements'
              },
              {
                name: 'created_at',
                type: 'input_elements'
              },
              {
                name: 'updated_at',
                type: 'input_elements'
              },
              {
                name: 'confirm',
                type: 'input_elements'
              },
              {
                name: 'user_type',
                type: 'input_elements'
              },
              {
                name: 'user_id',
                type: 'input_elements'
              },
              {
                name: 'admin_id',
                type: 'input_elements'
              }
            ]
          },
          api: {
            type: 'post',
            route: '/api/admin/admin_edit_comment',
            post: {
              payload: ['comment_id', 'product_id']
            },
            get: {
              route: '/api/admin/get_singel_comment',
              payload: ['comment_id']
            }
          },
          active: false,
          controls: {
            content: {
              label: 'متن کامنت',
              elementType: 'textarea',
              elementConfig: {
                placeholder: 'متن کامنت',
                styles: {
                  height: '100px',
                  overflowX: 'hidden'
                }
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            confirm: {
              label: 'تایید کامنت',
              elementType: 'checkbox',
              elementConfig: {
                type: 'checkbox',
                placeholder: 'تایید کامنت',
                checked: true
              },
              value: true,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            user_type: {
              elementType: 'HIDDEN',
              elementConfig: {
              },
              value: 1,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            user_id: {
              elementType: 'HIDDEN',
              elementConfig: {
                nullable: true
              },
              value: 1,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            admin_id: {
              elementType: 'HIDDEN',
              elementConfig: {
                nullable: true
              },
              value: null,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
          }
        },
        {
          title: 'ریپلای کامنت',
          id: 'reply_comment',
          hideOnDrawer: true,
          link: '/comments/reply',
          pageType: 'form',
          open: false,
          redirect: '/comments/index',
          hasId: true,
          api: {
            type: 'post',
            route: '/api/admin/admin_make_comment',
            post: {
              payload: ['parent_id', 'product_id']
            },
            get: {
              route: '/api/admin/admin_make_comment',
              payload: ['id']
            }
          },
          active: false,
          controls: {
            admin_id: {
              label: 'آیدی ادمین',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'آیدی ادمین'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            content: {
              label: 'متن کامنت',
              elementType: 'textarea',
              elementConfig: {
                type: 'text',
                placeholder: 'متن کامنت'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            confirm: {
              elementType: 'HIDDEN',
              elementConfig: {
              },
              value: 1,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        }
      ]
    },
    // {
    //   title: 'مدریت رنگ ها',
    //   id: 'color',
    //   subittle: 'مدریت رنگ ها',
    //   icon: 'dashboard',
    //   open: false,
    //   pages: [
    //     {
    //       title: 'لیست رنگ ها',
    //       id: 'color_list',
    //       link: '/color/index',
    //       open: false,
    //       active: false,
    //       pageType: 'table',
    //       api: {
    //         type: 'get',
    //         route: '/api/admin/get_all_color',
    //       },
    //       controls: {
    //         head: [
    //           { name: 'name_fa', title: 'نام رنگ', config: { textAlign: 'center', width: '300' } },
    //           { name: 'hex', title: 'hex', config: { textAlign: 'center', width: '100' } },
    //         ],
    //         filters: {
    //           category_name_fa: {
    //             label: 'نام رنگ',
    //             elementType: 'input',
    //             elementConfig: {
    //               type: 'text',
    //               placeholder: 'نام رنگ'
    //             },
    //             value: '',
    //             touched: false
    //           },
    //         },
    //         actions: {
    //           delete: { apiRoute: '/api/admin/delete_color', payload: ['color_id'] },
    //           edit: { redirect: '/color/edit', payload: [{ name: 'id', stateName: 'color_id' }] },
    //         }
    //       }
    //     },
    //     {
    //       title: 'ایجاد رنگ',
    //       id: 'create_color',
    //       link: '/color/create',
    //       pageType: 'form',
    //       open: false,
    //       redirect: '/color/index',
    //       hasId: false,
    //       loadAsyncData: false,
    //       api: {
    //         type: 'post',
    //         route: '/api/admin/create_color',
    //       },
    //       active: false,
    //       controls: {
    //         name_fa: {
    //           label: 'نام رنگ فارسی',
    //           elementType: 'input',
    //           elementConfig: {
    //             type: 'text',
    //             placeholder: 'نام رنگ فارسی'
    //           },
    //           value: '',
    //           validation: {
    //             required: true,
    //           },
    //           valid: false,
    //           touched: false
    //         },
    //         name_en: {
    //           label: 'نام رنگ انگلیسی',
    //           elementType: 'input',
    //           elementConfig: {
    //             type: 'text',
    //             placeholder: 'نام رنگ انگلیسی'
    //           },
    //           value: '',
    //           validation: {
    //             required: true,
    //           },
    //           valid: false,
    //           touched: false
    //         },
    //         hex: {
    //           label: 'کد رنگ hex',
    //           elementType: 'input',
    //           elementConfig: {
    //             type: 'text',
    //             placeholder: 'کد رنگ hex'
    //           },
    //           value: '',
    //           validation: {
    //             required: true,
    //           },
    //           valid: false,
    //           touched: false
    //         }
    //       }
    //     },
    //     {
    //       title: 'ویرایش رنگ',
    //       id: 'edit_color',
    //       link: '/color/edit',
    //       pageType: 'form',
    //       open: false,
    //       redirect: '/color/index',
    //       hasId: true,
    //       hideOnDrawer: true,
    //       loadAsyncData: true,
    //       loadAsyncDataConfig: {
    //         method: 'post',
    //         url: '/api/admin/get_single_color',
    //       },
    //       api: {
    //         type: 'post',
    //         route: '/api/admin/edit_color',
    //         get: {
    //           payload: ['color_id']
    //         }
    //       },
    //       active: false,
    //       controls: {
    //         name_fa: {
    //           label: 'نام رنگ فارسی',
    //           elementType: 'input',
    //           elementConfig: {
    //             type: 'text',
    //             placeholder: 'نام رنگ فارسی'
    //           },
    //           value: '',
    //           validation: {
    //             required: true,
    //           },
    //           valid: false,
    //           touched: false
    //         },
    //         name_en: {
    //           label: 'نام رنگ انگلیسی',
    //           elementType: 'input',
    //           elementConfig: {
    //             type: 'text',
    //             placeholder: 'نام رنگ انگلیسی'
    //           },
    //           value: '',
    //           validation: {
    //             required: true,
    //           },
    //           valid: false,
    //           touched: false
    //         },
    //         hex: {
    //           label: 'کد رنگ hex',
    //           elementType: 'input',
    //           elementConfig: {
    //             type: 'text',
    //             placeholder: 'کد رنگ hex'
    //           },
    //           value: '',
    //           validation: {
    //             required: true,
    //           },
    //           valid: false,
    //           touched: false
    //         }
    //       }
    //     }
    //   ]
    // },
    {
      title: 'مدریت دسته بندی ها',
      id: 'category',
      subittle: 'مدریت دسته بندی ها',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'لیست دسته بندی ها',
          id: 'category_list',
          link: '/category/index',
          open: false,
          active: false,
          pageType: 'table',
          api: {
            type: 'get',
            route: '/api/admin/get_all_category_unnested'
          },
          controls: {
            head: [
              { name: 'category_name_fa', title: 'نام دسته بندی', config: { textAlign: 'center', width: '300' } },
              { name: 'slug_fa', title: 'slug', config: { textAlign: 'center', width: '100' } },
            ],
            filters: {
              category_name_fa: {
                label: 'نام دسته بندی',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'نام دسته بندی'
                },
                value: '',
                touched: false
              },
              slug_fa: {
                label: 'slug',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'اسلاگ'
                },
                value: '',
                touched: false
              }
            },
            actions: {
              delete: { apiRoute: '/api/admin/delete_product_category', payload: ['category_id'] },
              edit: { redirect: '/category/edit', payload: [{ name: 'id', stateName: 'category_id' }] },
            }
          }
        },
        {
          title: 'ایجاد دسته بندی',
          id: 'create_product_category',
          link: '/category/create',
          pageType: 'form',
          open: false,
          redirect: '/category/index',
          hasId: false,
          loadAsyncData: false,
          api: {
            type: 'post',
            route: '/api/admin/create_product_category',
          },
          active: false,
          controls: {
            category_name_fa: {
              label: 'نام دسته بندی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام دسته بندی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            category_name_en: {
              label: 'نام دسته بندی انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام دسته بندی انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            parent_id: {
              label: 'دسته بندی والد',
              elementType: 'ASYNC_DROPDOWN',
              elementConfig: {
                type: 'text',
                placeholder: 'دسته بندی والد',
                optionsConfig: {
                  optionDisplayValue: 'category_name_fa',
                  optionValue: 'id'
                },
                getDataApiConfig: {
                  url: '/api/admin/get_all_category_unnested',
                  method: 'get'
                }
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            slug_fa: {
              label: 'slug فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'slug فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            slug_en: {
              label: 'slug انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'slug انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            file_id: {
              label: 'آیکون دسته بندی',
              elementType: 'fileManager',
              elementConfig: {
                limit: 1
              },
              value: null,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            static_properties_fa: {
              label: 'ویژگی های محصول فارسی',
              elementType: 'MULTIPLE_KEY_VALUE_INPUT',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            static_properties_en: {
              label: 'ویژگی های محصول انگلیسی',
              elementType: 'MULTIPLE_KEY_VALUE_INPUT',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            selectable_properties: {
              label: 'ویژگی های قابل انتخاب محصول',
              elementType: 'SELECTABLE_PROPERTY_SELECTOR',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
          }
        },
        {
          title: 'ویرایش دسته بندی',
          id: 'edit_cateory',
          hideOnDrawer: true,
          link: '/category/edit',
          pageType: 'form',
          open: false,
          redirect: '/category/index',
          hasId: true,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'post',
            url: '/api/admin/get_single_category',
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_product_category',
            get: {
              route: '/api/admin/get_single_category',
              payload: ['category_id']
            }
          },
          active: false,
          controls: {
            category_name_en: {
              label: 'نام دسته بندی انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام دسته بندی انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            category_name_fa: {
              label: 'نام دسته بندی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام دسته بندی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            parent_id: {
              label: 'دسته بندی والد',
              elementType: 'ASYNC_DROPDOWN',
              elementConfig: {
                type: 'text',
                placeholder: 'دسته بندی والد',
                optionsConfig: {
                  optionDisplayValue: 'category_name_fa',
                  optionValue: 'id'
                },
                getDataApiConfig: {
                  url: '/api/admin/get_all_category_unnested',
                  method: 'get'
                }
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            slug_fa: {
              label: 'slug فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'slug فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            slug_en: {
              label: 'slug انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'slug انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            file_id: {
              label: 'آیکون دسته بندی',
              elementType: 'fileManager',
              elementConfig: {
                limit: 1
              },
              value: null,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            static_properties_fa: {
              label: 'ویژگی های محصول فارسی',
              elementType: 'MULTIPLE_KEY_VALUE_INPUT',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            static_properties_en: {
              label: 'ویژگی های محصول انگلیسی',
              elementType: 'MULTIPLE_KEY_VALUE_INPUT',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            selectable_properties: {
              label: 'ویژگی های قابل انتخاب محصول',
              elementType: 'SELECTABLE_PROPERTY_SELECTOR',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
          }
        }
      ]
    },
    {
      title: 'مدیریت محصولات',
      id: 'products',
      subittle: 'مدیریت محصولات',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'لیست محصولات',
          id: 'products_list',
          link: '/product/index',
          open: false,
          active: false,
          pageType: 'table',
          api: {
            type: 'get',
            route: '/api/admin/get_all_product'
          },
          controls: {
            head: [
              { name: 'product_name_fa', title: 'نام محصول', config: { textAlign: 'center', width: '300' } },
              { name: 'stock_count', title: 'موجودی', config: { textAlign: 'center', width: '100' } },
            ],
            filters: {
              product_name_fa: {
                label: 'نام محصول',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'نام محصول'
                },
                value: '',
                touched: false
              },
              stock_count: {
                label: 'موجودی',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'موجودی'
                },
                value: '',
                touched: false
              }
            },
            actions: {
              delete: { apiRoute: '/api/admin/delete_permanently', payload: ['product_id'] },
              edit: { redirect: '/product/edit', payload: [{ name: 'id', stateName: 'product_id' }] },
              active: {
                activeApiRoute: '/api/admin/active_product',
                diactiveApiRoute: '/api/admin/deactive_product',
                payload: ['product_id']
              }
            }
          }
        },
        {
          title: 'ویرایش محصول',
          id: 'edit_product',
          hideOnDrawer: true,
          link: '/product/edit',
          pageType: 'form',
          open: false,
          redirect: '/product/index',
          hasId: true,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'post',
            url: '/api/admin/get_single_product',
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_product',
            get: {
              route: '/api/admin/get_single_product',
              payload: ['product_id']
            }
          },
          active: false,
          controls: {
            category: {
              label: 'دسته بندی',
              elementType: 'MULTIPLE_ASYNC_DROPDOWN',
              elementConfig: {
                type: 'text',
                placeholder: 'دسته بندی',
                optionsConfig: {
                  optionDisplayValue: 'category_name_fa',
                  optionValue: 'id',
                },
                getDataApiConfig: {
                  url: '/api/admin/get_all_category_unnested',
                  method: 'get'
                }
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            product_name_fa: {
              label: 'نام محصول',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام محصول'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            product_name_en: {
              label: 'نام محصول انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام محصول انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            description_fa: {
              label: 'توصیف محصول فارسی',
              elementType: 'CK_EDITOR',
              elementConfig: {
                type: 'text',
                placeholder: 'توصیف محصول فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            description_en: {
              label: 'توصیف محصول انگلیسی',
              elementType: 'CK_EDITOR',
              elementConfig: {
                type: 'text',
                placeholder: 'توصیف محصول انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            slug_fa: {
              label: 'slug فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'slug فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            slug_en: {
              label: 'slug انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'slug انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            value_unit: {
              label: 'قیمت واحد',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'قیمت واحد'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            stock_count: {
              label: 'موجودی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'موجودی',
                nullable: true
              },
              value: '',
              valid: false,
              touched: false
            },
            admin_rating: {
              label: 'امتیاز محصول',
              elementType: 'input',
              elementConfig: {
                type: 'number',
                placeholder: 'امتیاز محصول',
                nullable: true
              },
              value: '',
              valid: false,
              touched: false
            },
            static_properties_fa: {
              label: 'ویژگی های محصول فارسی',
              elementType: 'MULTIPLE_KEY_VALUE_INPUT',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            static_properties_en: {
              label: 'ویژگی های محصول انگلیسی',
              elementType: 'MULTIPLE_KEY_VALUE_INPUT',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            selectable_properties: {
              label: 'ویژگی های قابل انتخاب محصول',
              elementType: 'SELECTABLE_PROPERTY_SELECTOR',
              elementConfig: {
                type: 'text',
                placeholder: 'ویژگی'
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            files: {
              label: 'تصاویر محصول',
              elementType: 'fileManager',
              elementConfig: {
                limit: null
              },
              value: null,
              validation: {
                required: true,
              },
              valid: false,
              touched: false,
              name: 'file_ids'
            },
            three_d_model: {
              label: 'مدل سه بعدی',
              elementType: 'FILE_UPLOADER',
              elementConfig: {
                placeholder: 'آپلود فایل سه بعدی',
                nullable: true,
                apiConfig: {
                  method: 'post',
                  url: '/api/upload_3d_model',
                  uploadFileName: 'file',
                  uploadIdName: 'product_id'
                }
              },
              value: '',
              valid: false,
              touched: false
            },
            three_sixty_image: {
              label: 'عکس ۳۶۰ درجه',
              elementType: 'FILE_UPLOADER',
              elementConfig: {
                placeholder: 'آپلود تصویر',
                nullable: true,
                apiConfig: {
                  method: 'post',
                  url: '/api/admin/create_three_d_image',
                  uploadFileName: 'image',
                  uploadIdName: 'product_id'
                }
              },
              value: '',
              valid: false,
              touched: false
            },
            product_banner: {
              label: 'بنر محصول',
              elementType: 'FILE_UPLOADER',
              elementConfig: {
                placeholder: 'آپلود تصویر',
                nullable: true,
                apiConfig: {
                  method: 'post',
                  url: '/api/admin/upload_product_banner',
                  uploadFileName: 'file',
                  uploadIdName: 'product_id'
                }
              },
              value: '',
              valid: false,
              touched: false
            },
            slug_fa: {
              label: 'slug فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'slug انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            slug_en: {
              label: 'slug انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'slug انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_fa: {
              label: 'meta title فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_title_en: {
              label: 'meta title انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta title انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_fa: {
              label: 'meta description فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_description_en: {
            },
            meta_description_en: {
              label: 'meta description انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta description انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keywords_fa: {
              label: 'meta keywords فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keywords فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            meta_keywords_en: {
              label: 'meta keywords انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'meta keywords انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            discount_id: {
              label: 'انتخاب تخفیف ها',
              elementType: 'ITEM_SELECTOR',
              elementConfig: {
                getItemsUrl: '/api/admin/get_all_discount',
                selectorLabel: 'انتخاب تخفیف ها',
                selectorConfig: {
                  selectorType: "table",
                  limit: 1,
                  head: [
                    { name: 'name_fa', title: 'عنوان', config: { textAlign: 'center' } },
                    { name: 'start', title: 'تاریخ شروع', config: { textAlign: 'center' } },
                    { name: 'finish', title: 'تاریخ پایان', config: { textAlign: 'center' } }
                  ],
                },
                nullable: true
              },
              validation: {
                required: false
              },
              value: '',
              touched: false
            },
          }
        },
        {
          title: 'ایجاد محصول',
          id: 'create_product',
          link: '/product/create',
          pageType: 'steppedForm',
          open: false,
          redirect: '/product/index',
          active: false,
          steppedControls: [
            {
              title: 'ایجاد محصول',
              locationStateSetter: [
                {
                  apiResponseTitle: 'product_id',
                  setterStateTitle: 'product_id'
                }
              ],
              api: {
                type: 'post',
                route: '/api/admin/create_product',
              },
              controls: {
                category: {
                  label: 'دسته بندی',
                  elementType: 'MULTIPLE_ASYNC_DROPDOWN',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'دسته بندی',
                    optionsConfig: {
                      optionDisplayValue: 'category_name_fa',
                      optionValue: 'id',
                    },
                    getDataApiConfig: {
                      url: '/api/admin/get_all_category_unnested',
                      method: 'get'
                    }
                  },
                  value: [],
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                product_name_fa: {
                  label: 'نام محصول',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'نام محصول'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                product_name_en: {
                  label: 'نام محصول انگلیسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'نام محصول انگلیسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                description_fa: {
                  label: 'توصیف محصول فارسی',
                  elementType: 'textarea',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'توصیف محصول فارسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                description_en: {
                  label: 'توصیف محصول انگلیسی',
                  elementType: 'textarea',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'توصیف محصول انگلیسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                slug_fa: {
                  label: 'slug فارسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'slug فارسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                slug_en: {
                  label: 'slug انگلیسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'slug انگلیسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                review_fa: {
                  label: 'بررسی محصول فارسی',
                  elementType: 'CK_EDITOR',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'بررسی محصول فارسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                review_en: {
                  label: 'بررسی محصول انگلیسی',
                  elementType: 'CK_EDITOR',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'بررسی محصول انگلیسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                value_unit: {
                  label: 'قیمت واحد',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'قیمت واحد'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                stock_count: {
                  label: 'موجودی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'موجودی',
                    nullable: true
                  },
                  value: '',
                  valid: false,
                  touched: false
                },
                admin_rating: {
                  label: 'امتیاز محصول',
                  elementType: 'input',
                  elementConfig: {
                    type: 'number',
                    placeholder: 'امتیاز محصول',
                    nullable: true
                  },
                  value: '',
                  valid: false,
                  touched: false
                },
                static_properties_fa: {
                  label: 'ویژگی های محصول فارسی',
                  elementType: 'MULTIPLE_KEY_VALUE_INPUT',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'ویژگی',
                    valueCountlimit: 1
                  },
                  value: [],
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                static_properties_en: {
                  label: 'ویژگی های محصول انگلیسی',
                  elementType: 'MULTIPLE_KEY_VALUE_INPUT',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'ویژگی',
                    valueCountlimit: 1
                  },
                  value: [],
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                selectable_properties: {
                  label: 'ویژگی های قابل انتخاب محصول',
                  elementType: 'SELECTABLE_PROPERTY_SELECTOR',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'ویژگی'
                  },
                  value: [],
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                files: {
                  label: 'تصاویر محصول',
                  elementType: 'fileManager',
                  elementConfig: {
                    limit: null
                  },
                  value: [],
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false,
                  name: 'file_ids'
                },
                discount_id: {
                  label: 'انتخاب تخفیف ها',
                  elementType: 'ITEM_SELECTOR',
                  elementConfig: {
                    getItemsUrl: '/api/admin/get_all_discount',
                    selectorLabel: 'انتخاب تخفیف ها',
                    selectorConfig: {
                      selectorType: "table",
                      limit: 1,
                      head: [
                        { name: 'name_fa', title: 'عنوان', config: { textAlign: 'center' } },
                        { name: 'start', title: 'تاریخ شروع', config: { textAlign: 'center' } },
                        { name: 'finish', title: 'تاریخ پایان', config: { textAlign: 'center' } }
                      ],
                    },
                    nullable: true
                  },
                  validation: {
                    required: false
                  },
                  value: '',
                  touched: false
                },
                meta_title_fa: {
                  label: 'meta title فارسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'meta title فارسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                meta_title_en: {
                  label: 'meta title انگلیسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'meta title انگلیسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                meta_description_fa: {
                  label: 'meta description فارسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'meta description فارسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                meta_description_en: {
                },
                meta_description_en: {
                  label: 'meta description انگلیسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'meta description انگلیسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                meta_keywords_fa: {
                  label: 'meta keywords فارسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'meta keywords فارسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
                meta_keywords_en: {
                  label: 'meta keywords انگلیسی',
                  elementType: 'input',
                  elementConfig: {
                    type: 'text',
                    placeholder: 'meta keywords انگلیسی'
                  },
                  value: '',
                  validation: {
                    required: true,
                  },
                  valid: false,
                  touched: false
                },
              }
            },
            {
                title: 'ایجاد محصول',
                controls: {
                three_d_model: {
                  label: 'مدل سه بعدی',
                  elementType: 'FILE_UPLOADER',
                  elementConfig: {
                    placeholder: 'آپلود فایل سه بعدی',
                    nullable: true,
                    apiConfig: {
                      method: 'post',
                      url: '/api/upload_3d_model',
                      uploadFileName: 'file',
                      uploadIdName: 'product_id'
                    }
                  },
                  value: '',
                  valid: false,
                  touched: false
                },
                three_sixty_image: {
                  label: 'عکس ۳۶۰ درجه',
                  elementType: 'FILE_UPLOADER',
                  elementConfig: {
                    placeholder: 'آپلود تصویر',
                    nullable: true,
                    apiConfig: {
                      method: 'post',
                      url: '/api/admin/create_three_d_image',
                      uploadFileName: 'image',
                      uploadIdName: 'product_id'
                    }
                  },
                  value: '',
                  valid: false,
                  touched: false
                },
                product_banner: {
                  label: 'بنر محصول',
                  elementType: 'FILE_UPLOADER',
                  elementConfig: {
                    placeholder: 'آپلود تصویر',
                    nullable: true,
                    apiConfig: {
                      method: 'post',
                      url: '/api/admin/upload_product_banner',
                      uploadFileName: 'file',
                      uploadIdName: 'product_id'
                    }
                  },
                  value: '',
                  valid: false,
                  touched: false
                },
              }
            }
          ]
        }
      ]
    },
    {
      title: 'مدیریت سفارشات',
      id: 'orders',
      subittle: 'مدیریت سفارشات',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'لیست سفارشات ثبت شده',
          id: 'submited_orders_list',
          link: '/order/submitted_orders',
          open: false,
          active: false,
          pageType: 'table',
          api: {
            type: 'get',
            route: '/api/admin/get_all_submitted_orders'
          },
          controls: {
            head: [
              { name: 'receiver_first_name', title: 'نام', config: { textAlign: 'center' } },
              { name: 'receiver_last_name', title: 'نام خانوادگی', config: { textAlign: 'center' } },
              { name: 'receiver_phone', title: 'شماره تلفن', config: { textAlign: 'center' } },
              { name: 'tracing_code', title: 'کد پیگیری سفارش', config: { textAlign: 'center' } },
              { name: 'status', title: 'وضعیت', config: { textAlign: 'center' } },
            ],
            filters: {
              tracing_code: {
                label: 'کد پیگیری سفارش',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'کد پیگیری سفارش'
                },
                value: '',
                touched: false
              },
              reciever_first_name: {
                label: 'نام',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'نام'
                },
                value: '',
                touched: false
              },
              reciever_last_name: {
                label: 'نام خانوادگی',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'نام خانوادگی'
                },
                value: '',
                touched: false
              },
              reciever_phone: {
                label: 'شماره تلفن',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'شماره تلفن'
                },
                value: '',
                touched: false
              },
              tracing_code: {
                label: 'کد پیگیری سفارش',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'کد پیگیری سفارش'
                },
                value: '',
                touched: false
              },
              status: {
                label: 'وضعیت',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'وضعیت'
                },
                value: '',
                touched: false
              }
            },
            actions: {
              edit: { redirect: '/order/submitted_order_details', payload: [{ name: 'id', stateName: 'order_id' }] }
            }
          }
        },
        {
          title: 'ویرایش سفارش پرداخت شده',
          id: 'edit_submitted_orders',
          hideOnDrawer: true,
          link: '/order/submitted_order_details',
          pageType: 'form',
          open: false,
          redirect: '/order/submitted_orders',
          hasId: true,
          loadAsyncData: true,
          submitButtonText: 'ثبت سفارش به عنوان پرداخت شده',
          loadAsyncDataConfig: {
            method: 'post',
            url: '/api/admin/get_single_submitted_order',
          },
          api: {
            type: 'post',
            route: '/api/admin/make_submitted_order_paid',
            get: {
              route: '/api/admin/get_single_submitted_order',
              payload: ['order_id']
            }
          },
          active: false,
          controls: {
            // status: {
            //   label: 'وضعیت سفارش',
            //   elementType: 'select',
            //   elementConfig: {
            //     placeholder: 'وضعیت سفارش',
            //     options: [
            //       { value: 'Checking', displayValue: 'در حال بررسی' },
            //       { value: 'Sent', displayValue: 'ارسال شده' },
            //       { value: 'Delivered', displayValue: 'تحویل شده' },
            //       { value: 'rejected', displayValue: 'مرجوعی' },
            //     ]
            //   },
            //   value: 'owner',
            //   validation: {
            //     required: true,
            //   },
            //   valid: false,
            //   touched: false
            // },
            receiver_address: {
              label: 'آدرس',
              elementType: 'textarea',
              elementConfig: {
                type: 'text',
                placeholder: 'آدرس',
                disabled: true
              },
              value: '',
            },
            tracing_code: {
              label: 'کد پیگیری',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'کد پیگیری',
                disabled: true
              },
              value: '',
            },
            receiver_zip_code: {
              label: 'کد پستی تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'کد پستی تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_first_name: {
              label: 'نام تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_last_name: {
              label: 'نام خانوادگی تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام خانوادگی تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_ssn: {
              label: 'کد ملی تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'کد ملی تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_email: {
              label: 'ایمیل تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'ایمیل تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_phone: {
              label: 'تلفن تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'تلفن تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            product: {
              label: 'لیست محصولات',
              elementType: 'PRODUCT_LIST',
              elementConfig: {
                disabled: true,
                productType: 'product'
              },
              value: '',
            },
            kay: {
              label: 'لیست محصولات شخصی سازی شده',
              elementType: 'PRODUCT_LIST',
              elementConfig: {
                disabled: true,
                productType: 'kay'
              },
              value: '',
            },
          }
        },
        {
          title: 'لیست سفارشات پرداخت شده',
          id: 'paid_orders_list',
          link: '/order/paid_orders_list',
          open: false,
          active: false,
          pageType: 'table',
          api: {
            type: 'get',
            route: '/api/admin/get_all_paid_orders'
          },
          controls: {
            head: [
              { name: 'receiver_first_name', title: 'نام', config: { textAlign: 'center' } },
              { name: 'receiver_last_name', title: 'نام خانوادگی', config: { textAlign: 'center' } },
              { name: 'receiver_phone', title: 'شماره تلفن', config: { textAlign: 'center' } },
              { name: 'tracing_code', title: 'کد پیگیری سفارش', config: { textAlign: 'center' } },
              { name: 'status', title: 'وضعیت', config: { textAlign: 'center' } },
            ],
            filters: {
              tracing_code: {
                label: 'کد پیگیری سفارش',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'کد پیگیری سفارش'
                },
                value: '',
                touched: false
              },
              reciever_first_name: {
                label: 'نام',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'نام'
                },
                value: '',
                touched: false
              },
              reciever_last_name: {
                label: 'نام خانوادگی',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'نام خانوادگی'
                },
                value: '',
                touched: false
              },
              reciever_phone: {
                label: 'شماره تلفن',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'شماره تلفن'
                },
                value: '',
                touched: false
              },
              tracing_code: {
                label: 'کد پیگیری سفارش',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'کد پیگیری سفارش'
                },
                value: '',
                touched: false
              },
              status: {
                label: 'وضعیت',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'وضعیت'
                },
                value: '',
                touched: false
              }
            },
            actions: {
              edit: { redirect: '/order/edit_paid_orders', payload: [{ name: 'id', stateName: 'paid_order_id' }] }
            }
          }
        },
        {
          title: 'ویرایش سفارش پرداخت شده',
          id: 'edit_paid_orders',
          hideOnDrawer: true,
          link: '/order/edit_paid_orders',
          pageType: 'form',
          open: false,
          redirect: '/order/paid_orders_list',
          hasId: true,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'post',
            url: '/api/admin/get_single_paid_order',
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_paid_order',
            get: {
              route: '/api/admin/get_single_paid_order',
              payload: ['paid_order_id']
            }
          },
          active: false,
          controls: {
            status: {
              label: 'وضعیت سفارش',
              elementType: 'select',
              elementConfig: {
                placeholder: 'وضعیت سفارش',
                options: [
                  { value: 'Checking', displayValue: 'در حال بررسی' },
                  { value: 'Sent', displayValue: 'ارسال شده' },
                  { value: 'Delivered', displayValue: 'تحویل شده' },
                  { value: 'rejected', displayValue: 'مرجوعی' },
                ]
              },
              value: 'owner',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            receiver_address: {
              label: 'آدرس',
              elementType: 'textarea',
              elementConfig: {
                type: 'text',
                placeholder: 'آدرس'
              },
              value: '',
            },
            tracing_code: {
              label: 'کد پیگیری',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'کد پیگیری',
                disabled: true
              },
              value: '',
            },
            receiver_zip_code: {
              label: 'کد پستی تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'کد پستی تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_first_name: {
              label: 'نام تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_last_name: {
              label: 'نام خانوادگی تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'نام خانوادگی تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_ssn: {
              label: 'کد ملی تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'کد ملی تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_email: {
              label: 'ایمیل تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'ایمیل تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
            receiver_phone: {
              label: 'تلفن تحویل گیرنده',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'تلفن تحویل گیرنده',
                disabled: true
              },
              value: '',
            },
          }
        },
      ]
    },
    {
      title: 'مدیریت تخفیف ها',
      id: 'discount',
      subittle: 'مدیریت تخفیف ها',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'لیست تخفیف ها',
          id: 'discounts_list',
          link: '/discount/index',
          open: false,
          active: false,
          pageType: 'table',
          api: {
            type: 'get',
            route: '/api/admin/get_all_discount'
          },
          controls: {
            head: [
              { name: 'name_fa', title: 'عنوان تخفیف', config: { textAlign: 'center', width: '100' } },
              { name: 'start', title: 'تاریخ شروع', config: { textAlign: 'center', width: '300' } },
              { name: 'finish', title: 'تاریخ پایان', config: { textAlign: 'center', width: '300' } },
            ],
            filters: {
              name_fa: {
                label: 'عنوان تخفیف',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'عنوان تخفیف'
                },
                value: '',
                touched: false
              }
            },
            actions: {
              delete: { apiRoute: '/api/admin/delete_discount', payload: ['discount_id'] },
              edit: { redirect: '/discount/edit', payload: [{ name: 'id', stateName: 'discount_id' }] },
            }
          }
        },
        {
          title: 'ویرایش تخفیف',
          id: 'edit_discount',
          hideOnDrawer: true,
          link: '/discount/edit',
          pageType: 'form',
          open: false,
          redirect: '/discount/index',
          hasId: true,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'post',
            url: '/api/admin/get_one_discount',
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_discount',
            get: {
              route: '/api/admin/get_one_discount',
              payload: ['discount_id']
            }
          },
          active: false,
          controls: {
            name_fa: {
              label: 'عنوان تخفیف فارسی',
              elementType: 'text',
              elementConfig: {
                type: 'text',
                placeholder: 'عنوان تخفیف',
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            name_en: {
              label: 'عنوان تخفیف انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'عنوان تخفیف انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            description_fa: {
              label: 'توضیحات تخفیف فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'توضیحات تخفیف فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            description_en: {
              label: 'توضیحات تخفیف انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'توضیحات تخفیف انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            start: {
              label: 'تاریخ شروع تخفیف',
              elementType: 'DATE_PICKER',
              elementConfig: {
                type: 'text',
                placeholder: 'تاریخ شروع تخفیف'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            finish: {
              label: 'تاریخ پایان تخفیف',
              elementType: 'DATE_PICKER',
              elementConfig: {
                type: 'text',
                placeholder: 'تاریخ پایان تخفیف'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            title: {
              label: 'موقعیت در صفحه',
              elementType: 'select',
              elementConfig: {
                placeholder: 'موقعیت در صفحه',
                options: [
                  { value: '', displayValue: 'انتخاب' },
                  { value: 'home_up', displayValue: 'بالای صفحه اصلی' },
                  { value: 'home_down', displayValue: 'پایین صفحه اصلی' },
                ]
              },
              value: 'owner',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            is_percent: {
              label: 'مقدار تخفیف درصدی است؟',
              elementType: 'checkbox',
              elementConfig: {
                type: 'checkbox',
                placeholder: 'مقدار تخفیف درصدی است؟',
                checked: true
              },
              value: true,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            discount: {
              label: 'مقدار تخفیف',
              elementType: 'input',
              elementConfig: {
                type: 'number',
                placeholder: 'مقدار تخفیف',
                checked: true
              },
              value: true,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            order: {
              label: 'ترتیب تخفیف',
              elementType: 'input',
              elementConfig: {
                type: 'number',
                placeholder: 'ترتیب تخفیف',
              },
              value: true,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        },
        {
          title: 'ایجاد تخفیف',
          id: 'create_discount',
          link: '/discount/create',
          pageType: 'form',
          open: false,
          loadAsyncData: false,
          redirect: '/discount/index',
          api: {
            type: 'post',
            route: '/api/admin/create_discount',
          },
          active: false,
          controls: {
            name_fa: {
              label: 'عنوان تخفیف فارسی',
              elementType: 'text',
              elementConfig: {
                type: 'text',
                placeholder: 'عنوان تخفیف',
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            name_en: {
              label: 'عنوان تخفیف انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'عنوان تخفیف انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            description_fa: {
              label: 'توضیحات تخفیف فارسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'توضیحات تخفیف فارسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            description_en: {
              label: 'توضیحات تخفیف انگلیسی',
              elementType: 'input',
              elementConfig: {
                type: 'text',
                placeholder: 'توضیحات تخفیف انگلیسی'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            start: {
              label: 'تاریخ شروع تخفیف',
              elementType: 'DATE_PICKER',
              elementConfig: {
                type: 'text',
                placeholder: 'انتخاب تاریخ'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            finish: {
              label: 'تاریخ پایان تخفیف',
              elementType: 'DATE_PICKER',
              elementConfig: {
                type: 'text',
                placeholder: 'انتخاب تاریخ'
              },
              value: '',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            title: {
              label: 'موقعیت در صفحه',
              elementType: 'select',
              elementConfig: {
                placeholder: 'موقعیت در صفحه',
                options: [
                  { value: '', displayValue: 'انتخاب' },
                  { value: 'home_up', displayValue: 'بالای صفحه اصلی' },
                  { value: 'home_down', displayValue: 'پایین صفحه اصلی' },
                ]
              },
              value: 'owner',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            is_percent: {
              label: 'مقدار تخفیف درصدی است؟',
              elementType: 'checkbox',
              elementConfig: {
                type: 'checkbox',
                placeholder: 'مقدار تخفیف درصدی است؟',
                checked: true
              },
              value: true,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            discount: {
              label: 'مقدار تخفیف',
              elementType: 'input',
              elementConfig: {
                type: 'number',
                placeholder: 'مقدار تخفیف',
              },
              value: true,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            order: {
              label: 'ترتیب تخفیف',
              elementType: 'input',
              elementConfig: {
                type: 'number',
                placeholder: 'ترتیب تخفیف',
              },
              value: true,
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        }
      ]
    },
    {
      title: 'تنظیمات سرور',
      id: 'server_settings',
      subittle: 'تنظیمات سرور',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'زمان بروز رسانی قیمت ها',
          id: 'edit_price_refresh_time',
          link: '/settings/editPriceRefreshTiem',
          pageType: 'form',
          open: false,
          redirect: '/discount/index',
          hasId: true,
          // loadAsyncData: true,
          // loadAsyncDataConfig: {
          //   method: 'post',
          //   url: '/api/admin/get_one_discount',
          // },
          api: {
            type: 'post',
            route: '/api/admin/edit_discount',
            // get: {
            //   route: '/api/admin/get_one_discount',
            //   payload: ['discount_id']
            // }
          },
          active: false,
          controls: {
            time: {
              label: 'زمان بروز رسانی',
              elementType: 'TIME_PICKER',
              elementConfig: {
                type: 'text',
                placeholder: 'عنوان تخفیف',
              },
              value: '00:00',
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
            refresh_price: {
              label: 'بروز رسانی قیمت ها',
              elementType: 'API_BUTTON',
              elementConfig: {
                type: 'text',
                placeholder: 'بروز رسانی',
                apiType: 'post',
                apiUrl: '/api/admin/update_price',
                nullable: true
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            }
          }
        }
      ]
    },
    {
      title: 'مشاوره',
      id: 'arrange_meeting',
      subittle: 'مشاوره',
      icon: 'dashboard',
      open: false,
      pages: [
        {
          title: 'تنظیمات ساعات ملاقات',
          id: 'edit_meeting_time_table',
          link: '/timeTable/edit',
          pageType: 'form',
          open: false,
          active: false,
          redirect: '/timeTable/edit',
          // hasId: true,
          loadAsyncData: true,
          loadAsyncDataConfig: {
            method: 'get',
            url: '/api/admin/get_meeting_time_chart',
          },
          api: {
            type: 'post',
            route: '/api/admin/edit_meeting_time_chart',
            // get: {
            //   route: '/api/admin/get_one_discount',
            //   payload: ['discount_id']
            // }
          },
          controls: {
            days: {
              label: 'روز و ساعت ملاقات',
              elementType: 'TIME_TABLE',
              elementConfig: {
              },
              value: [],
              validation: {
                required: true,
              },
              valid: false,
              touched: false
            },
          }
        },
        {
          title: 'لیست درخواست ها',
          id: 'view_meeting_request_list',
          link: '/timeTable/reserved',
          pageType: 'table',
          open: false,
          active: false,
          api: {
            type: 'get',
            route: '/api/admin/get_all_discount'
          },
          controls: {
            head: [
              { name: 'date', title: 'تاریخ ملاقات', config: { textAlign: 'center', width: '100' } },
              { name: 'time', title: 'ساعت ملاقات', config: { textAlign: 'center', width: '300' } },
              { name: 'name', title: 'نام و نام خانوادگی', config: { textAlign: 'center', width: '300' } },
              { name: 'phone', title: 'شماره تماس', config: { textAlign: 'center', width: '300' } },
              { name: 'email', title: 'ایمیل', config: { textAlign: 'center', width: '300' } }
            ],
            filters: {
              name: {
                label: 'نام و نام خانوادگی',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'نام و نام خانوادگی'
                },
                value: '',
                touched: false
              },
              phone: {
                label: 'شماره تماس',
                elementType: 'input',
                elementConfig: {
                  type: 'text',
                  placeholder: 'شماره تماس'
                },
                value: '',
                touched: false
              }
            },
            actions: {
              delete: { apiRoute: '/api/admin/delete_discount', payload: ['meeting_id'] },
              edit: { redirect: '/timetable/confirmation', payload: [{ name: 'id', stateName: 'meeting_id' }] },
              confirm: { apiRoute: '/api/admin/confirm_comment', payload: ['id'], showConfirm: { 'confirm': 0 } },
            }
          }
        }
      ]
    }
  ]
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
}
export default reducer;