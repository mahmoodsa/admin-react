import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility'

const initialState = {
  fileTypes: [
    {
      id: 1,
      title: 'تصویر',
      fileType: 'image',
      apiDetails: {
        title: 'آپلود تصویر',
        post: '/api/admin/send_image',
        get: '/api/admin/get_all_image',
        acceptedFileTypes: 'image/png, image/jpeg'

      },
      isActive: true
      // icon: <Movie  classes={{root: classes.ml75}} />
    },
    {
      id: 2,
      title: 'ویدیو',
      fileType: 'video',
      apiDetails: {
        title: 'آپلود ویدیو',
        post: '/api/admin/send_video',
        get: '/api/admin/get_all_video',
        acceptedFileTypes: 'video/mp4'
      },
      isActive: false
      // icon: <Panorama classes={{root: classes.ml75}} />
    },
    {
      id: 3,
      title: 'فایل',
      fileType: 'file',
      apiDetails:
      {
        title: 'آپلود فایل',
        post: '/api/admin/send_file',
        get: '/api/admin/get_all_file',
        acceptedFileTypes: false
      },
      isActive: false
      // icon: <InsertDriveFile  classes={{root: classes.ml75}} />
    }
  ],
  fileItems: [],
  selectedFiles: [],
  downloading: false,
  downloadError: null,
  uploadError: false,
  uploading: false,
  altOpen: false,
  altConfig: {},
  uploadPayload: null,
  isOpen: false,
  limit: 10000,
  selected: [],
  callbackFunction: null,
  // Folders
  folders: [],
  foldersDownloading: false,
  foldersDownloadError: false,
  returnUrl: null
}

const sidebarLinkClicked = (state, action) => {
  const fileTypes = [...state.fileTypes];
  const newFileTypes = fileTypes.map((item, index) => {
    // if(item.isActive) {
    //   return Object.assign({}, item, {isActive:false});
    // }
    if (item.id === action.id) {
      return Object.assign({}, item, { isActive: true });
    }
    return item;
  })
  return updateObject(state, {
    fileTypes: newFileTypes
  })
}

const uploadTrigger = (state, action) => {
  let payload = {}
  if (action.fileType === 'image') {
    payload = {
      image: action.result.image,
      thumbnail_image: action.result.blob,
      name: action.result.name
    }
    return updateObject(state, {
      altOpen: true,
      uploading: true,
      uploadPayload: payload
    });
  } else if (action.fileType === 'video') {
    payload = {
      name: action.result.name,
      size: action.result.size,
      videoUrl: action.result.videoUrl,
      videoFile: action.result.videoFile
    }
    return updateObject(state, {
      altOpen: true,
      altConfig: {
        uploadVideo: true
      },
      uploadPayload: payload
    })
  } else {
    return updateObject(state, {
      altOpen: true,
      altConfig: {},
      uploadPayload: {
        file: action.file
      }
    })
  }

}
const uploadStart = (state, action) => {
  return updateObject(state, { loading: true })
}
const uploadFail = (state, action) => {
  return updateObject(state, { loading: false, uploadError: action.err, altOpen: false })
}
const uploadSuccess = (state, action) => {
  return updateObject(state, { loading: false, uploadError: false, altOpen: false })
}
const uplaodCancel = (state, action) => {
  return updateObject(state, { uploading: false, uploadError: false, altOpen: false, uploadPayload: null })
}
const downloadFail = (state, action) => {
  return updateObject(state, { downloading: false, downloadError: action.error })
}
const downloadStart = (state, action) => {
  return updateObject(state, { downloading: true })
}
const downloadSuccess = (state, action) => {
  return updateObject(state, { downloading: false, downloadError: null, fileItems: action.result })
}
const deleteFile = (state, action) => {
  const filteredItems = state.fileItems.filter(item => item.id !== action.id)
  return updateObject(state, { fileItems: filteredItems })
}
const openFileManager = (state, action) => {
  return updateObject(state, { isOpen: true })
}
const closeFileManager = (state, action) => {
  return updateObject(state, { isOpen: false })
}
const selectItem = (state, action) => {
  const selectedTemp = [...state.selected]
  selectedTemp.push(action.id)
  if (selectedTemp.length > state.limit) {
    return state;
  }
  return updateObject(state, { selected: selectedTemp });
}
const deSelectItem = (state, action) => {
  const selectedTemp = state.selected.filter(item => item !== action.id)
  return updateObject(state, { selected: selectedTemp });
}
const setLimit = (state, action) => {
  return updateObject(state, {
    limit: action.limit ? action.limit : 20000
  });
}
const setReturnUrl = (state, action) => {
  return updateObject(state, {
    returnUrl: action.returnUrl ? action.returnUrl : null
  })
}
const setCallbackFunction = (state, action) => {
  return updateObject(state, { callbackFunction: action.callbackFunction })
}
const fileManagerSubmitted = (state, action) => {
  if (state.limit === 1) {
    if(state.returnUrl) {
      const filteredItems = state.fileItems.filter(item => {
        if(item.id == state.selected) {
          return item
        }
      })
      state.callbackFunction(filteredItems[0].url);
    } else {
      state.callbackFunction(Number(state.selected));      
    }
  } else {
    state.callbackFunction(state.selected);
  }
  return state;
}
const setFileManagerValue = (state, action) => {
  if (action.value) {
    if (typeof action.value === 'object') {
      return updateObject(state, { selected: [...state.selected, ...action.value] })
    } else {
      return updateObject(state, { selected: [...state.selected, action.value] })
    }
  } else {
    return updateObject(state, { selected: [] })
  }
}
const setFileManagerInitialValue = (state, action) => {
  if (action.value) {
    if (typeof action.value === 'object') {
      return updateObject(state, { selected: action.value })
    } else if (typeof action.value === 'number') {
      return updateObject(state, { selected: [action.value] })
    } else {
      return updateObject(state, { selected: [] })
    }
  }
  return updateObject(state, { selected: [] })
}
const addFileItem = (state, action) => {
  return state;
}

const downlaodFoldersStart = (state, action) => {
  return updateObject(state, { foldersDownloading: true, foldersDownloadError: false })
  return state;
}
const donwloadFoldersSuccess = (state, action) => {
  return updateObject(state, { folders: action.folders, foldersDownloading: false, foldersDownloadError: false })
}
const downloadFoldersError = (state, action) => {
  return updateObject(state, { foldersDownloading: false, foldersDownloadError: action.error })
  return state;
}
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SIDEBAR_LINK_CLICKED: return sidebarLinkClicked(state, action);
    case actionTypes.UPLOAD_TRIGGER: return uploadTrigger(state, action);
    case actionTypes.UPLOAD_START: return uploadStart(state, action);
    case actionTypes.UPLOAD_FAIL: return uploadFail(state, action);
    case actionTypes.UPLOAD_SUCCESS: return uploadSuccess(state, action);
    case actionTypes.UPLOAD_CANCEL: return uplaodCancel(state, action);
    case actionTypes.DOWNLAOD_START: return downloadStart(state, action);
    case actionTypes.DOWNLAOD_FAIL: return downloadFail(state, action);
    case actionTypes.DOWNLAOD_SUCCESS: return downloadSuccess(state, action);
    case actionTypes.DELETE_FILE: return deleteFile(state, action);
    case actionTypes.OPEN_FILE_MANAGER: return openFileManager(state, action);
    case actionTypes.CLOSE_FILE_MANAGER: return closeFileManager(state, action);
    case actionTypes.SELECT_ITEM: return selectItem(state, action);
    case actionTypes.DE_SELECT_ITEM: return deSelectItem(state, action);
    case actionTypes.SET_LIMIT: return setLimit(state, action);
    case actionTypes.SET_RETURN_URL: return setReturnUrl(state, action);
    case actionTypes.SET_CALLBACK_FUNCITON: return setCallbackFunction(state, action);
    case actionTypes.SUBMIT_FILE_MANAGER: return fileManagerSubmitted(state, action);
    case actionTypes.SET_FILE_MANAGER_VALUE: return setFileManagerValue(state, action);
    case actionTypes.SET_FILE_MANAGER_INITIAL_VALUE: return setFileManagerInitialValue(state, action);
    case actionTypes.ADD_FILE_ITEM: return addFileItem(state, action);
    case actionTypes.DOWNLOAD_FOLDERS_START: return downlaodFoldersStart(state, action);
    case actionTypes.DOWNLOAD_FOLDERS_SUCCESS: return donwloadFoldersSuccess(state, action);
    case actionTypes.DOWNLOAD_FOLDERS_START: return downloadFoldersError(state, action);
    default:
      return state;
  }
}
export default reducer;