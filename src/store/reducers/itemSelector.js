import { AssignmentReturnTwoTone } from '@material-ui/icons';
import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility'

const initialState = {
    itemsList: [],
    label: null,
    selectedItemsId: [1],
    isOpen: false,
    donwloadUrl: '',
    laoding: false,
    error: false,
    countLimit: 10000,
    callbackFunction: () => {},
    type: 'grid'
}

const setItemSelectorItems = (state, action) => {
    return updateObject(state, {selectedItemsId: action.itemsList})
}
const setItemSelectorLabel = (state, action) => {
    return updateObject(state, {label: action.label})
}
const setItemSelectorSelectedItems = (state, action) => {
    if(state.countLimit == 1) {
        return updateObject(state, {selectedItemsId: [action.selectedItems]})
    } else {
        return updateObject(state, {selectedItemsId: action.selectedItems})
    }
}
const setItemSelectorCallbackFunction = (state, action) => {
    return updateObject(state, {callbackFunction: action.callbackFunction})
}
const openItemSelector = (state, action) => {
    return updateObject(state, {isOpen: true})
}
const closeItemSelector = (state, action) => {
    return updateObject(state, {isOpen: false})
}
const submitItemSelector = (state, action) => {
    if(!state.selectedItemsId || (state.selectedItemsId.length <=0)) {
        state.callbackFunction('')
    } else {
        if(state.countLimit == 1) {
            state.callbackFunction(state.selectedItemsId[0])
        } else {
            state.callbackFunction(state.selectedItemsId)
        }
    }
    return state;
}
const setIetmSelectorDownloadUrl = (state, action) => {
    return updateObject(state, {donwloadUrl: action.url})
}
const itemSelectorDownloadStart = (state, action) => {
    return updateObject(state, {laoding: true, error: false})
}
const itemSelectorDownloadSuccess = (state, action) => {
    return updateObject(state, {laoding: false, error: false, itemsList: action.itemsList})
}
const itemSelectorDownloadFail = (state, action) => {
    return updateObject(state, {laoding: false, error: action.error})
}
const itemSelectorSelectItem = (state, action) => {
    if(state.countLimit == 1) {
        return updateObject(state, {selectedItemsId: [action.id]})
    } else {
        return updateObject(state, {selectedItemsId: [...state.selectedItemsId , action.id]})
    }
}
const itemSelectorDeselectItem = (state, action) => {
    console.log(state.selectedItemsId.filter(id => {
        return id != action.id
    }));
    return updateObject(state, {
        selectedItemsId: state.selectedItemsId.filter(id => {
            return id != action.id
        })
    })
}
const setItemSelectorConfig = (state, action) => {
    return updateObject(state, {
        type: action.selectorConfig.selectorType,
        tableHeaderConfig: action.selectorConfig.head,
        countLimit: action.selectorConfig.limit
    })
}
const itemSelectorSelectAllItems = (state, action) => {
    let itemsIdList = []
    if(state.countLimit == 1) {
        return state;
    }
    if(action.isChecked) {
        itemsIdList = state.itemsList.map(item => item.id);
    } else {
        itemsIdList = []
    }
    return updateObject(state, {
        selectedItemsId: itemsIdList 
    });
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.OPEN_ITEM_SELECTOR: return openItemSelector(state, action);
        case actionTypes.CLOSE_ITEM_SELECTOR: return closeItemSelector(state, action);
        case actionTypes.SET_ITEM_SELECTOR_CONFIG: return setItemSelectorConfig(state, action);
        case actionTypes.SUBMIT_ITEM_SELECTOR: return submitItemSelector(state, action);
        case actionTypes.SET_ITEM_SELECTOR_ITEMS: return setItemSelectorItems(state, action);
        case actionTypes.SET_ITEM_SELECTOR_LABEL: return setItemSelectorLabel(state, action);
        case actionTypes.SET_ITEM_SELECTOR_SELECTED_ITEMS: return setItemSelectorSelectedItems(state, action);
        case actionTypes.SET_ITEM_SELECTOR_CALLBACK_FUNCTION: return setItemSelectorCallbackFunction(state, action);
        case actionTypes.SET_ITEM_SELECTOR_DOWNLOAD_URL: return setIetmSelectorDownloadUrl(state, action);
        case actionTypes.ITEM_SELECTOR_DOWNLOAD_START: return itemSelectorDownloadStart(state, action);
        case actionTypes.ITEM_SELECTOR_DOWNLOAD_SUCCESS: return itemSelectorDownloadSuccess(state, action);
        case actionTypes.ITEM_SELECTOR_DOWNLOAD_FAIL: return itemSelectorDownloadFail(state, action);
        case actionTypes.ITEM_SELECTOR_SELECT_ITEM: return itemSelectorSelectItem(state, action);
        case actionTypes.ITEM_SELECTOR_DESELECT_ITEM: return itemSelectorDeselectItem(state, action);
        case actionTypes.ITEM_SELECTOR_SELECT_ALL_ITEMS: return itemSelectorSelectAllItems(state, action);
        default:
            return state;
    }
}
export default reducer;