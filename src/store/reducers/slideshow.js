import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility'

const initialState = {
  isOpen: false,
  callbackFunction: null,
  controls: {
    file_id: {
      label: 'انتخاب فایل',
      elementType: 'fileManager',
      elementConfig: {
        type: 'fileManager',
        placeholder: 'fileManager',
        limit: 1
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    title_fa: {
      label: 'عنوان فارسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'عنوان فارسی'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    title_en: {
      label: 'عنوان انگلیسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'عنوان انگلیسی'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    text_fa: {
      label: 'متن فارسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'متن فارسی'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    text_en: {
      label: 'متن انگلیسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'متن انگلیسی'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    link_url_fa: {
      label: 'آدرس لینک فارسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'آدرس لینک فارسی'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    link_url_en: {
      label: 'آدرس لینک انگلیسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'آدرس لینک انگلیسی'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    link_text_fa: {
      label: 'متن لینک فارسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'متن لینک فارسی'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    link_text_en: {
      label: 'متن لینک انگلیسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'متن لینک انگلیسی'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    title_fa: {
      label: 'عنوان فارسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'عنوان'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    title_en: {
      label: 'عنوان انگلیسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'عنوان'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    link_text_fa: {
      label: 'متن لینک فارسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'متن لینک'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    link_text_en: {
      label: 'متن لینک انگلیسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'متن لینک'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    link_url_fa: {
      label: 'آدرس لینک فارسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'آدرس لینک'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    },
    link_url_en: {
      label: 'آدرس لینک انگلیسی',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'آدرس لینک'
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false
    }
  },
  seletctedItemIndex: 0,
  itemsList: []
}

const openSlideshow = (state, action) => {
  return updateObject(state, { isOpen: true })
}
const closeSlideshow = (state, action) => {
  return updateObject(state, { isOpen: false })
}
const setSlideshowCallbackFunction = (state, action) => {
  return updateObject(state, { callbackFunction: action.callbackFunction })
}
const setSlideshowControls = (state, action) => {
  return updateObject(state, { controls: action.controls })
}
const slideshowSubmitted = (state, action) => {
  const tempItemsList = [...state.itemsList];
  const updatedItem = {};
  for (let key in action.controls) {
    updatedItem[key] = action.controls[key].value;
  }
  updatedItem.thumbnail = action.thumbnail;
  tempItemsList[state.seletctedItemIndex] = {
    ...updatedItem
  }
  triggerCallbackFunction(tempItemsList, state)
  return updateObject(state, { itemsList: tempItemsList });
}
const triggerCallbackFunction = (itemsList, state) => {
  const tempResult = itemsList.map(item => {
    let fileId = Number(item.file_manager_id)
    if (item.file_manager_id) {
      fileId = Number(item.file_manager_id)
    } else if (item.file_id) {
      fileId = item.file_id;
    }
    return {
      file_id: fileId,
      thumbnail: item.thumbnail,
      text_fa: item.text_fa,
      text_en: item.text_en,
      title_fa: item.title_fa,
      title_en: item.title_en,
      link_text_fa: item.link_text_fa,
      link_text_en: item.link_text_en,
      link_url_fa: item.link_url_fa,
      link_url_en: item.link_url_en,
    }
  })
  state.callbackFunction(tempResult);
}
const setSlideshowIinitControls = (state, action) => {
  const tempControls = { ...state.controls }
  for (let key in tempControls) {
    const tempControl = { ...tempControls[key] };
    if (key === 'file_id') {
      if (action.value['file_manager_id']) {
        const fileManagerId = action.value['file_manager_id'];
        tempControl.value = Number(action.value['file_manager_id'])
      } else {
        tempControl.value = action.value[key];
      }
    } else {
      tempControl.value = action.value[key];
    }
    tempControls[key] = tempControl;
  }
  return updateObject(state, { controls: tempControls })
}
const setSlideShowItemsList = (state, action) => {
  const tempItemsList = action.items.map((item, index) => {
    const tempItem = {}
    for (let key in state.controls) {
      if (key === 'file_id') {
        if (item.file_manager_id) {
          tempItem[key] = Number(item.file_manager_id)
        } else {
          tempItem[key] = item[key];
        }
      } else {
        tempItem[key] = item[key];
      }
    }
    tempItem['thumbnail'] = item.thumbnail
    return tempItem;
  })
  return updateObject(state, { itemsList: tempItemsList });
}
const setSelectedItemIndex = (state, action) => {
  return updateObject(state, { seletctedItemIndex: action.index });
}
const refreshSlideshowControls = (state, action) => {
  const updatedControls = { ...state.controls }
  for (let key in updatedControls) {
    const tempControl = updatedControls[key];
    tempControl.value = '';
    updatedControls[key] = tempControl;
  }
  return updateObject(state, { controls: updatedControls })
}
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.OPEN_SLIDE_SHOW: return openSlideshow(state, action);
    case actionTypes.SET_SLIDE_SHOW_CALLBACK_FUNCITON: return setSlideshowCallbackFunction(state, action);
    case actionTypes.CLOSE_SLIDE_SHOW: return closeSlideshow(state, action);
    case actionTypes.SET_SLIDE_SHOW_CONTROLS: return setSlideshowControls(state, action);
    case actionTypes.SUBMIT_SLIDE_SHOW: return slideshowSubmitted(state, action);
    case actionTypes.SET_SLIDE_SHOW_INITI_CONTROLS: return setSlideshowIinitControls(state, action);
    case actionTypes.SET_SLIDE_SHOW_ITEMS_LIST: return setSlideShowItemsList(state, action);
    case actionTypes.SET_SELECTED_ITEM_INDEX: return setSelectedItemIndex(state, action);
    case actionTypes.REFRESH_SLIDE_SHOW_CONTROLS: return refreshSlideshowControls(state, action);
    default:
      return state;
  }
}
export default reducer;