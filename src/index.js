import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter } from 'react-router-dom';

import { createStore ,combineReducers, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk'

import * as serviceWorker from './serviceWorker';

import App from './App';

import authReducer from './store/reducers/auth';
import dynamicPages from './store/reducers/dynamicPages';
import fileManager from './store/reducers/fileManager';
import slideshow from './store/reducers/slideshow';
import itemSelector from './store/reducers/itemSelector';

import './index.css';

const rootReducer = combineReducers({
    auth: authReducer,
    dynamicPages: dynamicPages ,
    fileManager: fileManager,
    slideshow: slideshow,
    itemSelector: itemSelector
  });

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  
const store = createStore(rootReducer , composeEnhancers(
  applyMiddleware(thunk)
));

const app = (
  <Provider store={store}>
    <BrowserRouter basename="admin">
        <App />
      </BrowserRouter>
  </Provider>
)

ReactDOM.render(
  <React.StrictMode>
    {app}
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
