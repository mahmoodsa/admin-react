import React from 'react';

import { Search } from '@material-ui/icons'

import classes from './Search.module.scss';

const SearchForm = props => {
  const inputChangeHandler = (e) => {
    props.onSearch(e.target.value);
  }
  return (
    <form className={classes.SearchInputForm}>
    <input 
      type="text"
      placeholder="جستجو..."
      className={classes.SearchInput}
      onChange={inputChangeHandler}/>
      <Search fontSize="small" />
  </form>
  );
}

export default SearchForm;