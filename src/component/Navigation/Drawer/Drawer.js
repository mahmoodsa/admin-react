import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import classes from './Drawer.module.scss'

import Logo from '../../UI/Logo/Logo';
import SidebarToggler from '../../UI/SidebarToggler/SidebarToggler';
import Profile from './Profile/Profile';
import Search from './Search/Search';
import SidebarNavList from './SidebarNavList/SidebarNavList';

const Drawer = props => {
  const pages = [...useSelector(state => state.dynamicPages.pages)];

  const [navList, setNavList] = useState(pages);

  const defaultNavList = navList.map(item => {
    let dropdownItems = [...item.pages];
    dropdownItems = dropdownItems.map(dropdownItem => {
      return {...dropdownItem}
    });
    item.dropdownItems = dropdownItems;
    return {...item}
  });
  
  const navListItemClickHandler = (id) => {
    const newNavList = navList.map(navItem => {
      const item = {...navItem};
      item.id === id ? item.open = !item.open : item.open = false;
      return item;
    })
    setNavList(newNavList);
  }
  
  const searchHandler = (searchInput) => {
    if(searchInput.length <= 0) {
      setNavList(defaultNavList)
      return false;
    }
    const newNavList = navList.map(navItem => {
      const item = {...navItem};
      let shouldBeOpen = false;
      item.title.indexOf(searchInput) > -1 
      ? shouldBeOpen = true
      : shouldBeOpen = false;
      const navChildItems = navItem.dropdownItems.map(item => {
        return item
      });
      navChildItems.forEach(item => {
        if(item.title.indexOf(searchInput) > -1) {
          shouldBeOpen = true;
          navItemActivator(item, true)
        } else {
          navItemActivator(item, false);
        }
      })
      navItemActivator(item, shouldBeOpen);
      return item;
    })
    setNavList(newNavList);
  }

  const navItemActivator = (item, active) => {
    item.active = active;
    if(typeof(item.open) !== typeof(undefined)) {
       item.open = active;
    }
  }
  
  return (
      <div className={classes.Drawer}>
          <SidebarToggler />
          <Logo />
          <Profile />
          <Search onSearch={searchHandler}/>
          <SidebarNavList 
            navList={navList}
            clicked={navListItemClickHandler}/>
      </div>
  ) 
}

export default Drawer;