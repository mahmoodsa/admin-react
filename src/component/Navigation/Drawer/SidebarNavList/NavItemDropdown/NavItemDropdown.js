import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './NavItemDropdown.module.scss'

const NavItemDropdown = props => {
    const dropdownItems = props.dropdownItems.map(item => {
        return item.hideOnDrawer ? null : <li key={item.title}>
                <NavLink to={item.link}>{item.title}</NavLink>
            </li>
    })

    return (
        <div className={classes.SubmenuContainer}>
            <ul className={classes.SubmenuList}>
                {dropdownItems}
            </ul>
        </div>);
}

export default NavItemDropdown;