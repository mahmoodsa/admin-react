import React from 'react';
import { Dashboard, ChevronLeft } from '@material-ui/icons';

import classes from './NavItem.module.scss';

import NavItemDropdown from '../NavItemDropdown/NavItemDropdown';

const NavItem = props => {
    const itemIsOpen = props.item.open? classes.open : '';
    return (
    <li key={props.item.id} className={itemIsOpen}>
        <a href="#" onClick={props.itemClicked}>
            <Dashboard className={classes.NavItemIcon}/>
            <span>{props.item.title}</span>
            <ChevronLeft className={classes.CevronLeftIcon} />
        </a>
        <NavItemDropdown dropdownItems={props.item.dropdownItems} />
    </li>
  );
}

export default NavItem;