import React, { Fragment } from 'react';

import classes from './SidebarNavList.module.scss';

import NavItem from './NavItem/NavItem'

const SidebarNavList = props => {
    
    const navItemsList = props.navList.map((item, index) => {
        return <NavItem 
            item={item}
            key={item.id}
            itemIsOpen={item.open}
            itemClicked={props.clicked.bind(this, item.id)} />
    })

    
    return <Fragment>
        <div className={classes.NavItemsListHeader}>منو</div>
        <ul className={classes.SidebarNavList}>
            {navItemsList}
        </ul>
    </Fragment>;
}

export default SidebarNavList;