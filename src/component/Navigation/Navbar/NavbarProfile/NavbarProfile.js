import React, { useState } from 'react';

import classes from './NavbarProfile.module.scss'
import profileIcon from '../../../../assets/images/guy.jpg';

import { ArrowDropDown } from '@material-ui/icons'

import Dropdown from '../../../UI/Dropdown/Dropdown';

const NavbarProfile  = props => {
  const [dropdownItemsList, _setter] = useState([
    {title: 'ویرایش حساب کاربری', link: '/profile/edit', key: 'edit-profile'},
    {title: 'تغییر رمز عبور', link: '/profile/editPassword', key: 'edit-profile-password'},
    {title: 'خروج', link: '/logout', key: 'logout-link'}]);
  return (
    <div className={classes.Profile}>
      <button onClick={props.clicked}>
          <img src={profileIcon} alt="User image"/>
          <div className={classes.AccountInfo}>
                <div>نام کاربری</div>
                <div>مدیر سیستم</div>
          </div>
          <div className={classes.DropdownToggle}>
                <ArrowDropDown fontSize="small"/>
          </div>
      </button>
      {/* <Dropdown 
        header='حساب کاربری'
        items={dropdownItemsList}></Dropdown> */}
    </div>
  );
}

export default NavbarProfile;