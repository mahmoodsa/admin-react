import React from 'react';
import { Settings } from '@material-ui/icons'
import classes from './Navbar.module.scss';

import NavbarProfile from './NavbarProfile/NavbarProfile';

const Navbar  = props => {
  return (
      <div className={classes.Navbar}>
        <div className={classes.TitleContainer}>پنل مدریریت</div>
        <div className={classes.NavbarLeft}>
          <NavbarProfile />
          <button className={classes.SettingsButton}>
            <Settings />
          </button>
        </div>
      </div>
  );
}

export default Navbar;