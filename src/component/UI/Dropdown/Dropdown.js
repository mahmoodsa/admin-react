import React from 'react';

import classes from './Dropdown.module.scss';

const Dropdown  = props => {
  let dropdownItems = props.items.map(item => {
    return <div 
      key={item.key}
      className={classes.DropdownItem}>{item.title}</div>
  })
  return <div className={classes.Dropdown}>
          <div className={classes.DorpdownHeader}>
            {props.header}
          </div>
          {dropdownItems}
      </div>;
}

export default Dropdown;