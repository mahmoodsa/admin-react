import React from 'react';
import moment from 'moment';

import DatePicker from 'react-datetime';
import 'react-datetime/css/react-datetime.css';

import TimePicker from 'react-time-picker';
import 'react-time-picker/dist/TimePicker.css';


import FileManagerTrigger from '../../../container/Inputs/FileManager/Trigger/FileManagerTrigger';
import MultipleKeyValueInput from '../../../container/Inputs/MultipleKeyValueInput/MultipleKeyValueInput';
import ItemSelectorTrigger from '../../../container/Inputs/ItemSelector/ItemSelectorTrigger/ItemSelectorTrigger';
import SlideshowList from '../../../container/Inputs/slideshow/SlideshowList/SlideshowList';
import KeyValueInput from '../../../container/Inputs/KeyValueInput/KeyValueInput';
import AsyncDropdown from '../../../container/Inputs/AsyncDropdown/AsyncDropdown';
import InputList from '../../../container/Inputs/InputList/InputList';
import MultipleAsyncDropdown from '../../../container/Inputs/MultipleAsyncDropdown/MultipleAsyncDropdown';
import SelectablePropertySelector from '../../../container/Inputs/SelectablePropertySelector/SelectablePropertySelector'
import CheckBox from '../../../container/Inputs/CheckBox/CheckBox';
import FileUploader from '../../../container/Inputs/FileUploader/FileUploader';
import CkEditorInput from '../../../container/Inputs/CkEditorInput/CkEditorInput';
import ProductList from '../../../container/Inputs/ProductList/ProductList';
import ApiButton from '../../../container/Inputs/ApiButton/ApiButton';
import TimeTables from '../../../container/Inputs/TimeTable/TimeTable';

import classes from './Input.module.scss';
import './Input.scss';

const Input = (props) => {
    let inputElement = null;
    let validationMessage = null;
    const inputClasses = [classes.InputElement]
    const rootClassName = [classes.Input];
    if (props.invalid && props.shouldValidate && props.touched) {
        rootClassName.push(classes.Invalid)
        validationMessage = <div className={classes.ValidationMessage}>{props.validationMessage}</div>;
    }
    if (props.classes && props.classes.inputElement) {
        inputClasses.push(props.classes.inputElement)
    }
    switch (props.elementType) {
        case (elementTypes.input):
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value ? props.value : ''}
                onChange={props.changed} />
            break;
        case (elementTypes.textarea):
            inputElement = <textarea
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                style={props.elementConfig.styles}
                value={props.value ?? ''}
                onChange={props.changed} />
            break;
        case (elementTypes.select):
            inputElement = <select value={props.value}
                className={inputClasses.join(' ')}
                onChange={props.changed}>
                {props.elementConfig.options.map(option => {
                    return <option
                        key={option.value}
                        value={option.value}>{option.displayValue}</option>
                })}
            </select>
            break;
        case (elementTypes.checkbox):
            inputElement = <CheckBox
                {...props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;
        case (elementTypes.ASYNC_DROPDOWN):
            inputElement = <AsyncDropdown
                {...props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;
        case (elementTypes.MULTIPLE_ASYNC_DROPDOWN):
            inputElement = <MultipleAsyncDropdown
                {...props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;
        case (elementTypes.fileManager):
            inputElement = <FileManagerTrigger
                value={props.value}
                limit={props.elementConfig.limit ?? false}
                returnUrl={props.elementConfig.returnUrl ?? false}
                submitted={props.changed}>انتخاب فایل</FileManagerTrigger>
            break;
        case (elementTypes.slideshow):
            inputElement = <SlideshowList
                submitted={props.changed}
                {...props.elementConfig}
                value={props.value} />
            break;
        case (elementTypes.itemSelector):
            inputElement = <ItemSelectorTrigger
                {...props.elementConfig}
                submitted={props.changed}
                value={props.value} />
            break;
        case (elementTypes.INPUT_LIST):
            inputElement = <InputList
                {...props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;
        case (elementTypes.MULTIPLE_KEY_VALUE_INPUT):
            inputElement = <MultipleKeyValueInput
                {...props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;
        case (elementTypes.KEY_VALUE_INPUT):
            inputElement = <KeyValueInput
                {...props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;
        case (elementTypes.SELECTABLE_PROPERTY_SELECTOR):
            inputElement = <SelectablePropertySelector
                {...props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;
        case (elementTypes.DATE_PICKER):
            let m = new moment(props.value);
            m.format('YYYY-MM-DD HH:mm:ss')
            console.log(m);
            inputElement = <div>
                <DatePicker
                    onChange={value => {
                        props.changed(value.format('YYYY-MM-DD HH:mm:ss'));
                    }}
                    inputProps={{
                        placeholder: props.elementConfig.placeholder ?? '',
                        readOnly: true
                    }}
                    value={m}
                    // timeFormat="hh:mm:ss"
                    // dateFormat="YYYY-MM-DD"
                    className="date-picker" />
            </div>
            break;

        case (elementTypes.TIME_TABLE):
            inputElement = <div className={classes.TimePicker}>
                <TimeTables
                    changed={props.changed}
                    value={props.value} />
            </div>
            break;
        case (elementTypes.TIME_PICKER):
            let m1 = new moment(props.value, "hh:mm").format('hh:mm');
            inputElement = <div className={classes.TimePicker}>
                <TimePicker
                    onChange={value => {
                        props.changed(value);
                    }}
                    inputProps={{
                        placeholder: props.elementConfig.placeholder ?? '',
                        readOnly: true
                    }}
                    value={m1}
                    initialViewMode="time" />
            </div>
            break;
        case (elementTypes.hidden):
            inputElement = <input
                type="hidden"
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />
            break;
        case (elementTypes.FILE_UPLOADER):
            inputElement = <FileUploader
                elementConfig={props.elementConfig}
                value={props.value}
                onChange={props.changed} />
            break;
        case (elementTypes.CK_EDITOR):
            inputElement = <CkEditorInput
                elementConfig={props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;

        case (elementTypes.PRODUCT_LIST):
            inputElement = <ProductList
                elementConfig={props.elementConfig}
                value={props.value}
                changed={props.changed} />
            break;
        case (elementTypes.API_BUTTON):
            inputElement = <ApiButton
                elementConfig={props.elementConfig}
                value={props.value} />
            break;
        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />
            break;
    }
    const labelStyles = props.styles ? props.styles.label : null;
    const lableClasses = [classes.Label]
    if (props.classes && props.classes.label) {
        lableClasses.push(props.classes.label)
    }
    let rootStyles = props.styles ? props.styles.rootStyles : '';
    rootStyles = {
        ...rootStyles,
        display: props.elementType === elementTypes.hidden ? 'none' : ''
    }
    if (props.classes && props.classes.root) {
        rootClassName.push(props.classes.root);
    }
    return (
        <div className={rootClassName.join(' ')} style={rootStyles}>
            {props.disableLabel
                ? null
                : <label className={lableClasses.join(' ')} style={labelStyles}>{props.label}</label>}
            <div className={classes.InputContainer}>
                {inputElement}
                {validationMessage}
            </div>
        </div>
    )
};

export default Input;
export const elementTypes = {
    fileManager: 'fileManager',
    checkbox: 'checkbox',
    select: 'select',
    textarea: 'textarea',
    input: 'input',
    slideshow: 'SLIDE_SHOW',
    itemSelector: 'ITEM_SELECTOR',
    hidden: 'HIDDEN',
    INPUT_LIST: 'INPUT_LIST',
    MULTIPLE_KEY_VALUE_INPUT: 'MULTIPLE_KEY_VALUE_INPUT',
    KEY_VALUE_INPUT: 'KEY_VALUE_INPUT',
    ASYNC_DROPDOWN: 'ASYNC_DROPDOWN',
    MULTIPLE_ASYNC_DROPDOWN: 'MULTIPLE_ASYNC_DROPDOWN',
    SELECTABLE_PROPERTY_SELECTOR: 'SELECTABLE_PROPERTY_SELECTOR',
    DATE_PICKER: 'DATE_PICKER',
    TIME_PICKER: 'TIME_PICKER',
    ITEM_SELECTOR_TABLE: 'ITEM_SELECTOR_TABLE',
    FILE_UPLOADER: 'FILE_UPLOADER',
    CK_EDITOR: 'CK_EDITOR',
    PRODUCT_LIST: 'PRODUCT_LIST',
    API_BUTTON: 'API_BUTTON',
    TIME_TABLE: 'TIME_TABLE'
}