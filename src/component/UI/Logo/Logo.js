import React from 'react';

import classes from './Logo.module.scss';

import LogoIcon from '../../../assets/images/logo/namira-logo.png'

const Logo  = props => {
  return (
      <div className={classes.Logo}>
          <img src={LogoIcon} alt="Logo"/>  
      </div>
  );
}

export default Logo;