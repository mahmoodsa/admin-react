import React from 'react';
import classes from './Button.module.scss';

const Button  = props => {
  const classname = [props.type ?? '', classes.Button, props.className, props.disabled ? classes.Disabled : '' ].join(' ')
  return <button
    style={props.style}
    className={classname}
    onClick={props.clicked}
    type={props.buttonType}>
      {props.children}
    </button>;
}

export const buttonTypes = {
  primary: classes.BtnPrimary,
  secondary: classes.BtnSecondary
}

export default Button;