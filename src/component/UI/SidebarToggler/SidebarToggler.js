import React from 'react';

import SyncAlt from '@material-ui/icons/SyncAlt';

import classes from './SidebarToggler.module.scss'

const SidebarToggler  = props => {
  return (
      <button className={classes.SidebarToggler}>
          <SyncAlt />
      </button>
  );
}

export default SidebarToggler;