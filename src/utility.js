export const success = (msg) => {
  console.log(`%c ${msg}`, `background: #28a745; color: #fff;padding:.2rem;border-radius:10px;`)
}
export const info = (msg) => {
  console.log(`%c ${msg}`, `background: dodgerblue; color: #fff;padding:.2rem;border-radius:10px;`)
}

export const error = (msg) =>  {
  console.log(`%c ${msg}`, `background: #D9534F; color: #fff;padding:.2rem;border-radius:10px;`)
}
// ------ Global functions for inputs that require Comma Sparation ------
export const commaSeparate = (number) => {
  if (typeof number == typeof 'number') {
    number = number.toString();
  }
  var reversedString = number.split('').reverse().map(function (v, i) {
    var comma = (i !== 0 && i % 3 === 0) ? ',' : '';
    return v + comma;
  });
  return reversedString.reverse().join('');
}

export const stripComma = (number) => {
  var stripped = number.split('').filter(function (v) {
    return v !== ',';
  });
  return stripped.join('');
}

export const toPersianNumbers = (number) => {
  number = String(number);
  const persian = { 0: '۰', 1: '۱', 2: '۲', 3: '۳', 4: '۴', 5: '۵', 6: '۶', 7: '۷', 8: '۸', 9: '۹' };

  var list = number.match(/[0-9]/g);
  if (list != null && list.length != 0) {
    for (var i = 0; i < list.length; i++)
      number = number.replace(list[i], persian[list[i]]);
  }
  return number
}
