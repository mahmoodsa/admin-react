import React from 'react';

const LayoutContent = props => {
  return (<div style={{padding: '1.25rem'}}>{props.children}</div>);
}

export default LayoutContent;