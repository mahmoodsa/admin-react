import React, { useCallback, useEffect, useState } from 'react';

import classes from './Modal.module.scss';

const Modal  = props => {
  const [modalClasses, setModalClasses] = useState([]);
  const closeModalHandler = useCallback(() => {
    setModalClasses([classes.Modal, props.inbox? classes.ModalInbox : ''])
    if (props.modalClosed) {
      props.modalClosed();
    }
  }, [setModalClasses]);
  
  useEffect(() => {
    setModalClasses([classes.Modal, props.open ? classes.Open : '', props.inbox? classes.ModalInbox : '']);
  }, [props.open])
  
  

  return (
    <div className={modalClasses.join(' ')} style={props.style? props.style.root : null}>
    <div onClick={closeModalHandler} className={classes.ModalBackDorp}></div>
      <div className={classes.ModalContent} style={{width: props.width, height: props.height , ...props.style? props.style.modal: null}}>
        {props.children}
      </div>
    </div>
  );
}

export default Modal;